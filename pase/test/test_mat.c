#include "multilevel.h"
#include "eigensolver.h"
#include "errorestimate.h"

#include "pase.h"
#include "app_slepc.h"
#include "petscmat.h"

static char help[] = "Test with Laplace Mat.\n";

double AH_uH_time = 0.0;
double ah_gamma_time = 0.0;
double ah_T_uH_time = 0.0;
double alpha_gamma_time = 0.0;
double axpby_time = 0.0;

BOUNDARYTYPE BoundCond(INT bdid);
BOUNDARYTYPE MassBoundCond(INT bdid);
void BoundFun(double X[3], int dim, double *values);
void stiffmatrix(DOUBLE *left, DOUBLE *right, DOUBLE *coord, DOUBLE *AuxFEMValues, DOUBLE *value);
void massmatrix(DOUBLE *left, DOUBLE *right, DOUBLE *coord, DOUBLE *AuxFEMValues, DOUBLE *value);
void MatrixRead2(Mat **A, Mat **B, Mat **P, int refinetime1, int refinetime2, int levelnum);

int main(int argc, char *argv[])
{
    PetscErrorCode ierr;
    SlepcInitialize(&argc, &argv, (char *)0, help);
    srand(1);
    Mat *A_array, *B_array, *P_array;
    MatrixRead2(&A_array, &B_array, &P_array, 3, 1, 1);

    // create gcge_ops
    OPS *gcge_ops;
    OPS_Create(&gcge_ops);
    OPS_SLEPC_Set(gcge_ops);
    OPS_Setup(gcge_ops);

    // create pase_ops
    PASE_OPS *pase_ops;
    PASE_OPS_Create(&pase_ops, gcge_ops);

    int blocksize = 400;
    int pase_nev = 4 * blocksize;
    PASE_Matrix A = NULL;
    PASE_MatrixCreate(&A, pase_nev, A_array[0], pase_ops);
    PASE_MultiVector x, b;
    PASE_DefaultMultiVecCreateByMat((void ***)(&x), blocksize, (void *)A, pase_ops);
    PASE_DefaultMultiVecCreateByMat((void ***)(&b), blocksize, (void *)A, pase_ops);
    int start[2] = {0, 0};
    int end[2] = {blocksize, blocksize};

    int multtime = 500, i;
    OpenPFEM_Print("blocksize : %d   pase_nev : %d\n\n", blocksize, pase_nev);

    MPI_Barrier(MPI_COMM_WORLD);
    double starttime = MPI_Wtime();
    Mat A_H = (Mat)(A->A_H);
    BV x_H = (BV)(x->b_H), b_H = (BV)(b->b_H);
    for (i = 0; i < multtime; i++)
    {
        gcge_ops->MatTransDotMultiVec((void *)A_H, (void **)x_H, (void **)b_H, start, end, gcge_ops);
    }
    MPI_Barrier(MPI_COMM_WORLD);
    double endtime = MPI_Wtime();
    OpenPFEM_Print("======== [ 单独计算稀疏矩阵向量乘 ] ========\n");
    OpenPFEM_Print("%d 次矩阵乘法时间 : %g\n", multtime, endtime - starttime);

    MPI_Barrier(MPI_COMM_WORLD);
    starttime = MPI_Wtime();
    for (i = 0; i < multtime; i++)
    {
        MatTransDotMultiVecAll((void *)A, (void **)x, (void **)b, start, end, pase_ops);
    }
    MPI_Barrier(MPI_COMM_WORLD);
    endtime = MPI_Wtime();
    OpenPFEM_Print("======== [ 旧版本 ] ========\n");
    OpenPFEM_Print("%d 次矩阵乘法时间 : %g\n", multtime, endtime - starttime);

    MPI_Barrier(MPI_COMM_WORLD);
    starttime = MPI_Wtime();
    for (i = 0; i < multtime; i++)
    {
        MatTransDotMultiVecAll_new((void *)A, (void **)x, (void **)b, start, end, pase_ops);
    }
    MPI_Barrier(MPI_COMM_WORLD);
    endtime = MPI_Wtime();
    OpenPFEM_Print("======== [ 新版本 ] ========\n");
    OpenPFEM_Print("%d 次矩阵乘法时间 : %g\n", multtime, endtime - starttime);

    ierr = SlepcFinalize();
    return 0;
}


void MatrixRead2(Mat **A, Mat **B, Mat **P, int refinetime1, int refinetime2, int levelnum)
{
    int size, rank;
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    DOUBLE starttime = MPI_Wtime();
    // 如果想尝试不同规模可以修改加密次数(MeshUniformRefine) or 选择不同次数的有限元(FEMSpaceBuild)
    MESH *mesh = NULL;
    MeshCreate(&mesh, 3, MPI_COMM_WORLD);
    MeshBuild(mesh, "../data/dataCube5.txt", SIMPLEX, TETHEDRAL); // [0,1]^3 的立方体
    MeshUniformRefine(mesh, refinetime1);                         // 一致加密3次后会有5504个单元，这个时候分的能均匀一点
    MeshPartition(mesh);
    MeshUniformRefine(mesh, refinetime2);

    MULTIINDEX stiffLmultiindex[3] = {D100, D010, D001}, stiffRmultiindex[3] = {D100, D010, D001};
    MULTIINDEX massLmultiindex[1] = {D000}, massRmultiindex[1] = {D000};
    QUADRATURE *Quadrature = QuadratureBuild(QuadTetrahedral56);

    MESH *finermesh = NULL, *coarsemesh = NULL;
    FEMSPACE *finerspace = NULL, *coarsespace = NULL;
    FEMSPACE *massfinerspace = NULL, *masscoarsespace = NULL;
    DISCRETEFORM *StiffDiscreteForm = NULL, *MassDiscreteForm = NULL;
    MATRIX **stiffmatrices = (MATRIX **)malloc(levelnum * sizeof(MATRIX *));
    MATRIX **massmatrices = (MATRIX **)malloc(levelnum * sizeof(MATRIX *));
    MATRIX **prolongs = (MATRIX **)malloc((levelnum - 1) * sizeof(MATRIX *));
    INT i;
    int *localnnz = (int *)malloc(levelnum * sizeof(int));
    for (i = 0; i < levelnum; i++)
    {
        if (i == 0)
        {
            coarsemesh = mesh;
            coarsespace = FEMSpaceBuild(coarsemesh, C_T_P3_3D, BoundCond);
            masscoarsespace = FEMSpaceBuild(coarsemesh, C_T_P3_3D, MassBoundCond);
            StiffDiscreteForm = DiscreteFormBuild(coarsespace, 3, stiffLmultiindex, coarsespace, 3, stiffRmultiindex,
                                                  stiffmatrix, NULL, BoundFun, Quadrature);
            MassDiscreteForm = DiscreteFormBuild(masscoarsespace, 1, massLmultiindex, masscoarsespace, 1, massRmultiindex,
                                                 massmatrix, NULL, BoundFun, Quadrature);
            stiffmatrices[i] = NULL, massmatrices[i] = NULL;
            MatrixAssemble(&(stiffmatrices[i]), NULL, StiffDiscreteForm, TYPE_OPENPFEM);
            MatrixAssemble(&(massmatrices[i]), NULL, MassDiscreteForm, TYPE_OPENPFEM);
            MatrixDeleteDirichletBoundary(stiffmatrices[i], StiffDiscreteForm);
            MatrixDeleteDirichletBoundary(massmatrices[i], MassDiscreteForm);
            localnnz[i] = stiffmatrices[i]->diag->NumEntries + stiffmatrices[i]->ndiag->NumEntries;
            MatrixConvert(stiffmatrices[i], TYPE_PETSC, 0);
            MatrixConvert(massmatrices[i], TYPE_PETSC, 0);
            DiscreteFormDestroy(&StiffDiscreteForm);
            DiscreteFormDestroy(&MassDiscreteForm);
        }
        else
        {
            finermesh = MeshDuplicate(coarsemesh);
            MeshUniformRefine(finermesh, 1);
            finerspace = FEMSpaceBuild(finermesh, C_T_P3_3D, BoundCond);
            massfinerspace = FEMSpaceBuild(finermesh, C_T_P3_3D, MassBoundCond);
            StiffDiscreteForm = DiscreteFormBuild(finerspace, 3, stiffLmultiindex, finerspace, 3, stiffRmultiindex,
                                                  stiffmatrix, NULL, BoundFun, Quadrature);
            MassDiscreteForm = DiscreteFormBuild(massfinerspace, 1, massLmultiindex, massfinerspace, 1, massRmultiindex,
                                                 massmatrix, NULL, BoundFun, Quadrature);
            stiffmatrices[i] = NULL, massmatrices[i] = NULL, prolongs[i - 1] = NULL;
            MatrixAssemble(&(stiffmatrices[i]), NULL, StiffDiscreteForm, TYPE_OPENPFEM);
            MatrixAssemble(&(massmatrices[i]), NULL, MassDiscreteForm, TYPE_OPENPFEM);
            MatrixDeleteDirichletBoundary(stiffmatrices[i], StiffDiscreteForm);
            MatrixDeleteDirichletBoundary(massmatrices[i], MassDiscreteForm);
            localnnz[i] = stiffmatrices[i]->diag->NumEntries + stiffmatrices[i]->ndiag->NumEntries;
            MatrixConvert(stiffmatrices[i], TYPE_PETSC, 0);
            MatrixConvert(massmatrices[i], TYPE_PETSC, 0);
            ProlongMatrixAssemble(&(prolongs[i - 1]), coarsespace, finerspace, TYPE_OPENPFEM);
            ProlongDeleteDirichletBoundary(prolongs[i - 1], coarsespace, finerspace);
            MatrixConvert(prolongs[i - 1], TYPE_PETSC, 0);
            MeshDestroy(&coarsemesh);
            FEMSpaceDestroy(&coarsespace);
            DiscreteFormDestroy(&StiffDiscreteForm);
            DiscreteFormDestroy(&MassDiscreteForm);
            coarsemesh = finermesh;
            coarsespace = finerspace;
        }
    }
    DOUBLE endtime = MPI_Wtime();
    OpenPFEM_Print("完成时间 : %g\n", endtime - starttime);

    MeshDestroy(&coarsemesh);
    FEMSpaceDestroy(&coarsespace);
    FEMSpaceDestroy(&masscoarsespace);
    QuadratureDestroy(&Quadrature);

    Mat *A_array = (Mat *)malloc(levelnum * sizeof(Mat));
    Mat *B_array = (Mat *)malloc(levelnum * sizeof(Mat));
    int *localsize = (int *)malloc(levelnum * sizeof(int));
    int *maxsize = (int *)malloc(levelnum * sizeof(int));
    int *minsize = (int *)malloc(levelnum * sizeof(int));
    int *globalnnz = (int *)malloc(levelnum * sizeof(int));
    for (i = 0; i < levelnum; i++)
    {
        A_array[i] = (Mat)(stiffmatrices[i]->data_petsc);
        B_array[i] = (Mat)(massmatrices[i]->data_petsc);
        localsize[i] = stiffmatrices[i]->local_nrows;
    }
    MPI_Reduce(localsize, maxsize, levelnum, MPI_INT, MPI_MAX, 0, MPI_COMM_WORLD);
    MPI_Reduce(localsize, minsize, levelnum, MPI_INT, MPI_MIN, 0, MPI_COMM_WORLD);
    MPI_Reduce(localnnz, globalnnz, levelnum, MPI_INT, MPI_SUM, 0, MPI_COMM_WORLD);
    for (i = 0; i < levelnum; i++)
    {
        OpenPFEM_Print("[ 第 %d 层矩阵负载情况 ] 全局 %d 行, 其中最多 %d 行, 最少 %d 行\n", levelnum - i - 1, stiffmatrices[i]->global_nrows, maxsize[i], minsize[i]);
        OpenPFEM_Print("[ 第 %d 层stiffness矩阵 ] 非零元个数 %d \n", levelnum - i - 1, globalnnz[i]);
    }
    OpenPFEM_Print("\n");
    free(localsize);
    free(maxsize);
    free(minsize);
    free(localnnz);
    free(globalnnz);
    Mat *P_array = (Mat *)malloc((levelnum - 1) * sizeof(Mat));
    for (i = 0; i < levelnum - 1; i++)
    {
        P_array[i] = (Mat)(prolongs[i]->data_petsc);
    }
    *A = A_array;
    *B = B_array;
    *P = P_array;
}

BOUNDARYTYPE BoundCond(INT bdid)
{
    if (bdid > 0)
    {
        return DIRICHLET;
    }
    else
    {
        return INNER;
    }
}

BOUNDARYTYPE MassBoundCond(INT bdid)
{
    if (bdid > 0)
    {
        return MASSDIRICHLET;
    }
    else
    {
        return INNER;
    }
}

void BoundFun(double X[3], int dim, double *values)
{
    values[0] = 0.0;
}

void stiffmatrix(DOUBLE *left, DOUBLE *right, DOUBLE *coord, DOUBLE *AuxFEMValues, DOUBLE *value)
{
    value[0] = (left[0] * right[0] + left[1] * right[1] + left[2] * right[2]);
}

void massmatrix(DOUBLE *left, DOUBLE *right, DOUBLE *coord, DOUBLE *AuxFEMValues, DOUBLE *value)
{
    value[0] = left[0] * right[0];
}