#include "multilevel.h"
#include "eigensolver.h"
#include "errorestimate.h"

#include "pase.h"
#include "app_slepc.h"
#include "petscmat.h"

static char help[] = "Test 2.\n";

BOUNDARYTYPE BoundCond(INT bdid);
BOUNDARYTYPE MassBoundCond(INT bdid);
void BoundFun(double X[3], int dim, double *values);
void stiffmatrix(DOUBLE *left, DOUBLE *right, DOUBLE *coord, DOUBLE *AuxFEMValues, DOUBLE *value);
void massmatrix(DOUBLE *left, DOUBLE *right, DOUBLE *coord, DOUBLE *AuxFEMValues, DOUBLE *value);
void MatrixRead(Mat **A, Mat **B, Mat **P, int refinetime1, int refinetime2, int levelnum);

int PASE_DIRECT_SLEPC_KrylovSchur(void *A, void *B, int nev, double rtol)
{
    Mat mat_A = (Mat)(A);
    Mat mat_B = (Mat)(B);

    EPS eps;
    EPSCreate(MPI_COMM_WORLD, &eps);
    EPSSetOperators(eps, A, B);
    EPSSetProblemType(eps, EPS_GHEP);                         // generalized Hermitian problem
    EPSSetDimensions(eps, nev, PETSC_DEFAULT, PETSC_DEFAULT); // 只指定了求解个数, ncv(subspace最大维数)和mpd(projected problem最大维数)默认
    EPSSetWhichEigenpairs(eps, EPS_TARGET_MAGNITUDE);         // 距离target最小in magnitude
    EPSSetTarget(eps, 0);                                     // target = 0
    EPSSetTolerances(eps, rtol, PETSC_DEFAULT);               // maxitr默认

    //下面这些似乎都没啥用
    ST st;
    EPSGetST(eps, &st);       // spectral transformation
    STSetType(st, STSINVERT); // shift and invert

    KSP ksp;
    PC pc;
    STGetKSP(st, &ksp);
    KSPSetType(ksp, KSPPREONLY);
    KSPGetPC(ksp, &pc);
    PCSetType(pc, PCCHOLESKY); // cholesky

    EPSKrylovSchurSetDetectZeros(eps, PETSC_TRUE);
    PCFactorSetMatSolverType(pc, MATSOLVERMUMPS);
    PetscOptionsInsertString(NULL, "-st_mat_mumps_icntl_13 1 -st_mat_mumps_icntl_24 1 -st_mat_mumps_cntl_3 1e-12"); 

    EPSSetFromOptions(eps);
    EPSSetUp(eps);
    EPSSolve(eps);
    int i, rank;
    double eigr, eigi;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    for (i = 0; i < nev; i++)
    {
        EPSGetEigenvalue(eps, i, &eigr, &eigi);
        if (rank == 0)
            printf("[ %d ] %2.14f + %2.14f i\n", i, eigr, eigi);
    }
    EPSDestroy(&eps);
}

int PASE_DIRECT_SLEPC_LOBPCG(void *A, void *B, int nev, double rtol)
{
    Mat mat_A = (Mat)(A);
    Mat mat_B = (Mat)(B);

    EPS eps;
    EPSCreate(MPI_COMM_WORLD, &eps);
    EPSSetOperators(eps, A, B);
    EPSSetType(eps, EPSLOBPCG);

    EPSSetFromOptions(eps);
    EPSSetUp(eps);
    EPSSolve(eps);
    int i, rank;
    double eigr, eigi;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    for (i = 0; i < nev; i++)
    {
        EPSGetEigenvalue(eps, i, &eigr, &eigi);
        if (rank == 0)
            printf("[ %d ] %2.14f + %2.14f i\n", i, eigr, eigi);
    }
    EPSDestroy(&eps);
}

int main(int argc, char *argv[])
{
    PetscErrorCode ierr;
    SlepcInitialize(&argc, &argv, (char *)0, help);
    srand(1);

    int nev = 800, num_levels = 3;
    Mat *A_array, *B_array, *P_array;
#if 0
    MatrixRead(&A_array, &B_array, &P_array, num_levels);
    double *eval = NULL;
    void **evec = NULL;
    PASE_PARAMETER param;
    PASE_PARAMETER_Create(&param, num_levels, nev);
    param->multigrid_type = PASE_GMG;
    param->if_precondition = PRECOND_A;
    param->rtol = 1e-10;
    param->initial_rtol = 1e-8;
    param->aux_rtol = 1e-6;

    PASE_EigenSolver_GMG((void **)A_array, (void **)B_array, (void **)P_array, num_levels, eval, evec, nev, param);

    PASE_PARAMETER_Destroy(&param);
#elif 1
    MatrixRead(&A_array, &B_array, &P_array, 4, 1, 3);
    double *eval = NULL;
    void **evec = NULL;
    PASE_PARAMETER param;
    PASE_PARAMETER_Create(&param, num_levels, nev);
    param->multigrid_type = PASE_GMG;
    param->boundary_delete = true;
    param->if_precondition = PRECOND_B_A;
    param->rtol = 1e-8;
    param->initial_rtol = 1e-8;
    param->aux_rtol = 1e-4;

    PASE_EigenSolver_GMG((void **)A_array, (void **)B_array, (void **)P_array, num_levels, eval, evec, nev, param);

    PASE_PARAMETER_Destroy(&param);
#else
    MatrixRead2(&A_array, &B_array, &P_array, 4, 2, 1);
    int flag = 0;
    double atol = 1e-5;
    double rtol = 1e-8;

    PASE_DIRECT_SLEPC_KrylovSchur((void *)A_array[0], (void *)B_array[0], 100, rtol);
    // PASE_DIRECT_GCGE((void *)A_array[0], (void *)B_array[0], flag, nev, atol, rtol, argc, argv);
    //flag = 6; //6: lobpcg 2:ks
    //PASE_DIRECT_EPS((void *)A_array[2], (void *)B_array[2], flag, nev, rtol, argc, argv);
#endif

    ierr = SlepcFinalize();
    return 0;
}

void MatrixRead(Mat **A, Mat **B, Mat **P, int refinetime1, int refinetime2, int levelnum)
{
    DOUBLE starttime = MPI_Wtime();
    // 如果想尝试不同规模可以修改加密次数(MeshUniformRefine) or 选择不同次数的有限元(FEMSpaceBuild)
    MESH *mesh = NULL;
    MeshCreate(&mesh, 3, MPI_COMM_WORLD);
    MeshBuild(mesh, "../data/dataCube5.txt", SIMPLEX, TETHEDRAL); // [0,1]^3 的立方体
    MeshUniformRefine(mesh, refinetime1);                         // 一致加密3次后会有5504个单元，这个时候分的能均匀一点
    MeshPartition(mesh);
    MeshUniformRefine(mesh, refinetime2);

    MULTIINDEX stiffLmultiindex[4] = {D000, D100, D010, D001}, stiffRmultiindex[4] = {D000, D100, D010, D001};
    MULTIINDEX massLmultiindex[1] = {D000}, massRmultiindex[1] = {D000};
    QUADRATURE *Quadrature = QuadratureBuild(QuadTetrahedral56);

    MESH *finermesh = NULL, *coarsemesh = NULL;
    FEMSPACE *finerspace = NULL, *coarsespace = NULL;
    FEMSPACE *massfinerspace = NULL, *masscoarsespace = NULL;
    DISCRETEFORM *StiffDiscreteForm = NULL, *MassDiscreteForm = NULL;
    MATRIX **stiffmatrices = (MATRIX **)malloc(levelnum * sizeof(MATRIX *));
    MATRIX **massmatrices = (MATRIX **)malloc(levelnum * sizeof(MATRIX *));
    MATRIX **prolongs = (MATRIX **)malloc((levelnum - 1) * sizeof(MATRIX *));
    INT i;
    for (i = 0; i < levelnum; i++)
    {
        if (i == 0)
        {
            coarsemesh = mesh;
            coarsespace = FEMSpaceBuild(coarsemesh, C_T_P2_3D, BoundCond);
            masscoarsespace = FEMSpaceBuild(coarsemesh, C_T_P2_3D, MassBoundCond);
            StiffDiscreteForm = DiscreteFormBuild(coarsespace, 4, stiffLmultiindex, coarsespace, 4, stiffRmultiindex,
                                                  stiffmatrix, NULL, BoundFun, Quadrature);
            MassDiscreteForm = DiscreteFormBuild(masscoarsespace, 1, massLmultiindex, masscoarsespace, 1, massRmultiindex,
                                                 massmatrix, NULL, BoundFun, Quadrature);
            stiffmatrices[i] = NULL, massmatrices[i] = NULL;
            MatrixAssemble(&(stiffmatrices[i]), NULL, StiffDiscreteForm, TYPE_OPENPFEM);
            MatrixAssemble(&(massmatrices[i]), NULL, MassDiscreteForm, TYPE_OPENPFEM);
            MatrixDeleteDirichletBoundary(stiffmatrices[i], StiffDiscreteForm);
            MatrixDeleteDirichletBoundary(massmatrices[i], MassDiscreteForm);
            MatrixConvert(stiffmatrices[i], TYPE_PETSC, 0);
            MatrixConvert(massmatrices[i], TYPE_PETSC, 0);
            DiscreteFormDestroy(&StiffDiscreteForm);
            DiscreteFormDestroy(&MassDiscreteForm);
        }
        else
        {
            finermesh = MeshDuplicate(coarsemesh);
            MeshUniformRefine(finermesh, 1);
            finerspace = FEMSpaceBuild(finermesh, C_T_P2_3D, BoundCond);
            massfinerspace = FEMSpaceBuild(finermesh, C_T_P2_3D, MassBoundCond);
            StiffDiscreteForm = DiscreteFormBuild(finerspace, 4, stiffLmultiindex, finerspace, 4, stiffRmultiindex,
                                                  stiffmatrix, NULL, BoundFun, Quadrature);
            MassDiscreteForm = DiscreteFormBuild(massfinerspace, 1, massLmultiindex, massfinerspace, 1, massRmultiindex,
                                                 massmatrix, NULL, BoundFun, Quadrature);
            stiffmatrices[i] = NULL, massmatrices[i] = NULL, prolongs[i - 1] = NULL;
            MatrixAssemble(&(stiffmatrices[i]), NULL, StiffDiscreteForm, TYPE_OPENPFEM);
            MatrixAssemble(&(massmatrices[i]), NULL, MassDiscreteForm, TYPE_OPENPFEM);
            MatrixDeleteDirichletBoundary(stiffmatrices[i], StiffDiscreteForm);
            MatrixDeleteDirichletBoundary(massmatrices[i], MassDiscreteForm);
            MatrixConvert(stiffmatrices[i], TYPE_PETSC, 0);
            MatrixConvert(massmatrices[i], TYPE_PETSC, 0);
            ProlongMatrixAssemble(&(prolongs[i - 1]), coarsespace, finerspace, TYPE_OPENPFEM);
            ProlongDeleteDirichletBoundary(prolongs[i - 1], coarsespace, finerspace);
            MatrixConvert(prolongs[i - 1], TYPE_PETSC, 0);
            MeshDestroy(&coarsemesh);
            FEMSpaceDestroy(&coarsespace);
            DiscreteFormDestroy(&StiffDiscreteForm);
            DiscreteFormDestroy(&MassDiscreteForm);
            coarsemesh = finermesh;
            coarsespace = finerspace;
        }
    }
    DOUBLE endtime = MPI_Wtime();
    OpenPFEM_Print("完成时间 : %g\n", endtime - starttime);

    MeshDestroy(&coarsemesh);
    FEMSpaceDestroy(&coarsespace);
    FEMSpaceDestroy(&masscoarsespace);
    QuadratureDestroy(&Quadrature);

    Mat *A_array = (Mat *)malloc(levelnum * sizeof(Mat));
    Mat *B_array = (Mat *)malloc(levelnum * sizeof(Mat));
    int size, rank;
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    int *localsize = (int *)malloc(levelnum * sizeof(int));
    int *maxsize = (int *)malloc(levelnum * sizeof(int));
    int *minsize = (int *)malloc(levelnum * sizeof(int));
    for (i = 0; i < levelnum; i++)
    {
        A_array[i] = (Mat)(stiffmatrices[i]->data_petsc);
        B_array[i] = (Mat)(massmatrices[i]->data_petsc);
        localsize[i] = stiffmatrices[i]->local_nrows;
    }
    MPI_Reduce(localsize, maxsize, levelnum, MPI_INT, MPI_MAX, 0, MPI_COMM_WORLD);
    MPI_Reduce(localsize, minsize, levelnum, MPI_INT, MPI_MIN, 0, MPI_COMM_WORLD);
    for (i = 0; i < levelnum; i++)
    {
        OpenPFEM_Print("[ 第 %d 层矩阵负载情况 ] 全局 %d 行, 其中最多 %d 行, 最少 %d 行\n", levelnum - i - 1, stiffmatrices[i]->global_nrows, maxsize[i], minsize[i]);
    }
    OpenPFEM_Print("\n");
    free(localsize);
    free(maxsize);
    free(minsize);
    Mat *P_array = (Mat *)malloc((levelnum - 1) * sizeof(Mat));
    for (i = 0; i < levelnum - 1; i++)
    {
        P_array[i] = (Mat)(prolongs[i]->data_petsc);
    }
    *A = A_array;
    *B = B_array;
    *P = P_array;
}

BOUNDARYTYPE BoundCond(INT bdid)
{
    if (bdid > 0)
    {
        return DIRICHLET;
    }
    else
    {
        return INNER;
    }
}

BOUNDARYTYPE MassBoundCond(INT bdid)
{
    if (bdid > 0)
    {
        return MASSDIRICHLET;
    }
    else
    {
        return INNER;
    }
}

void BoundFun(double X[3], int dim, double *values)
{
    values[0] = 0.0;
}

void stiffmatrix(DOUBLE *left, DOUBLE *right, DOUBLE *coord, DOUBLE *AuxFEMValues, DOUBLE *value)
{
    double x1 = coord[0] - 0.5;
    double x2 = coord[1] - 0.5;
    double x3 = coord[2] - 0.5;
    double a11 = 1 + x1 * x1;
    double a12 = x1 * x2;
    double a13 = x1 * x3;
    double a22 = 1 + x2 * x2;
    double a23 = x2 * x3;
    double a33 = 1 + x3 * x3;
    value[0] = (a11 * left[1] + a12 * left[2] + a13 * left[3]) * right[1] +
               (a12 * left[1] + a22 * left[2] + a23 * left[3]) * right[2] +
               (a13 * left[1] + a23 * left[2] + a33 * left[3]) * right[3] +
               exp(x1 * x2 * x3) * left[0] * right[0];
}

void massmatrix(DOUBLE *left, DOUBLE *right, DOUBLE *coord, DOUBLE *AuxFEMValues, DOUBLE *value)
{
    value[0] = left[0] * right[0];
}
