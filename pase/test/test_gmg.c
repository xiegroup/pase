#include "multilevel.h"
#include "eigensolver.h"
#include "errorestimate.h"

#include "pase.h"
#include "app_slepc.h"
#include "petscmat.h"
#include "math.h"

#define BASE_FUNCTION C_T_P2_3D

static char help[] = "Test with Laplace Mat.\n";

void eigenvalue_generate(int n);
BOUNDARYTYPE BoundCond(INT bdid);
BOUNDARYTYPE MassBoundCond(INT bdid);
void BoundFun(double X[3], int dim, double *values);
void stiffmatrix(DOUBLE *left, DOUBLE *right, DOUBLE *coord, DOUBLE *AuxFEMValues, DOUBLE *value);
void massmatrix(DOUBLE *left, DOUBLE *right, DOUBLE *coord, DOUBLE *AuxFEMValues, DOUBLE *value);
void MatrixRead(Mat **A, Mat **B, Mat **P, int refinetime1, int refinetime2, int levelnum, MPI_Comm comm);
void MatrixRead2(Mat **A, Mat **B, Mat **P, int refinetime1, int refinetime2, int levelnum);

int main(int argc, char *argv[])
{
    SlepcInitialize(&argc, &argv, (char *)0, help);
    srand(1);

    int myid, procnum, group_id, group_num = 2;
    MPI_Comm_rank(MPI_COMM_WORLD, &myid);
    MPI_Comm_size(MPI_COMM_WORLD, &procnum);
    MPI_Comm group_comm;
    MPI_Group group;
    group_id = myid / (procnum / group_num);
    MPI_Comm_split(MPI_COMM_WORLD, group_id, myid, &group_comm);
    MPI_Comm_group(group_comm, &group);

    int nev = 600, num_levels = 3;
    Mat *A_array, *B_array, *P_array;
    // group_comm = MPI_COMM_WORLD;
    #if 1
    MatrixRead(&A_array, &B_array, &P_array, 4, 1, num_levels, group_comm);
    // MatrixRead2(&A_array, &B_array, &P_array, 5, 1, num_levels); //ref1,ref2,num_level

    double *eval = NULL;
    void **evec = NULL;
    PASE_PARAMETER param;
    PASE_PARAMETER_Create(&param, num_levels, nev, 1e-8, PASE_GMG);
    param->A_array = (void **)A_array;
    param->B_array = (void **)B_array;
    param->P_array = (void **)P_array;
    param->group_num = group_num;
    param->group_id = group_id;
    param->group = group;
    param->group_comm = group_comm;
    // param->initial_rtol = 1e-8;
    param->aux_rtol = 1e-8;
    param->pc_type = PRECOND_A;
    param->max_direct_count = 40;
    param->max_cycle_count = 20;
    // param->max_pre_count = 5;
    // param->max_post_count = 5;
    // param->pc_eigendcp = true;
    param->batch_size = 0;

    PASE_EigenSolver(&eval, &evec, param);

    // double atol = 1e-5;
    // double rtol = 1e-8;
    // PASE_DIRECT_GCGE((void *)A_array[0], (void *)B_array[0], 0, 1000, atol, rtol, argc, argv);

    PASE_PARAMETER_Destroy(&param);


    #else
    MatrixRead2(&A_array, &B_array, &P_array, 5, 1, 3); //ref1,ref2,num_level
    int flag = 0;
    double atol = 1e-1;
    double rtol = 1e-8;
     PASE_DIRECT_GCGE((void *)A_array[2], (void *)B_array[2], flag, nev, atol, rtol, argc, argv);
    #endif
    SlepcFinalize();
    return 0;
}
void MatrixRead2(Mat **A, Mat **B, Mat **P, int refinetime1, int refinetime2, int levelnum)
{
    DOUBLE starttime = MPI_Wtime();
    // 如果想尝试不同规模可以修改加密次数(MeshUniformRefine) or 选择不同次数的有限元(FEMSpaceBuild)
    MESH *mesh = NULL;
    MeshCreate(&mesh, 3, MPI_COMM_WORLD);
    MeshBuild(mesh, "../data/dataCube5.txt", SIMPLEX, TETHEDRAL); // [0,1]^3 的立方体
    MeshUniformRefine(mesh, refinetime1);                         // 一致加密3次后会有5504个单元，这个时候分的能均匀一点
    MeshPartition(mesh);
    MeshUniformRefine(mesh, refinetime2);

    MULTIINDEX stiffLmultiindex[3] = {D100, D010, D001}, stiffRmultiindex[3] = {D100, D010, D001};
    MULTIINDEX massLmultiindex[1] = {D000}, massRmultiindex[1] = {D000};
    QUADRATURE *Quadrature = QuadratureBuild(QuadTetrahedral56);

    MESH *finermesh = NULL, *coarsemesh = NULL;
    FEMSPACE *finerspace = NULL, *coarsespace = NULL;
    FEMSPACE *massfinerspace = NULL, *masscoarsespace = NULL;
    DISCRETEFORM *StiffDiscreteForm = NULL, *MassDiscreteForm = NULL;
    MATRIX **stiffmatrices = (MATRIX **)malloc(levelnum * sizeof(MATRIX *));
    MATRIX **massmatrices = (MATRIX **)malloc(levelnum * sizeof(MATRIX *));
    MATRIX **prolongs = (MATRIX **)malloc((levelnum - 1) * sizeof(MATRIX *));
    INT i;
    for (i = 0; i < levelnum; i++)
    {
        if (i == 0)
        {
            coarsemesh = mesh;
            coarsespace = FEMSpaceBuild(coarsemesh, BASE_FUNCTION, BoundCond);
            masscoarsespace = FEMSpaceBuild(coarsemesh, BASE_FUNCTION, MassBoundCond);
            StiffDiscreteForm = DiscreteFormBuild(coarsespace, 3, stiffLmultiindex, coarsespace, 3, stiffRmultiindex,
                                                  stiffmatrix, NULL, BoundFun, Quadrature);
            MassDiscreteForm = DiscreteFormBuild(masscoarsespace, 1, massLmultiindex, masscoarsespace, 1, massRmultiindex,
                                                 massmatrix, NULL, BoundFun, Quadrature);
            stiffmatrices[i] = NULL, massmatrices[i] = NULL;
            MatrixAssemble(&(stiffmatrices[i]), NULL, StiffDiscreteForm, TYPE_OPENPFEM);
            MatrixAssemble(&(massmatrices[i]), NULL, MassDiscreteForm, TYPE_OPENPFEM);
            MatrixDeleteDirichletBoundary(stiffmatrices[i], StiffDiscreteForm);
            MatrixDeleteDirichletBoundary(massmatrices[i], MassDiscreteForm);
            MatrixConvert(stiffmatrices[i], TYPE_PETSC, 0);
            MatrixConvert(massmatrices[i], TYPE_PETSC, 0);
            DiscreteFormDestroy(&StiffDiscreteForm);
            DiscreteFormDestroy(&MassDiscreteForm);
        }
        else
        {
            finermesh = MeshDuplicate(coarsemesh);
            MeshUniformRefine(finermesh, 1);
            finerspace = FEMSpaceBuild(finermesh, BASE_FUNCTION, BoundCond);
            massfinerspace = FEMSpaceBuild(finermesh, BASE_FUNCTION, MassBoundCond);
            StiffDiscreteForm = DiscreteFormBuild(finerspace, 3, stiffLmultiindex, finerspace, 3, stiffRmultiindex,
                                                  stiffmatrix, NULL, BoundFun, Quadrature);
            MassDiscreteForm = DiscreteFormBuild(massfinerspace, 1, massLmultiindex, massfinerspace, 1, massRmultiindex,
                                                 massmatrix, NULL, BoundFun, Quadrature);
            stiffmatrices[i] = NULL, massmatrices[i] = NULL, prolongs[i - 1] = NULL;
            MatrixAssemble(&(stiffmatrices[i]), NULL, StiffDiscreteForm, TYPE_OPENPFEM);
            MatrixAssemble(&(massmatrices[i]), NULL, MassDiscreteForm, TYPE_OPENPFEM);
            MatrixDeleteDirichletBoundary(stiffmatrices[i], StiffDiscreteForm);
            MatrixDeleteDirichletBoundary(massmatrices[i], MassDiscreteForm);
            MatrixConvert(stiffmatrices[i], TYPE_PETSC, 0);
            MatrixConvert(massmatrices[i], TYPE_PETSC, 0);
            ProlongMatrixAssemble(&(prolongs[i - 1]), coarsespace, finerspace, TYPE_OPENPFEM);
            ProlongDeleteDirichletBoundary(prolongs[i - 1], coarsespace, finerspace);
            MatrixConvert(prolongs[i - 1], TYPE_PETSC, 0);
            MeshDestroy(&coarsemesh);
            FEMSpaceDestroy(&coarsespace);
            DiscreteFormDestroy(&StiffDiscreteForm);
            DiscreteFormDestroy(&MassDiscreteForm);
            coarsemesh = finermesh;
            coarsespace = finerspace;
        }
    }
    DOUBLE endtime = MPI_Wtime();
    OpenPFEM_Print("完成时间 : %g\n", endtime - starttime);

    MeshDestroy(&coarsemesh);
    FEMSpaceDestroy(&coarsespace);
    FEMSpaceDestroy(&masscoarsespace);
    QuadratureDestroy(&Quadrature);

    Mat *A_array = (Mat *)malloc(levelnum * sizeof(Mat));
    Mat *B_array = (Mat *)malloc(levelnum * sizeof(Mat));
    int size, rank;
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    int *localsize = (int *)malloc(levelnum * sizeof(int));
    int *maxsize = (int *)malloc(levelnum * sizeof(int));
    int *minsize = (int *)malloc(levelnum * sizeof(int));
    for (i = 0; i < levelnum; i++)
    {
        A_array[i] = (Mat)(stiffmatrices[i]->data_petsc);
        B_array[i] = (Mat)(massmatrices[i]->data_petsc);
        localsize[i] = stiffmatrices[i]->local_nrows;
    }
    MPI_Reduce(localsize, maxsize, levelnum, MPI_INT, MPI_MAX, 0, MPI_COMM_WORLD);
    MPI_Reduce(localsize, minsize, levelnum, MPI_INT, MPI_MIN, 0, MPI_COMM_WORLD);
    for (i = 0; i < levelnum; i++)
    {
        OpenPFEM_Print("[ 第 %d 层矩阵负载情况 ] 全局 %d 行, 其中最多 %d 行, 最少 %d 行\n", levelnum - i - 1, stiffmatrices[i]->global_nrows, maxsize[i], minsize[i]);
    }
    OpenPFEM_Print("\n");
    free(localsize);
    free(maxsize);
    free(minsize);
    Mat *P_array = (Mat *)malloc((levelnum - 1) * sizeof(Mat));
    for (i = 0; i < levelnum - 1; i++)
    {
        P_array[i] = (Mat)(prolongs[i]->data_petsc);
    }
    *A = A_array;
    *B = B_array;
    *P = P_array;
}

void MatrixRead(Mat **A, Mat **B, Mat **P, int refinetime1, int refinetime2, int levelnum, MPI_Comm comm)
{
    DOUBLE starttime = MPI_Wtime();
    // 如果想尝试不同规模可以修改加密次数(MeshUniformRefine) or 选择不同次数的有限元(FEMSpaceBuild)
    MESH *mesh = NULL;
    MeshCreate(&mesh, 3, comm);
    MeshBuild(mesh, "../data/dataCube5.txt", SIMPLEX, TETHEDRAL); // [0,1]^3 的立方体
    MeshUniformRefine(mesh, refinetime1);                         // 一致加密3次后会有5504个单元，这个时候分的能均匀一点
    MeshPartition(mesh);
    MeshUniformRefine(mesh, refinetime2);

    MULTIINDEX stiffLmultiindex[3] = {D100, D010, D001}, stiffRmultiindex[3] = {D100, D010, D001};
    MULTIINDEX massLmultiindex[1] = {D000}, massRmultiindex[1] = {D000};
    QUADRATURE *Quadrature = QuadratureBuild(QuadTetrahedral56);

    MESH *finermesh = NULL, *coarsemesh = NULL;
    FEMSPACE *finerspace = NULL, *coarsespace = NULL;
    FEMSPACE *massfinerspace = NULL, *masscoarsespace = NULL;
    DISCRETEFORM *StiffDiscreteForm = NULL, *MassDiscreteForm = NULL;
    MATRIX **stiffmatrices = (MATRIX **)malloc(levelnum * sizeof(MATRIX *));
    MATRIX **massmatrices = (MATRIX **)malloc(levelnum * sizeof(MATRIX *));
    MATRIX **prolongs = (MATRIX **)malloc((levelnum - 1) * sizeof(MATRIX *));
    INT i;
    for (i = 0; i < levelnum; i++)
    {
        if (i == 0)
        {
            coarsemesh = mesh;
            coarsespace = FEMSpaceBuild(coarsemesh, BASE_FUNCTION, BoundCond);
            masscoarsespace = FEMSpaceBuild(coarsemesh, BASE_FUNCTION, MassBoundCond);
            StiffDiscreteForm = DiscreteFormBuild(coarsespace, 3, stiffLmultiindex, coarsespace, 3, stiffRmultiindex,
                                                  stiffmatrix, NULL, BoundFun, Quadrature);
            MassDiscreteForm = DiscreteFormBuild(masscoarsespace, 1, massLmultiindex, masscoarsespace, 1, massRmultiindex,
                                                 massmatrix, NULL, BoundFun, Quadrature);
            stiffmatrices[i] = NULL, massmatrices[i] = NULL;
            MatrixAssemble(&(stiffmatrices[i]), NULL, StiffDiscreteForm, TYPE_OPENPFEM);
            MatrixAssemble(&(massmatrices[i]), NULL, MassDiscreteForm, TYPE_OPENPFEM);
            MatrixDeleteDirichletBoundary(stiffmatrices[i], StiffDiscreteForm);
            MatrixDeleteDirichletBoundary(massmatrices[i], MassDiscreteForm);
            MatrixConvert(stiffmatrices[i], TYPE_PETSC, 0);
            MatrixConvert(massmatrices[i], TYPE_PETSC, 0);
            DiscreteFormDestroy(&StiffDiscreteForm);
            DiscreteFormDestroy(&MassDiscreteForm);
        }
        else
        {
            finermesh = MeshDuplicate(coarsemesh);
            MeshUniformRefine(finermesh, 1);
            finerspace = FEMSpaceBuild(finermesh, BASE_FUNCTION, BoundCond);
            massfinerspace = FEMSpaceBuild(finermesh, BASE_FUNCTION, MassBoundCond);
            StiffDiscreteForm = DiscreteFormBuild(finerspace, 3, stiffLmultiindex, finerspace, 3, stiffRmultiindex,
                                                  stiffmatrix, NULL, BoundFun, Quadrature);
            MassDiscreteForm = DiscreteFormBuild(massfinerspace, 1, massLmultiindex, massfinerspace, 1, massRmultiindex,
                                                 massmatrix, NULL, BoundFun, Quadrature);
            stiffmatrices[i] = NULL, massmatrices[i] = NULL, prolongs[i - 1] = NULL;
            MatrixAssemble(&(stiffmatrices[i]), NULL, StiffDiscreteForm, TYPE_OPENPFEM);
            MatrixAssemble(&(massmatrices[i]), NULL, MassDiscreteForm, TYPE_OPENPFEM);
            MatrixDeleteDirichletBoundary(stiffmatrices[i], StiffDiscreteForm);
            MatrixDeleteDirichletBoundary(massmatrices[i], MassDiscreteForm);
            MatrixConvert(stiffmatrices[i], TYPE_PETSC, 0);
            MatrixConvert(massmatrices[i], TYPE_PETSC, 0);
            ProlongMatrixAssemble(&(prolongs[i - 1]), coarsespace, finerspace, TYPE_OPENPFEM);
            ProlongDeleteDirichletBoundary(prolongs[i - 1], coarsespace, finerspace);
            MatrixConvert(prolongs[i - 1], TYPE_PETSC, 0);
            MeshDestroy(&coarsemesh);
            FEMSpaceDestroy(&coarsespace);
            DiscreteFormDestroy(&StiffDiscreteForm);
            DiscreteFormDestroy(&MassDiscreteForm);
            coarsemesh = finermesh;
            coarsespace = finerspace;
        }
    }
    DOUBLE endtime = MPI_Wtime();
    OpenPFEM_Print("完成时间 : %g\n", endtime - starttime);

    MeshDestroy(&coarsemesh);
    FEMSpaceDestroy(&coarsespace);
    FEMSpaceDestroy(&masscoarsespace);
    QuadratureDestroy(&Quadrature);

    Mat *A_array = (Mat *)malloc(levelnum * sizeof(Mat));
    Mat *B_array = (Mat *)malloc(levelnum * sizeof(Mat));
    int size, rank;
    MPI_Comm_size(comm, &size);
    MPI_Comm_rank(comm, &rank);
    int *localsize = (int *)malloc(levelnum * sizeof(int));
    int *maxsize = (int *)malloc(levelnum * sizeof(int));
    int *minsize = (int *)malloc(levelnum * sizeof(int));
    for (i = 0; i < levelnum; i++)
    {
        A_array[i] = (Mat)(stiffmatrices[i]->data_petsc);
        B_array[i] = (Mat)(massmatrices[i]->data_petsc);
        localsize[i] = stiffmatrices[i]->local_nrows;
    }
    MPI_Reduce(localsize, maxsize, levelnum, MPI_INT, MPI_MAX, 0, MPI_COMM_WORLD);
    MPI_Reduce(localsize, minsize, levelnum, MPI_INT, MPI_MIN, 0, MPI_COMM_WORLD);
    for (i = 0; i < levelnum; i++)
    {
        OpenPFEM_Print("[ 第 %d 层矩阵负载情况 ] 全局 %d 行, 其中最多 %d 行, 最少 %d 行\n", levelnum - i - 1, stiffmatrices[i]->global_nrows, maxsize[i], minsize[i]);
    }
    OpenPFEM_Print("\n");
    free(localsize);
    free(maxsize);
    free(minsize);
    Mat *P_array = (Mat *)malloc((levelnum - 1) * sizeof(Mat));
    for (i = 0; i < levelnum - 1; i++)
    {
        P_array[i] = (Mat)(prolongs[i]->data_petsc);
    }
    *A = A_array;
    *B = B_array;
    *P = P_array;
}

BOUNDARYTYPE BoundCond(INT bdid)
{
    if (bdid > 0)
    {
        return DIRICHLET;
    }
    else
    {
        return INNER;
    }
}

BOUNDARYTYPE MassBoundCond(INT bdid)
{
    if (bdid > 0)
    {
        return MASSDIRICHLET;
    }
    else
    {
        return INNER;
    }
}

void BoundFun(double X[3], int dim, double *values)
{
    values[0] = 0.0;
}

void stiffmatrix(DOUBLE *left, DOUBLE *right, DOUBLE *coord, DOUBLE *AuxFEMValues, DOUBLE *value)
{
    value[0] = (left[0] * right[0] + left[1] * right[1] + left[2] * right[2]);
}

void massmatrix(DOUBLE *left, DOUBLE *right, DOUBLE *coord, DOUBLE *AuxFEMValues, DOUBLE *value)
{
    value[0] = left[0] * right[0];
}

static void merge_double(double *data, int left, int mid, int right)
{
    int len = right - left + 1;
    double *temp = (double *)malloc(len * sizeof(double));
    int pos = 0, i = left, j = mid + 1;
    while (i <= mid && j <= right)
    {
        if (data[i] <= data[j])
        {
            temp[pos++] = data[i++];
        }
        else
        {
            temp[pos++] = data[j++];
        }
    }
    while (i <= mid)
    {
        temp[pos++] = data[i++];
    }
    while (j <= right)
    {
        temp[pos++] = data[j++];
    }
    int k;
    for (k = 0; k < len; k++)
    {
        data[left++] = temp[k];
    }
    free(temp);
}

// 排序 [ left , right ]
static void mergesort_double(double *data, int left, int right)
{
    if (left == right)
        return;
    int mid = (left + right) / 2;
    mergesort_double(data, left, mid);
    mergesort_double(data, mid + 1, right);
    merge_double(data, left, mid, right);
}

void eigenvalue_generate(int n)
{
    int *index = (int *)malloc(n * sizeof(int));
    int i, j, k, pos;
    for (i = 0; i < n; i++)
        index[i] = i + 1;
    double *eval = (double *)malloc(n * n * n * sizeof(double));
    for (i = 1; i <= n; i++)
    {
        for (j = 1; j <= n; j++)
        {
            for (k = 1; k <= n; k++)
            {
                eval[pos] = PI * PI * (i * i + j * j + k * k);
                pos++;
            }
        }
    }
    mergesort_double(eval, 0, n * n * n - 1);
    int rank;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    if (rank == 0)
    {
        for (i = 0; i < n * n * n; i++)
        {
            printf("eval[%d] = %2.16e\n", i, eval[i]);
        }
    }
}
