#include "pase_sol.h"
#include "pase_app_gcge.h"
#include "app_slepc.h"
#include "eigensolver.h"

#define PASE_Error(FUNCTION_NAME, ERROR_INFO)                         \
    do                                                                \
    {                                                                 \
        OpenPFEM_Print("[ERROR %s] %s\n", FUNCTION_NAME, ERROR_INFO); \
        MPI_Barrier(MPI_COMM_WORLD);                                  \
        MPI_Abort(MPI_COMM_WORLD, 1);                                 \
    } while (0)

#define PRINT_INFO 1

int debug_aux_gcge = 0;

int A_pre = 0;
#define PASE_COMM_WORLD MPI_COMM_WORLD
int PASE_EigenSolver(double **eval, void ***evec, PASE_PARAMETER param)
{
    // create gcge_ops
    OPS *gcge_ops;
    OPS_Create(&gcge_ops);
    OPS_SLEPC_Set(gcge_ops);
    OPS_Setup(gcge_ops);

    // create pase_ops
    PASE_OPS *pase_ops;
    PASE_OPS_Create(&pase_ops, gcge_ops);

    // create solver
#if PRINT_INFO
    gcge_ops->Printf("\n============== [ solver creation ] ==============\n");
#endif
    PASE_MG_SOLVER solver = PASE_Mg_solver_create(param, pase_ops);
#if PRINT_INFO
    gcge_ops->Printf("=================================================\n\n");
#endif

    solver->total_time -= MPI_Wtime();
#if PRINT_INFO
    gcge_ops->Printf("=============== [ solver setup ] ================\n");
#endif
    PASE_Mg_set_up(solver, param);
#if PRINT_INFO
    gcge_ops->Printf("=================================================\n\n");
    gcge_ops->Printf("================ [ PASE solve ] =================\n");
#endif
    solver->total_solve_time -= gcge_ops->GetWtime();
    PASE_Mg_solve(solver);
    solver->total_solve_time += gcge_ops->GetWtime();
    solver->total_time += gcge_ops->GetWtime();
#if PRINT_INFO
    gcge_ops->Printf("=================================================\n\n");
#endif
    PASE_Mg_print_timer(solver);
    PASE_Mg_solver_destroy(solver);

    solver->total_time += MPI_Wtime();
}

int PASE_Mg_solve(PASE_MG_SOLVER solver)
{
    OPS *gcge_ops = solver->gcge_ops;
    int idx_batch, idx_cycle;
    solver->conv_nev = 0;
    solver->nlock_smooth = 0;
    for (idx_batch = 0; idx_batch < solver->batch_num; idx_batch++)
    {
        solver->current_batch = idx_batch;
        int rank = -1;
        MPI_Comm_rank(solver->group_comm, &rank);
        if (rank == 0)
            printf("\n第 %d 组 batch %d : 计算从 %d 到 %d 的特征对\n", solver->group_id, idx_batch,
                   solver->current_nev_start, solver->current_nev_end);
        // if (idx_batch == 1)
        {
            for (idx_cycle = 0; idx_cycle < solver->max_cycle_count; idx_cycle++)
            {
                solver->current_cycle = idx_cycle;

#if PRINT_INFO
                gcge_ops->Printf("\n================= batch [%d] cycle [%d] =================\n\n", idx_batch, idx_cycle);
#endif
                if (solver->group_num == 1 && solver->batch_num == 1)
                    PASE_Mg_cycle_simple(solver);
                else
                    PASE_Mg_cycle(solver);
#if PRINT_INFO
                gcge_ops->Printf("\n------------------------------\n");
#endif
                solver->error_estimate_time -= gcge_ops->GetWtime();
                PASE_Mg_error_estimate(solver);
                solver->error_estimate_time += gcge_ops->GetWtime();
                if (solver->conv_nev >= solver->real_batch_size)
                {
                    break;
                }
                // break;
            }
        }
        solver->conv_nev -= solver->real_batch_size;
        solver->nlock_smooth = solver->conv_nev;
        // 重新确定起始位置
        solver->current_nev_start += solver->real_batch_size;
        solver->nlock_smooth = solver->conv_nev;
        solver->current_nev_end = (solver->current_nev_start + solver->batch_size) > solver->real_nev ? solver->real_nev : (solver->current_nev_start + solver->batch_size);
        solver->batch_size = solver->current_nev_end - solver->current_nev_start;
        // break;
    }
}

int PASE_Mg_cycle_simple(PASE_MG_SOLVER solver)
{
    OPS *gcge_ops = solver->gcge_ops;
#if PRINT_INFO
    gcge_ops->Printf("(0) pre-smoothing:\n");
    double timer = solver->smooth_time;
#endif
    solver->smooth_time -= gcge_ops->GetWtime();
    PASE_Mg_smoothing(solver, solver->max_pre_count);
    solver->smooth_time += gcge_ops->GetWtime();
#if PRINT_INFO
    gcge_ops->Printf("-   pre-smoothing time : %f sec\n\n", solver->smooth_time - timer);
    gcge_ops->Printf("(1) aux space setup:\n");
    timer = solver->build_aux_time;
#endif
    solver->build_aux_time -= gcge_ops->GetWtime();
    PASE_Mg_set_pase_aux_vector(solver);
    PASE_Mg_set_pase_aux_matrix_simple(solver);
    solver->build_aux_time += gcge_ops->GetWtime();
#if PRINT_INFO
    gcge_ops->Printf("\n-   build aux space time : %f sec\n\n", solver->build_aux_time - timer);
    gcge_ops->Printf("(2) aux solver:\n");
    timer = solver->aux_direct_solve_time;
#endif
    solver->aux_direct_solve_time -= gcge_ops->GetWtime();
    PASE_Aux_direct_solve_simple(solver);
    solver->aux_direct_solve_time += gcge_ops->GetWtime();
#if PRINT_INFO
    gcge_ops->Printf("\n-   aux solver time : %f sec\n\n", solver->aux_direct_solve_time - timer);
    gcge_ops->Printf("(3) prolong to fine level:\n");
    timer = solver->prolong_time;
#endif
    solver->prolong_time -= gcge_ops->GetWtime();
    PASE_Mg_choose_eigenpairs_simple(solver);
    PASE_Mg_prolong_from_pase_aux_Solution(solver, solver->aux_fine_level);
    solver->prolong_time += gcge_ops->GetWtime();
#if PRINT_INFO
    gcge_ops->Printf("-   prolong time : %f sec\n\n", solver->prolong_time - timer);
    gcge_ops->Printf("(4) post smoothing:\n");
    timer = solver->smooth_time;
#endif
    solver->smooth_time -= gcge_ops->GetWtime();
    PASE_Mg_smoothing(solver, solver->max_post_count);
    solver->smooth_time += gcge_ops->GetWtime();
#if PRINT_INFO
    gcge_ops->Printf("-   post-smoothing time : %f sec\n\n", solver->smooth_time - timer);
#endif
    return 0;
}

int PASE_Mg_cycle(PASE_MG_SOLVER solver)
{
    OPS *gcge_ops = solver->gcge_ops;
#if PRINT_INFO
    gcge_ops->Printf("(0) pre-smoothing:\n");
    double timer = solver->smooth_time;
#endif
    solver->smooth_time -= gcge_ops->GetWtime();
    PASE_Mg_smoothing(solver, solver->max_pre_count);
    solver->smooth_time += gcge_ops->GetWtime();
    solver->solver_setup_time -= gcge_ops->GetWtime();
    if (!solver->current_cycle)
        PASE_Mg_find_shift(solver, 0);
    solver->solver_setup_time += gcge_ops->GetWtime();
#if PRINT_INFO
    gcge_ops->Printf("-   pre-smoothing time : %f sec\n\n", solver->smooth_time - timer);
    gcge_ops->Printf("(1) aux space setup:\n");
    timer = solver->build_aux_time;
#endif
    solver->build_aux_time -= gcge_ops->GetWtime();
    PASE_Mg_set_pase_aux_vector(solver);
    PASE_Mg_set_pase_aux_matrix(solver);
    solver->build_aux_time += gcge_ops->GetWtime();
#if PRINT_INFO
    gcge_ops->Printf("\n-   build aux space time : %f sec\n\n", solver->build_aux_time - timer);
    gcge_ops->Printf("(2) aux solver:\n");
    timer = solver->aux_direct_solve_time;
#endif
    solver->aux_direct_solve_time -= gcge_ops->GetWtime();
    PASE_Aux_direct_solve(solver);
    solver->aux_direct_solve_time += gcge_ops->GetWtime();
    if (solver->current_shift != 0.0)
        solver->aux_A->A_H = solver->multigrid->A_array[solver->aux_coarse_level];
#if PRINT_INFO
    gcge_ops->Printf("\n-   aux solver time : %f sec\n\n", solver->aux_direct_solve_time - timer);
    gcge_ops->Printf("(3) prolong to fine level:\n");
    timer = solver->prolong_time;
#endif
    solver->prolong_time -= gcge_ops->GetWtime();
    PASE_Mg_choose_eigenpairs(solver);
    PASE_Mg_prolong_from_pase_aux_Solution(solver, solver->aux_fine_level);
    solver->prolong_time += gcge_ops->GetWtime();
#if PRINT_INFO
    gcge_ops->Printf("-   prolong time : %f sec\n\n", solver->prolong_time - timer);
    gcge_ops->Printf("(4) post smoothing:\n");
    timer = solver->smooth_time;
#endif
    solver->smooth_time -= gcge_ops->GetWtime();
    PASE_Mg_smoothing(solver, solver->max_post_count);
    solver->smooth_time += gcge_ops->GetWtime();
#if PRINT_INFO
    gcge_ops->Printf("-   post-smoothing time : %f sec\n\n", solver->smooth_time - timer);
#endif

    return 0;
}

int PASE_Mg_error_estimate(PASE_MG_SOLVER solver)
{
    OPS *gcge_ops = solver->gcge_ops;
    int mv_s[2], mv_e[2];
    int batch_size = solver->batch_size;
    int conv_nev = solver->conv_nev;
    int nev_start = solver->current_nev_start;
    int nev_end = solver->current_nev_end;

    double *eigenvalues = solver->eigenvalues;
    double rtol = solver->rtol;
    int current_level = solver->aux_fine_level;
    void **sol = solver->solution[current_level];
    void **rhs = solver->cg_rhs[current_level];
    void **res = solver->cg_res[current_level];
    void *A = solver->multigrid->A_array[current_level];
    void *B = solver->multigrid->B_array[current_level];

#if PRINT_INFO
    gcge_ops->Printf("[error estimate] cycle %d ( %d - %d )\n", solver->current_cycle, nev_start, nev_end);
    gcge_ops->Printf("·                conv_nev = %d\n", conv_nev);
#endif

    int block_size = batch_size;
    int idx_eig = 0, start = conv_nev;
    int end = (start + block_size) < batch_size ? (start + block_size) : batch_size;
    int flag = 0;
    double *check_multi = solver->double_tmp;
    double r_norm;
    while (1)
    {
        mv_s[0] = nev_start + start;
        mv_e[0] = nev_start + end;
        mv_s[1] = 0;
        mv_e[1] = end - start;
        // rhs = Bx
        gcge_ops->MatDotMultiVec(B, sol, rhs, mv_s, mv_e, gcge_ops);
        // res = Ax
        gcge_ops->MatDotMultiVec(A, sol, res, mv_s, mv_e, gcge_ops);
        for (idx_eig = start; idx_eig < end; idx_eig++)
        {
            mv_s[0] = idx_eig - start;
            mv_e[0] = mv_s[0] + 1;
            mv_s[1] = idx_eig - start;
            mv_e[1] = mv_s[1] + 1;
            // rhs = Ax - lambda Bx
            gcge_ops->MultiVecAxpby(1.0, res, -eigenvalues[nev_start + idx_eig], rhs, mv_s, mv_e, gcge_ops);
            // r_norm = rhs^T rhs
            gcge_ops->MultiVecInnerProd('N', rhs, rhs, 0, mv_s, mv_e, &r_norm, 1, gcge_ops);
            r_norm = sqrt(r_norm) / eigenvalues[nev_start + idx_eig];
            if (idx_eig + 1 < nev_end)
                check_multi[idx_eig] = fabs((eigenvalues[nev_start + idx_eig] - eigenvalues[nev_start + idx_eig + 1]) / eigenvalues[nev_start + idx_eig]);
            if (r_norm < rtol)
            {
#if (PRINT_INFO == 1)
                gcge_ops->Printf("· 收敛！         [eig %d] %2.14e : %2.14e\n", nev_start + idx_eig, eigenvalues[nev_start + idx_eig], r_norm);
#endif
                if(!flag)
                    solver->conv_nev = idx_eig + 1;
            }
            else
            {
                gcge_ops->Printf("·                [eig %d] %2.14e : %2.14e\n", nev_start + idx_eig, eigenvalues[nev_start + idx_eig], r_norm);
                flag = 1;
                // break;
            }
        }
        // solver->conv_nev = idx_eig;
        if (end == batch_size || flag)
            break;
        start += block_size;
        end = (end + block_size) < batch_size ? (end + block_size) : batch_size;
    }
    // double gapMin = 1e-4;
    // if (solver->conv_nev > 1 && solver->conv_nev < batch_size)
    // {
    //         while (check_multi[solver->conv_nev - 1] < gapMin && solver->conv_nev > conv_nev)
    //         {
    // #if PRINT_INFO
    //             gcge_ops->Printf("[error estimate] No.%d 和 No.%d 判定重根！\n", solver->conv_nev - 1, solver->conv_nev);
    // #endif
    //             solver->conv_nev--;
    //             if (solver->conv_nev < 1 && solver->conv_nev >= batch_size)
    //             {
    //                 break;
    //             }
    //         }
    // }
    solver->nlock_smooth = solver->conv_nev;

#if PRINT_INFO
    gcge_ops->Printf("[error estimate] final conv_nev = %d\n", solver->conv_nev);
#endif
}

int PASE_Aux_direct_solve(PASE_MG_SOLVER solver)
{
    void *A = solver->aux_A;
    void *B = solver->aux_B;
    // = solver->multigrid->A_array[solver->aux_coarse_level];
    double shift_invert = solver->current_shift; // 传给GCGE的位移值\theta
    int pc_type = solver->pc_type;

    double *eigenvalues = solver->aux_eigenvalues;
    memcpy(eigenvalues, solver->eigenvalues + (solver->current_batch) * solver->real_batch_size, solver->batch_size * sizeof(int));
    void **eigenvectors = (void **)(solver->aux_sol);

    OPS *gcge_ops = solver->gcge_ops;
    OPS *pase_ops_to_gcge = solver->pase_ops_to_gcge;

    int nevConv = solver->batch_size + solver->more_aux_nev, multiMax = 1;
    double gapMin = 1e-5;
    int nevGiven = solver->batch_size;
    // nevGiven = nevConv;
    int block_size = nevConv / 2, nevMax = 2 * nevConv;
    int nevInit = 2 * nevConv;
    nevInit = nevInit < nevMax ? nevInit : nevMax;

    block_size = nevConv / 4; // can be smaller , i.e. nevConv / 8 , depending on nevConv
    nevInit = nevConv + block_size;
    nevMax = nevInit;

    // if (solver->current_shift > 800)
    // {
    //     // nevConv = nevConv + 60;
    //     block_size = nevConv / 2;
    //     nevInit = nevConv + block_size;
    //     nevMax = nevInit;
    // }
    int max_iter_gcg = solver->max_direct_count;
    // solver->aux_rtol = solver->current_cycle < 1 ? solver->aux_rtol * 1e-2 : solver->aux_rtol;
    double tol_gcg[2] = {sqrt(solver->aux_rtol), solver->aux_rtol};
    void **gcg_mv_ws[5];
    gcg_mv_ws[0] = (void **)(solver->aux_gcg_mv[0]);
    gcg_mv_ws[1] = (void **)(solver->aux_gcg_mv[1]);
    gcg_mv_ws[2] = (void **)(solver->aux_gcg_mv[2]);
    gcg_mv_ws[3] = (void **)(solver->aux_gcg_mv[3]);
    gcg_mv_ws[4] = (void **)(solver->aux_gcg_mv[4]->b_H);
    double *dbl_ws = solver->double_tmp;
    int *int_ws = solver->int_tmp;
    double *dbl_ws_hrr = solver->double_tmp_hrr;

    srand(0);
    EigenSolverSetup_GCG_h(multiMax, gapMin, nevInit, nevMax, block_size,
                           tol_gcg, max_iter_gcg, 0, shift_invert, gcg_mv_ws, dbl_ws, int_ws, dbl_ws_hrr, pase_ops_to_gcge);

    int check_conv_max_num = 50;

    char initX_orth_method[8] = "mgs";
    int initX_orth_block_size = -1;
    int initX_orth_max_reorth = 2;
    double initX_orth_zero_tol = 2 * DBL_EPSILON; // 1e-12

    char compP_orth_method[8] = "mgs";
    int compP_orth_block_size = -1;
    int compP_orth_max_reorth = 2;
    double compP_orth_zero_tol = 2 * DBL_EPSILON; // 1e-12

    char compW_orth_method[8] = "mgs";
    int compW_orth_block_size = -1;
    int compW_orth_max_reorth = 2;
    double compW_orth_zero_tol = 2 * DBL_EPSILON; // 1e-12
    int compW_bpcg_max_iter = 50;                 // todo 这个次数也许不需要这么多
    double compW_bpcg_rate = 1e-2;
    double compW_bpcg_tol = 1e-14;
    char compW_bpcg_tol_type[8] = "abs";

    int compRR_min_num = -1;
    double compRR_min_gap = gapMin;
    double compRR_tol = 2 * DBL_EPSILON;
#if PRINT_INFO
#if DIRECT
    gcge_ops->Printf("use direct lin_sol in GCGE(aux)\n");
#endif
    gcge_ops->Printf("[aux direct solve] level :\n", solver->aux_coarse_level);
    gcge_ops->Printf("[aux direct solve] gcg设置:\n");
    gcge_ops->Printf("·                  nevConv    = %d \n", nevConv);
    gcge_ops->Printf("·                  block_size = %d \n", block_size);
    gcge_ops->Printf("·                  nevMax     = %d \n", nevMax);
    gcge_ops->Printf("·                  nevGiven   = %d \n", nevGiven);
    gcge_ops->Printf("·                  gapMin     = %e \n", gapMin);
    gcge_ops->Printf("·                  rtol       = %e \n", solver->aux_rtol);
    gcge_ops->Printf("·                  max_iter   = %d \n", solver->max_direct_count);
#endif

    EigenSolverSetParameters_GCG(check_conv_max_num,
                                 initX_orth_method, initX_orth_block_size, initX_orth_max_reorth, initX_orth_zero_tol,
                                 compP_orth_method, compP_orth_block_size, compP_orth_max_reorth, compP_orth_zero_tol,
                                 compW_orth_method, compW_orth_block_size, compW_orth_max_reorth, compW_orth_zero_tol,
                                 compW_bpcg_max_iter, compW_bpcg_rate, compW_bpcg_tol, compW_bpcg_tol_type, 0,
                                 compRR_min_num, compRR_min_gap, compRR_tol,
                                 pase_ops_to_gcge);
    // in pase iteration
    // if (solver->current_batch == 1)
    // nevGiven = 0;
#if 0
    // if (solver->current_shift > 1000)
    {
        struct GCGSolver_ *gcgsolver;
        gcgsolver = (GCGSolver*)pase_ops_to_gcge->eigen_solver_workspace;
        gcgsolver->tol[0] = 1e-2;
        gcgsolver->tol[1] = 1e-5;
        int inev = nevConv;
        // gcge_ops->Printf("inev  = %d,nevGiven = %d\n", inev, nevGiven);
        pase_ops_to_gcge->EigenSolver(A, B, NULL, eigenvalues, eigenvectors, 0, &inev,    shift_invert, pase_ops_to_gcge);
        gcgsolver->tol[0] = tol_gcg[0];
        gcgsolver->tol[1] = tol_gcg[1];
    }
#endif
    pase_ops_to_gcge->EigenSolver(A, B, NULL, eigenvalues, eigenvectors, nevGiven, &nevConv, shift_invert, pase_ops_to_gcge); // shift_invert 表示位移\theta
    int idx;
#if 0
    int idx;
    gcge_ops->Printf("before ==aux_nev=%d=bs=%d \n", solver->aux_gcg_mv[4]->num_aux_vec, solver->aux_sol->num_aux_vec);
    for (idx = 0; idx < nevConv; idx++)
    {
        gcge_ops->Printf("beig[%d]=%2.16e\n", idx, eigenvalues[idx]);
    }
    

    int start[2], end[2];
    start[0] = 0; end[0] = solver->real_batch_size;
    start[1] = 0; end[1] = solver->real_batch_size; //多大再确认下
    // struct GCGSolver_ *gcg_solver;
    double *ss_matA = solver->double_tmp;
    gcge_ops->MatDotMultiVec(A, eigenvectors,(void **)solver->aux_gcg_mv[4], start, end, gcge_ops);
        MPI_Barrier(MPI_COMM_WORLD);
        MPI_Abort(MPI_COMM_WORLD, 0);
    gcge_ops->MultiVecInnerProd('S', eigenvectors, gcg_mv_ws[0], 0, start, end, ss_matA, nevConv, gcge_ops);
    double *ss_matB = solver->double_tmp_hrr;
    gcge_ops->MatDotMultiVec(A, eigenvectors, gcg_mv_ws[0], start, end, gcge_ops);
    gcge_ops->MultiVecInnerProd('S', eigenvectors, gcg_mv_ws[0], 0, start, end, ss_matB, nevConv, gcge_ops);

    char char_V = 'V', char_U = 'U';
    int type = 1, info;
    int real_size = nevConv;
    int lwork = 1 + 6 * real_size + 2 * real_size * real_size;
    double *work_dsygv = solver->double_tmp + nevConv * nevConv;
    int liwork = 3 + 5 * real_size;
    int *iwork = solver->int_tmp;
    dsygvd(&type, &char_V, &char_U, &real_size,
            ss_matA, &real_size, ss_matB, &real_size, eigenvalues,
            work_dsygv, &lwork, iwork, &liwork, &info);

    gcge_ops->MultiVecAxpby(1.0, eigenvectors, 0.0, gcg_mv_ws[0], start, end, gcge_ops);

    gcge_ops->MultiVecLinearComb(gcg_mv_ws[0], eigenvectors, 0, start, end, ss_matA, real_size, NULL, 0, gcge_ops);

    gcge_ops->Printf("after === \n");
    for (idx = 0; idx < nevConv; idx++)
    {
        gcge_ops->Printf("aeig[%d]=%2.16e\n", idx, eigenvalues[idx]);
    }
    
        MPI_Barrier(MPI_COMM_WORLD);
        MPI_Abort(MPI_COMM_WORLD, 0);
#endif

    if (solver->current_batch == 1)
        debug_aux_gcge = 0;

#if PRINT_INFO
    gcge_ops->Printf("[aux direct solve] gcg结果:\n");
    gcge_ops->Printf("·                  nevConv    = %d \n", nevConv);
    // for (idx = 0; idx < solver->batch_size + solver->more_aux_nev; idx++)
    // {
    //     solver->aux_eigenvalues[idx] = eigenvalues[idx];
    // }
    for (idx = 0; idx < solver->batch_size + solver->more_aux_nev; idx++)
    {
        gcge_ops->Printf("aux_eig[%d]=%2.16e\n", idx, eigenvalues[idx]);
    }
#endif

    return 0;
}

int PASE_Aux_direct_solve_simple(PASE_MG_SOLVER solver)
{
    void *A = solver->aux_A;
    void *B = solver->aux_B;

    double *eigenvalues = solver->aux_eigenvalues;
    void **eigenvectors = (void **)(solver->aux_sol);

    OPS *gcge_ops = solver->gcge_ops;
    OPS *pase_ops_to_gcge = solver->pase_ops_to_gcge;

    int nevConv = solver->user_nev, multiMax = 1;
    int nevGiven = nevConv;
    double gapMin = 1e-5;
    int block_size = nevConv / 2, nevMax = 2 * nevConv;
    int nevInit = 2 * nevConv;
    nevInit = nevInit < nevMax ? nevInit : nevMax;

    // block_size = nevConv / 4; // can be smaller , i.e. nevConv / 8 , depending on nevConv
    nevInit = nevConv + block_size;
    nevMax = nevInit;

    int max_iter_gcg = solver->max_direct_count;
    double tol_gcg[2] = {sqrt(solver->aux_rtol), solver->aux_rtol};
    void **gcg_mv_ws[5];
    gcg_mv_ws[0] = (void **)(solver->aux_gcg_mv[0]);
    gcg_mv_ws[1] = (void **)(solver->aux_gcg_mv[1]);
    gcg_mv_ws[2] = (void **)(solver->aux_gcg_mv[2]);
    gcg_mv_ws[3] = (void **)(solver->aux_gcg_mv[3]);
    gcg_mv_ws[4] = (void **)(solver->aux_gcg_mv[4]->b_H);
    double *dbl_ws = solver->double_tmp;
    int *int_ws = solver->int_tmp;
    double *dbl_ws_hrr = solver->double_tmp_hrr;

    srand(0);
    EigenSolverSetup_GCG(multiMax, gapMin, nevInit, nevMax, block_size,
                         tol_gcg, max_iter_gcg, 0, 0.0, gcg_mv_ws, dbl_ws, int_ws, pase_ops_to_gcge);

    int check_conv_max_num = 50;

    char initX_orth_method[8] = "mgs";
    int initX_orth_block_size = -1;
    int initX_orth_max_reorth = 2;
    double initX_orth_zero_tol = 2 * DBL_EPSILON; // 1e-12

    char compP_orth_method[8] = "mgs";
    int compP_orth_block_size = -1;
    int compP_orth_max_reorth = 2;
    double compP_orth_zero_tol = 2 * DBL_EPSILON; // 1e-12

    char compW_orth_method[8] = "mgs";
    int compW_orth_block_size = -1;
    int compW_orth_max_reorth = 2;
    double compW_orth_zero_tol = 2 * DBL_EPSILON; // 1e-12
    int compW_bpcg_max_iter = 50;                 // todo 这个次数也许不需要这么多
    double compW_bpcg_rate = 1e-2;
    double compW_bpcg_tol = 1e-14;
    char compW_bpcg_tol_type[8] = "abs";

    int compRR_min_num = -1;
    double compRR_min_gap = gapMin;
    double compRR_tol = 2 * DBL_EPSILON;
#if PRINT_INFO
#if DIRECT
    gcge_ops->Printf("use direct lin_sol in GCGE(aux)\n");
#endif
    gcge_ops->Printf("[aux direct solve] level :\n", solver->aux_coarse_level);
    gcge_ops->Printf("[aux direct solve] gcg设置:\n");
    gcge_ops->Printf("·                  nevConv    = %d \n", nevConv);
    gcge_ops->Printf("·                  block_size = %d \n", block_size);
    gcge_ops->Printf("·                  nevMax     = %d \n", nevMax);
    gcge_ops->Printf("·                  nevGiven   = %d \n", nevGiven);
    gcge_ops->Printf("·                  gapMin     = %e \n", gapMin);
    gcge_ops->Printf("·                  rtol       = %e \n", solver->aux_rtol);
    gcge_ops->Printf("·                  max_iter   = %d \n", solver->max_direct_count);
#endif

    EigenSolverSetParameters_GCG(check_conv_max_num,
                                 initX_orth_method, initX_orth_block_size, initX_orth_max_reorth, initX_orth_zero_tol,
                                 compP_orth_method, compP_orth_block_size, compP_orth_max_reorth, compP_orth_zero_tol,
                                 compW_orth_method, compW_orth_block_size, compW_orth_max_reorth, compW_orth_zero_tol,
                                 compW_bpcg_max_iter, compW_bpcg_rate, compW_bpcg_tol, compW_bpcg_tol_type, 0,
                                 compRR_min_num, compRR_min_gap, compRR_tol,
                                 pase_ops_to_gcge);
    pase_ops_to_gcge->EigenSolver(A, B, NULL, eigenvalues, eigenvectors, nevGiven, &nevConv, 0.0, pase_ops_to_gcge);
    int idx;

#if PRINT_INFO
    gcge_ops->Printf("[aux direct solve] gcg结果:\n");
    gcge_ops->Printf("·                  nevConv    = %d \n", nevConv);
    for (idx = 0; idx < solver->batch_size + solver->more_aux_nev; idx++)
    {
        gcge_ops->Printf("aux_eig[%d]=%2.16e\n", idx, eigenvalues[idx]);
    }
#endif

    return 0;
}

int PASE_Mg_prolong_from_pase_aux_Solution(PASE_MG_SOLVER solver, int object_level)
{
    // 这里应该是排好序的 只算 conv_nev 到 batch_size 个
    OPS *gcge_ops = solver->gcge_ops;
    int mv_s[2], mv_e[2];
    int batch_size = solver->batch_size;
    int conv_nev = solver->conv_nev;
    double one_double = 1.0, zero_double = 0.0, mone_double = -1.0;
    int current_level = solver->aux_coarse_level;
    int one_int = 1;

    PASE_MultiVector aux_u = solver->aux_sol;
    void **uH = aux_u->b_H;
    double *gamma = aux_u->aux_h;

    void **u_H2h = solver->cg_rhs[object_level];
    void **solution_h = solver->solution[object_level];
    // 只取前面 conv_nev 到 batch_size
    mv_s[0] = conv_nev;
    mv_e[0] = batch_size;
    mv_s[1] = conv_nev;
    mv_e[1] = batch_size;
    PASE_MULTIGRID_fromItoJ(solver->multigrid,
                            current_level, object_level, mv_s, mv_e, uH, u_H2h);
    // 找对应的原位置
    int start = solver->current_nev_start, end = solver->current_nev_end;
    mv_s[0] = start;
    mv_e[0] = end;
    mv_s[1] = conv_nev;
    mv_e[1] = batch_size;
    gcge_ops->MultiVecLinearComb(solution_h, u_H2h, 0, mv_s, mv_e,
                                 gamma + conv_nev * batch_size, batch_size, &one_double, 0, gcge_ops);
    BVSetActiveColumns((BV)u_H2h, conv_nev, batch_size);
    BVSetActiveColumns((BV)solution_h, start + conv_nev, end);
    BVCopy((BV)u_H2h, (BV)solution_h);
}

static void merge_double_with_int(double *data, int *index, int left, int mid, int right)
{
    int len = right - left + 1;
    double *temp = (double *)malloc(len * sizeof(double));
    int *index_temp = (int *)malloc(len * sizeof(int));
    int pos = 0, i = left, j = mid + 1;
    while (i <= mid && j <= right)
    {
        if (data[i] <= data[j])
        {
            index_temp[pos] = index[i];
            temp[pos++] = data[i++];
        }
        else
        {
            index_temp[pos] = index[j];
            temp[pos++] = data[j++];
        }
    }
    while (i <= mid)
    {
        index_temp[pos] = index[i];
        temp[pos++] = data[i++];
    }
    while (j <= right)
    {
        index_temp[pos] = index[j];
        temp[pos++] = data[j++];
    }
    int k;
    for (k = 0; k < len; k++)
    {
        index[left] = index_temp[k];
        data[left++] = temp[k];
    }
    free(temp);
    free(index_temp);
}

// 排序 [ left , right ]
static void mergesort_double_with_int(double *data, int *index, int left, int right)
{
    if (left == right)
        return;
    int mid = (left + right) / 2;
    mergesort_double_with_int(data, index, left, mid);
    mergesort_double_with_int(data, index, mid + 1, right);
    merge_double_with_int(data, index, left, mid, right);
}

int PASE_Mg_choose_eigenpairs_simple(PASE_MG_SOLVER solver)
{
    OPS *gcge_ops = solver->gcge_ops;
    int mv_s[2], mv_e[2], i, j;
    int batch_size = solver->batch_size;
    int conv_nev = solver->conv_nev;
    int real_size = batch_size - conv_nev;
    int aux_nev = batch_size + solver->more_aux_nev;
    double one_double = 1.0, zero_double = 0.0, mone_double = -1.0;
    int one_int = 1;
    double *eigenvalues = solver->aux_eigenvalues;
    int nev_start = solver->current_nev_start;

    double *xT = solver->double_tmp;
    void **bh = solver->aux_B->aux_Hh;
    double *beta = solver->pre_double;
    if (solver->pc_type == PRECOND_NONE)
    {
        beta = solver->aux_B->aux_hh;
    }
    void **uH = solver->aux_sol->b_H;
    double *gamma = solver->aux_sol->aux_h;

    // (1) 处理 A 预处理
    if (solver->pc_type == PRECOND_A)
    {
        solver->precondition_solve_time -= gcge_ops->GetWtime();

        void **invA_ah = solver->aux_A->AH_inv_aux_Hh;
        int num = batch_size * aux_nev;
        dcopy(&num, gamma, &one_int, solver->aux_sol->aux_h_tmp, &one_int);
        dscal(&num, &mone_double, solver->aux_sol->aux_h_tmp, &one_int);
        mv_s[0] = 0;
        mv_e[0] = batch_size;
        mv_s[1] = 0;
        mv_e[1] = aux_nev;
        gcge_ops->MultiVecLinearComb(invA_ah, uH, 0, mv_s, mv_e,
                                     solver->aux_sol->aux_h_tmp, batch_size, &one_double, 0, gcge_ops);

        solver->precondition_solve_time += gcge_ops->GetWtime();
    }
    // (2) 处理 B 预处理
    else if (solver->pc_type == PRECOND_B || solver->pc_type == PRECOND_B_A)
    {
        solver->precondition_solve_time -= gcge_ops->GetWtime();

        void **invB_bh = solver->aux_B->AH_inv_aux_Hh;
        int num = batch_size * aux_nev;
        dcopy(&num, gamma, &one_int, solver->aux_sol->aux_h_tmp, &one_int);
        dscal(&num, &mone_double, solver->aux_sol->aux_h_tmp, &one_int);
        mv_s[0] = 0;
        mv_e[0] = batch_size;
        mv_s[1] = 0;
        mv_e[1] = aux_nev;
        gcge_ops->MultiVecLinearComb(invB_bh, uH, 0, mv_s, mv_e,
                                     solver->aux_sol->aux_h_tmp, batch_size, &one_double, 0, gcge_ops);

        solver->precondition_solve_time += gcge_ops->GetWtime();
    }

    double *tmp_double = solver->double_tmp;
    double *solution_BV, *tmp_BV;
    BVGetArray((BV)uH, &solution_BV);
    BVGetArray((BV)(solver->cg_rhs[solver->aux_coarse_level]), &tmp_BV);
    int ind;
    int length = solver->multigrid->local_size[solver->aux_coarse_level];
    for (i = 0; i < real_size; i++)
    {
        ind = i + conv_nev;
        solver->eigenvalues[conv_nev + i] = eigenvalues[ind];
        // (4.1) dense 部分
        dcopy(&batch_size, gamma + ind * batch_size, &one_int, tmp_double + i * batch_size, &one_int);
        // (4.2) BV 部分
        dcopy(&length, solution_BV + ind * length, &one_int, tmp_BV + i * length, &one_int);
    }
    int num = batch_size * real_size;
    dcopy(&num, tmp_double, &one_int, gamma + conv_nev * batch_size, &one_int);
    num = length * real_size;
    dcopy(&num, tmp_BV, &one_int, solution_BV + conv_nev * length, &one_int);
    BVRestoreArray((BV)uH, &solution_BV);
    BVRestoreArray((BV)(solver->cg_rhs[solver->aux_coarse_level]), &tmp_BV);
}

int PASE_Mg_choose_eigenpairs(PASE_MG_SOLVER solver)
{
    OPS *gcge_ops = solver->gcge_ops;
    int mv_s[2], mv_e[2], i, j;
    int batch_size = solver->batch_size;
    int conv_nev = solver->conv_nev;
    int real_size = batch_size - conv_nev;
    int aux_nev = batch_size + solver->more_aux_nev;
    double one_double = 1.0, zero_double = 0.0, mone_double = -1.0;
    int one_int = 1;
    double *eigenvalues = solver->aux_eigenvalues;
    int nev_start = solver->current_nev_start;

    double *xT = solver->double_tmp;
    // void **bh = solver->aux_B->aux_Hh;
    void **bh = solver->pre_bh;
    double *beta = solver->pre_double;
    void **uH = solver->aux_sol->b_H;
    double *gamma = solver->aux_sol->aux_h;


    // (1) 处理 A 预处理
    if (solver->pc_type == PRECOND_A)
    {
        solver->precondition_solve_time -= gcge_ops->GetWtime();

        void **invA_ah = solver->aux_A->AH_inv_aux_Hh;
        int num = batch_size * aux_nev;
        dcopy(&num, gamma, &one_int, solver->aux_sol->aux_h_tmp, &one_int);
        dscal(&num, &mone_double, solver->aux_sol->aux_h_tmp, &one_int);
        mv_s[0] = 0;
        mv_e[0] = batch_size;
        mv_s[1] = 0;
        mv_e[1] = aux_nev;
        gcge_ops->MultiVecLinearComb(invA_ah, uH, 0, mv_s, mv_e,
                                     solver->aux_sol->aux_h_tmp, batch_size, &one_double, 0, gcge_ops);

        solver->precondition_solve_time += gcge_ops->GetWtime();
    }
    // (2) 处理 B 预处理
    else if (solver->pc_type == PRECOND_B || solver->pc_type == PRECOND_B_A)
    {
        solver->precondition_solve_time -= gcge_ops->GetWtime();

        void **invB_bh = solver->aux_B->AH_inv_aux_Hh;
        int num = batch_size * aux_nev;
        dcopy(&num, gamma, &one_int, solver->aux_sol->aux_h_tmp, &one_int);
        dscal(&num, &mone_double, solver->aux_sol->aux_h_tmp, &one_int);
        mv_s[0] = 0;
        mv_e[0] = batch_size;
        mv_s[1] = 0;
        mv_e[1] = aux_nev;
        gcge_ops->MultiVecLinearComb(invB_bh, uH, 0, mv_s, mv_e,
                                     solver->aux_sol->aux_h_tmp, batch_size, &one_double, 0, gcge_ops);

        solver->precondition_solve_time += gcge_ops->GetWtime();
    }

    // if (solver->more_aux_nev == 0)
    // return 0;

    // (1) X^T = inv(beta) * bh^T * uh + gamma
    mv_s[0] = conv_nev;
    mv_e[0] = batch_size;
    mv_s[1] = 0;
    mv_e[1] = aux_nev;
    gcge_ops->MultiVecInnerProd('N', bh, uH, 0, mv_s, mv_e, xT, real_size, gcge_ops);
    if (solver->aux_B->is_dense_diag)
    {
        vdInv(real_size, beta, beta);
        for (i = 0; i < aux_nev - conv_nev; i++)
        {
            for (j = 0; j < real_size; j++)
            {
                xT[i * real_size + j] *= beta[j];
            }
        }
    }
    else
    {
        int info, num = real_size * real_size;
        int *ipiv = solver->int_tmp;
        double *beta_inv = solver->double_tmp + aux_nev * batch_size;
        double *work = solver->double_tmp + (aux_nev + batch_size) * batch_size;
        char char_U = 'U', char_N = 'N', char_L = 'L';
        dcopy(&num, beta, &one_int, beta_inv, &one_int);
        int lwork = 24 * real_size;
        dsytrf(&char_U, &real_size, beta_inv, &real_size, ipiv, work, &lwork, &info);
        assert(info == 0);
        dsytri(&char_U, &real_size, beta_inv, &real_size, ipiv, work, &info);
        assert(info == 0);
        dsymm(&char_L, &char_U, &real_size, &aux_nev, &one_double, beta_inv, &real_size,
              xT, &real_size, &zero_double, xT, &real_size);
    }
    int num = batch_size * aux_nev;
    if (conv_nev == 0)
    {
        daxpy(&num, &one_double, gamma, &one_int, xT, &one_int);
    }
    else
    {
        for (i = 0; i < aux_nev; i++)
        {
            daxpy(&real_size, &one_double, gamma + i * batch_size + conv_nev, &one_int,
                  xT + i * real_size, &one_int);
        }
    }

    // (2) 计算分量 X \beta X^T
    double *result = solver->double_tmp + aux_nev * batch_size;
    double *work = solver->double_tmp + aux_nev * batch_size + aux_nev;
    char char_U = 'U', char_L = 'L';
    dsymm(&char_L, &char_U, &real_size, &aux_nev, &one_double, beta, &real_size,
          xT, &real_size, &zero_double, work, &real_size);
    double tmp;
    for (i = 0; i < aux_nev; i++)
    {
        tmp = 0.0;
        for (j = 0; j < real_size; j++)
        {
            tmp += xT[i * real_size + j] * work[i * real_size + j];
        }
        result[i] = tmp;
    }

    // (3) 按分量大小排序
    int *index = solver->int_tmp;
    for (i = 0; i < aux_nev; i++)
        index[i] = i;
    mergesort_double_with_int(result, index, 0, aux_nev - 1);
    // for (i = 0; i < aux_nev; i++)
    // {
    //     gcge_ops->Printf("分量第%d大的是第%d个特征值%g, 分量大小%g\n",
    //                      i, index[aux_nev - i - 1], eigenvalues[index[aux_nev - i - 1]], result[aux_nev - i - 1]);
    //     if (i == batch_size - conv_nev - 1)
    //     {
    //         if (result[aux_nev - i - 1] < 0.5)
    //             PASE_Error("choosing", "不足!");
    //     }
    // }

    // (4) 选出 batch_size 个之后再把它们按从小到大排列
    double *tmp_eigenvalues = solver->double_tmp;
    int *index_real = solver->int_tmp + aux_nev;
    for (i = 0; i < real_size; i++)
    {
        tmp_eigenvalues[i] = eigenvalues[index[aux_nev - i - 1]];
        index_real[i] = index[aux_nev - i - 1];
    }
    mergesort_double_with_int(tmp_eigenvalues, index_real, 0, real_size - 1);

    // (5) 选择后面 batch_size 个
    double *tmp_double = solver->double_tmp + batch_size;
    double *solution_BV, *tmp_BV;
    BVGetArray((BV)uH, &solution_BV);
    BVGetArray((BV)(solver->cg_rhs[solver->aux_coarse_level]), &tmp_BV);
    int ind;
    int length = solver->multigrid->local_size[solver->aux_coarse_level];
    for (i = 0; i < real_size; i++)
    {
        ind = index_real[i];
        // if (solver->current_batch == 1)
        gcge_ops->Printf("第%d个取了第%d个特征值%g\n", i, ind, tmp_eigenvalues[i]);
        solver->eigenvalues[nev_start + conv_nev + i] = tmp_eigenvalues[i];
        // (4.1) dense 部分
        dcopy(&batch_size, gamma + ind * batch_size, &one_int, tmp_double + i * batch_size, &one_int);
        // (4.2) BV 部分
        dcopy(&length, solution_BV + ind * length, &one_int, tmp_BV + i * length, &one_int);
    }
    num = batch_size * real_size;
    dcopy(&num, tmp_double, &one_int, gamma + conv_nev * batch_size, &one_int);
    num = length * real_size;
    dcopy(&num, tmp_BV, &one_int, solution_BV + conv_nev * length, &one_int);
    BVRestoreArray((BV)uH, &solution_BV);
    BVRestoreArray((BV)(solver->cg_rhs[solver->aux_coarse_level]), &tmp_BV);
}

int PASE_Mg_find_shift(PASE_MG_SOLVER solver, int rr)
{
    double *eigenvalues = solver->eigenvalues;
    int nev_start = solver->current_nev_start;
    int nev_end = solver->current_nev_end;
    int conv_nev = solver->conv_nev;
    conv_nev = 0;

    if (rr)
    {
        OPS *gcge_ops = solver->gcge_ops;
        int current_level = solver->aux_fine_level;
        void *A = solver->multigrid->A_array[current_level];
        void *B = solver->multigrid->B_array[current_level];
        void **solution = solver->solution[current_level];
        int mv_s[2], mv_e[2];
        int batch_size = solver->batch_size;

        double *a_mat = solver->double_tmp;
        double *b_mat = solver->double_tmp + batch_size * batch_size;
        void **tmp = solver->cg_rhs[current_level];

        // (1) 计算 V^T Ah V
        mv_s[0] = nev_start + conv_nev;
        mv_e[0] = nev_end;
        mv_s[1] = 0;
        mv_e[1] = batch_size - conv_nev;
        gcge_ops->MatDotMultiVec(A, solution, tmp, mv_s, mv_e, gcge_ops);
        gcge_ops->MultiVecInnerProd('S', solution, tmp, 0, mv_s, mv_e, a_mat, batch_size - conv_nev, gcge_ops);

        // (2) 计算 V^T Bh V
        mv_s[0] = nev_start + conv_nev;
        mv_e[0] = nev_end;
        mv_s[1] = 0;
        mv_e[1] = batch_size - conv_nev;
        gcge_ops->MatDotMultiVec(B, solution, tmp, mv_s, mv_e, gcge_ops);
        gcge_ops->MultiVecInnerProd('S', solution, tmp, 0, mv_s, mv_e, b_mat, batch_size - conv_nev, gcge_ops);

        // (3) Rayleigh Ritz
        char char_V = 'V', char_U = 'U';
        int type = 1, info;
        int real_size = batch_size - conv_nev;
        int lwork = 1 + 6 * real_size + 2 * real_size * real_size;
        double *work_dsygv = solver->double_tmp + 2 * batch_size * batch_size;
        int liwork = 3 + 5 * real_size;
        int *iwork = solver->int_tmp;
        dsygvd(&type, &char_V, &char_U, &real_size,
               a_mat, &real_size, b_mat, &real_size, eigenvalues + nev_start + conv_nev,
               work_dsygv, &lwork, iwork, &liwork, &info);

        // (4) 改变 eigenvectors
        mv_s[0] = nev_start + conv_nev;
        mv_e[0] = nev_end;
        mv_s[1] = 0;
        mv_e[1] = real_size;
        gcge_ops->MultiVecAxpby(1.0, solution, 0.0, tmp, mv_s, mv_e, gcge_ops);
        mv_s[0] = 0;
        mv_e[0] = real_size;
        mv_s[1] = nev_start + conv_nev;
        mv_e[1] = nev_end;
        gcge_ops->MultiVecLinearComb(tmp, solution, 0, mv_s, mv_e, a_mat, real_size, NULL, 0, gcge_ops);
    }

    double lambda_s = eigenvalues[nev_start];
    double lambda_e = eigenvalues[nev_end - 1];
    solver->current_shift = (lambda_s + lambda_e) / 2;
    // solver->current_shift = 543;
    // solver->current_shift = 0.0;
#if 1
    void *shift_A = solver->multigrid->A_array[solver->aux_coarse_level + 1]; // the coarse + 1
    void *BH = solver->multigrid->B_array[solver->aux_coarse_level];
    void *AH = solver->multigrid->A_array[solver->aux_coarse_level];

    Mat shift_AH = (Mat)shift_A;
    Mat shift_BH = (Mat)BH;

    double shift_invert = solver->current_shift;
    OPS *gcge_ops = solver->gcge_ops;
    gcge_ops->Printf("eig[%d]=%g, eig[%d]=%g\n", nev_start, lambda_s, nev_end, lambda_e);
    gcge_ops->Printf("=========find  shift = %f======= (%d)\n", shift_invert, rr);

    if (solver->shift_factorization != NULL)
    {
        KSPDestroy((KSP *)(&(solver->shift_factorization)));
        solver->shift_factorization = NULL;
        MatDestroy(&shift_AH);
        MatConvert((Mat)AH, MATSAME, MAT_INITIAL_MATRIX, &shift_AH);
        // MatCopy((Mat)AH, (Mat)(shift_A), DIFFERENT_NONZERO_PATTERN);
    }

    // A_H = A_H - \theta B_H
    // MatAXPY(Mat Y,PetscScalar a,Mat X,MatStructure str)  Y = a*X + Y.
    MatAXPY(shift_AH, -shift_invert, shift_BH, DIFFERENT_NONZERO_PATTERN);
    MatAssemblyBegin(shift_AH, MAT_FINAL_ASSEMBLY);
    MatAssemblyEnd(shift_AH, MAT_FINAL_ASSEMBLY);
    shift_A = (void *)(shift_AH);
    solver->multigrid->A_array[solver->aux_coarse_level + 1] = shift_A;

    /* LU for shift_AH*/
    KSP A_ksp;
    PC A_factor;
    KSPCreate(PETSC_COMM_WORLD, &A_ksp);
    KSPSetOperators(A_ksp, shift_AH, shift_AH);
    KSPSetType(A_ksp, KSPPREONLY);
    KSPGetPC(A_ksp, &A_factor);
    PCSetType(A_factor, PCLU);
    // PCFactorSetMatSolverType(A_factor, MATSOLVERMUMPS);
    PCFactorSetMatSolverType(A_factor, MATSOLVERSUPERLU_DIST);
    PCFactorSetUpMatSolverType(A_factor);
    KSPSetFromOptions(A_ksp);
    KSPSetUp(A_ksp);
    solver->shift_factorization = (void *)A_ksp;
#endif
}

static int LUfactor_Solve(PASE_MG_SOLVER solver, PASE_Matrix aux_Mat)
{
    // PASE_Mat_Output(aux_Mat, "auxA");
    KSP ksp = (KSP)(aux_Mat->factorization);
    void **sol = aux_Mat->AH_inv_aux_Hh;
    void **rhs = aux_Mat->aux_Hh;
    int batch_size = solver->batch_standard_size;
    Mat solmat, rhsmat;
    BVSetActiveColumns((BV)sol, 0, batch_size);
    BVSetActiveColumns((BV)rhs, 0, batch_size);
    BVGetMat((BV)sol, &solmat);
    BVGetMat((BV)rhs, &rhsmat);
    KSPSetMatSolveBatchSize(ksp, 1);
    KSPMatSolve(ksp, (Mat)rhsmat, (Mat)solmat); // 没有测试过单向量的情况
    BVRestoreMat((BV)sol, &solmat);
    BVRestoreMat((BV)rhs, &rhsmat);
    // MultiVec_Output(sol, 100, "invA_ah");
    return 0;
}

int PASE_Mg_set_pase_aux_matrix(PASE_MG_SOLVER solver)
{
    OPS *gcge_ops = solver->gcge_ops;
    int mv_s[2], mv_e[2];

#if PRINT_INFO
    double timer_start, timer_end;
    timer_start = gcge_ops->GetWtime();
#endif

    int i, j;
    int batch_size = solver->batch_size;
    int conv_nev = solver->conv_nev;
    int aux_nev = batch_size + solver->more_aux_nev;
    int f_level = solver->aux_fine_level, c_level = solver->aux_coarse_level;
    int nev_start = solver->current_nev_start, nev_end = solver->current_nev_end;
    void **uh = solver->solution[f_level];

    /*--    A    --*/
    // (1) 参数
    PASE_Matrix aux_A = solver->aux_A;
    aux_A->num_aux_vec = batch_size;
    void *Ah = solver->multigrid->A_array[f_level];
    void *AH = aux_A->A_H;
    void **ah = aux_A->aux_Hh;
    double *alpha = aux_A->aux_hh;
    void **Ah_uh = solver->cg_rhs[f_level];
    int nlock = 0; // 上一次计算中已经计算了的部分
    // (2) 多向量部分, 只处理 nlcok - batch_size 部分
    gcge_ops->Printf("计算 %d - %d 部分的特征对\n", nev_start + nlock, nev_end);
    mv_s[0] = nev_start + nlock;
    mv_e[0] = nev_end;
    mv_s[1] = nlock;
    mv_e[1] = batch_size;
    gcge_ops->MatDotMultiVec(Ah, uh, Ah_uh, mv_s, mv_e, gcge_ops);
    mv_s[0] = nlock;
    mv_e[0] = batch_size;
    mv_s[1] = nlock;
    mv_e[1] = batch_size;
    PASE_MULTIGRID_fromItoJ(solver->multigrid, f_level, c_level, mv_s, mv_e, Ah_uh, ah);
    // (3) 稠密部分, 算后面的 nlock - batch_size 列然后做对称化(这里应该还可以优化)
    mv_s[0] = nev_start;
    mv_e[0] = nev_end;
    mv_s[1] = nlock;
    mv_e[1] = batch_size;
    gcge_ops->MultiVecInnerProd('N', uh, Ah_uh, 0, mv_s, mv_e, alpha + nlock * batch_size, batch_size, gcge_ops);
    for (i = nlock; i < batch_size; i++)
    {
        for (j = 0; j < nlock; j++)
        {
            alpha[j * batch_size + i] = alpha[i * batch_size + j];
        }
    }
    // (4) 需要的话处理边界自由度
    if (solver->multigrid_type == PASE_GMG && solver->boundary_delete == false)
    {
        int boundary_num = solver->boundary_num;
        int local_size = solver->multigrid->local_size[c_level];
        int *boundary_index = solver->boundary_index;
        double *array = NULL;
        BV bv = (BV)ah;
        BVSetActiveColumns(bv, 0, batch_size);
        BVGetArray(bv, &array);
        for (i = 0; i < batch_size; i++)
        {
            for (j = 0; j < boundary_num; j++)
            {
                array[i * local_size + boundary_index[j]] = 0.0;
            }
        }
        BVRestoreArray(bv, &array);
    }
#if PRINT_INFO
    timer_end = gcge_ops->GetWtime();
    gcge_ops->Printf("-   setup A time : %f sec\n", timer_end - timer_start);
#endif

    /*--    B    --*/
    // (1) 参数
    PASE_Matrix aux_B = solver->aux_B;
    aux_B->num_aux_vec = batch_size;
    void *Bh = solver->multigrid->B_array[f_level];
    void *BH = aux_B->A_H;
    void **bh = aux_B->aux_Hh;
    double *beta = aux_B->aux_hh;
    void **Bh_uh = solver->cg_p[f_level];
    nlock = 0; // 上一次计算中已经计算了的部分
    void **pre_bh = solver->pre_bh;

    // (2) 多向量部分, 只处理 nlcok - batch_size 部分
    mv_s[0] = nev_start + nlock;
    mv_s[1] = nlock;
    mv_e[0] = nev_end;
    mv_e[1] = batch_size;
    gcge_ops->MatDotMultiVec(Bh, uh, Bh_uh, mv_s, mv_e, gcge_ops);
    mv_s[0] = nlock;
    mv_e[0] = batch_size;
    mv_s[1] = nlock;
    mv_e[1] = batch_size;
    PASE_MULTIGRID_fromItoJ(solver->multigrid, f_level, c_level, mv_s, mv_e, Bh_uh, bh);
    // (3) 稠密部分, 算后面的 nlock - batch_size 列然后做对称化(这里应该还可以优化)
    mv_s[0] = nev_start;
    mv_e[0] = nev_end;
    mv_s[1] = nlock;
    mv_e[1] = batch_size;
    gcge_ops->MultiVecInnerProd('N', uh, Bh_uh, 0, mv_s, mv_e, beta + nlock * batch_size, batch_size, gcge_ops);
    for (i = nlock; i < batch_size; i++)
    {
        for (j = 0; j < nlock; j++)
        {
            beta[j * batch_size + i] = beta[i * batch_size + j];
        }
    }

    // (4) 需要的话处理边界自由度
    if (solver->multigrid_type == PASE_GMG && solver->boundary_delete == false)
    {
        int boundary_num = solver->boundary_num;
        int local_size = solver->multigrid->local_size[c_level];
        int *boundary_index = solver->boundary_index;
        double *array = NULL;
        BV bv = (BV)bh;
        BVSetActiveColumns(bv, 0, batch_size);
        BVGetArray(bv, &array);
        for (i = 0; i < batch_size; i++)
        {
            for (j = 0; j < boundary_num; j++)
            {
                array[i * local_size + boundary_index[j]] = 0.0;
            }
        }
        BVRestoreArray(bv, &array);
    }
#if PRINT_INFO
    timer_end = gcge_ops->GetWtime();
    gcge_ops->Printf("-   setup B time : %f sec\n", timer_end - timer_start);
#endif

    double p_one = 1.0, m_one = -1.0;
    int i_one = 1;
    /* A precondition */
    if (solver->pc_type == PRECOND_A)
    {
        solver->precondition_solve_time -= gcge_ops->GetWtime();
#if PRINT_INFO
        timer_start = gcge_ops->GetWtime();
        gcge_ops->Printf("-   A precondition [ON]\n");
#endif
#if 0
        double shift_invert = solver->current_shift;
        double beta1 = 1.0, alpha1 = -shift_invert;
        //A = A - \theta B
        PASE_OPS *pase_ops;
        pase_ops = solver->pase_ops;
        pase_ops->MatAxpby(alpha1, aux_B, beta1, aux_A, pase_ops);

        aux_A->factorization = solver->shift_factorization;
#else

        double shift_invert = solver->current_shift;
        aux_A->A_H = solver->multigrid->A_array[solver->aux_coarse_level + 1]; // AH
        shift_invert *= -1.0;
        int num1 = batch_size * batch_size;
        daxpy(&num1, &shift_invert, beta, &i_one, alpha, &i_one); // alpha
        mv_s[0] = 0;
        mv_e[0] = batch_size;
        mv_s[1] = 0;
        mv_e[1] = batch_size;
        gcge_ops->MultiVecAxpby(shift_invert, bh, 1.0, ah, mv_s, mv_e, gcge_ops); // ah

        aux_A->factorization = solver->shift_factorization; // find_shift 得到的
#endif

        // (-1) beta 保存下来
        int num = batch_size * batch_size;
        if (conv_nev == 0)
        {
            dcopy(&num, beta, &i_one, solver->pre_double, &i_one);
        }
        else
        {
            num = batch_size - conv_nev;
            for (i = 0; i < batch_size - conv_nev; i++)
            {
                dcopy(&num, beta + (i + conv_nev) * batch_size + conv_nev, &i_one,
                      solver->pre_double + i * num, &i_one);
            }
        }

        // (0) 求解 inv(AH)ah
        LUfactor_Solve(solver, aux_A);
        void **invA_ah = aux_A->AH_inv_aux_Hh;

        // (1) 处理矩阵 A 的稠密部分: alpha -= ah^T * inv(AH) * ah
        aux_A->is_diag = 1;
        double *ahT_invA_ah = aux_A->aux_hh_inv;
        mv_s[0] = 0;
        mv_e[0] = batch_size;
        mv_s[1] = 0;
        mv_e[1] = batch_size;
        gcge_ops->MultiVecInnerProd('S', ah, invA_ah, 0, mv_s, mv_e, ahT_invA_ah, batch_size, gcge_ops);
        num = batch_size * batch_size;
        daxpy(&num, &m_one, ahT_invA_ah, &i_one, alpha, &i_one);

        double *invalpha = aux_A->aux_hh_inv;
        dcopy(&num, alpha, &i_one, invalpha, &i_one);

        int info;
        int *ipiv = solver->int_tmp;
        double *WORK = solver->double_tmp;
        dgetrf(&batch_size, &batch_size, invalpha, &batch_size, ipiv, &info);
        assert(info == 0);
        dgetri(&batch_size, invalpha, &batch_size, ipiv, WORK, &num, &info);
        assert(info == 0);
        // (2) 开始处理 B
        // 会用 B 的 AH_inv_aux_Hh 来记录 BH * inv(AH) * ah
        void **B_invA_ah = aux_B->AH_inv_aux_Hh;
        gcge_ops->MatDotMultiVec(BH, invA_ah, B_invA_ah, mv_s, mv_e, gcge_ops);

        // (3) 处理 B 的稠密部分: 计算 [ inv(AH) * ah ]^T * [ BH * inv(AH) * ah ]
        // 会用 B 的 AH_inv_aux_Hh 来记录这个值
        double *ahT_invA_B_invA_ah = aux_B->aux_hh_inv;
        gcge_ops->MultiVecInnerProd('S', invA_ah, B_invA_ah, 0, mv_s, mv_e,
                                    ahT_invA_B_invA_ah, batch_size, gcge_ops);

        // (4) 处理 B 的稠密部分: 计算 bh^T * inv(AH) * ah
        double *bhT_invA_ah = solver->double_tmp;
        gcge_ops->MultiVecInnerProd('N', bh, invA_ah, 0, mv_s, mv_e,
                                    bhT_invA_ah, batch_size, gcge_ops);

        // (5) 改变 B 的多向量部分 bh -= BH * inv(AH) * ah
        gcge_ops->MultiVecAxpby(1.0, bh, 0.0, pre_bh, mv_s, mv_e, gcge_ops);
        gcge_ops->MultiVecAxpby(-1.0, B_invA_ah, 1.0, bh, mv_s, mv_e, gcge_ops);

        // (6) 改变 B 的稠密部分
        // beta += [ inv(AH) * ah ]^T * [ BH * inv(AH) * ah ] - bh^T * inv(AH) * ah - ah^T * inv(AH) * bh
        daxpy(&num, &m_one, bhT_invA_ah, &i_one, ahT_invA_B_invA_ah, &i_one);
        for (i = 0; i < batch_size; i++)
        {
            for (j = 0; j < batch_size; j++)
            {
                ahT_invA_B_invA_ah[i * batch_size + j] -= bhT_invA_ah[j * batch_size + i];
            }
        }
        daxpy(&num, &p_one, ahT_invA_B_invA_ah, &i_one, beta, &i_one);

        // (4) 处理解向量的部分
        gcge_ops->MultiVecAxpby(1.0, invA_ah, 0.0, solver->aux_sol->b_H, mv_s, mv_e, gcge_ops);

        solver->precondition_solve_time += gcge_ops->GetWtime();
#if PRINT_INFO
        timer_end = gcge_ops->GetWtime();
        gcge_ops->Printf("-   A precondition time : %f sec\n", timer_end - timer_start);
#endif
    }
    else if (solver->pc_type == PRECOND_B)
    {
        solver->precondition_solve_time -= gcge_ops->GetWtime();
#if PRINT_INFO
        timer_start = gcge_ops->GetWtime();
        gcge_ops->Printf("-   B precondition [ON]\n");
#endif
        // (-1) beta 保存下来
        int num = batch_size * batch_size;
        if (conv_nev == 0)
        {
            dcopy(&num, beta, &i_one, solver->pre_double, &i_one);
        }
        else
        {
            num = batch_size - conv_nev;
            for (i = 0; i < batch_size - conv_nev; i++)
            {
                dcopy(&num, beta + (i + conv_nev) * batch_size + conv_nev, &i_one,
                      solver->pre_double + i * num, &i_one);
            }
        }

        // (0) 求解 inv(BH)bh
        LUfactor_Solve(solver, aux_B);
        void **invB_bh = aux_B->AH_inv_aux_Hh;

        // (1) 处理矩阵 B 的稠密部分: beta -= bh^T * inv(BH) * bh
        aux_B->is_diag = 1;
        double *bhT_invB_bh = aux_B->aux_hh_inv;
        mv_s[0] = 0;
        mv_e[0] = batch_size;
        mv_s[1] = 0;
        mv_e[1] = batch_size;
        gcge_ops->MultiVecInnerProd('S', bh, invB_bh, 0, mv_s, mv_e, bhT_invB_bh, batch_size, gcge_ops);
        num = batch_size * batch_size;
        daxpy(&num, &m_one, bhT_invB_bh, &i_one, beta, &i_one);

        // (2) 开始处理 A
        // 会用 A 的 AH_inv_aux_Hh 来记录 AH * inv(BH) * bh
        void **A_invB_bh = aux_A->AH_inv_aux_Hh;
        gcge_ops->MatDotMultiVec(AH, invB_bh, A_invB_bh, mv_s, mv_e, gcge_ops);

        // (3) 处理 A 的稠密部分: 计算 [ inv(BH) * bh ]^T * [ AH * inv(BH) * bh ]
        // 会用 A 的 AH_inv_aux_Hh 来记录这个值
        double *bhT_invB_A_invB_bh = aux_A->aux_hh_inv;
        gcge_ops->MultiVecInnerProd('S', invB_bh, A_invB_bh, 0, mv_s, mv_e,
                                    bhT_invB_A_invB_bh, batch_size, gcge_ops);

        // (4) 处理 A 的稠密部分: 计算 ah^T * inv(BH) * bh
        double *ahT_invB_bh = solver->double_tmp;
        gcge_ops->MultiVecInnerProd('N', ah, invB_bh, 0, mv_s, mv_e,
                                    ahT_invB_bh, batch_size, gcge_ops);

        // (5) 改变 A 的多向量部分 ah -= AH * inv(BH) * bh
        gcge_ops->MultiVecAxpby(-1.0, A_invB_bh, 1.0, ah, mv_s, mv_e, gcge_ops);

        // (6) 改变 A 的稠密部分
        // alpha += [ inv(BH) * bh ]^T * [ AH * inv(BH) * bh ] - ah^T * inv(BH) * bh - bh^T * inv(BH) * ah
        daxpy(&num, &m_one, ahT_invB_bh, &i_one, bhT_invB_A_invB_bh, &i_one);
        for (i = 0; i < batch_size; i++)
        {
            for (j = 0; j < batch_size; j++)
            {
                bhT_invB_A_invB_bh[i * batch_size + j] -= ahT_invB_bh[j * batch_size + i];
            }
        }
        daxpy(&num, &p_one, bhT_invB_A_invB_bh, &i_one, alpha, &i_one);

        // (4) 处理解向量的部分
        gcge_ops->MultiVecAxpby(1.0, invB_bh, 0.0, solver->aux_sol->b_H, mv_s, mv_e, gcge_ops);

        solver->precondition_solve_time += gcge_ops->GetWtime();
#if PRINT_INFO
        timer_end = gcge_ops->GetWtime();
        gcge_ops->Printf("-   B precondition time : %f sec\n", timer_end - timer_start);
#endif
    }
    else if (solver->pc_type == PRECOND_B_A)
    {
        solver->precondition_solve_time -= gcge_ops->GetWtime();
#if PRINT_INFO
        timer_start = gcge_ops->GetWtime();
        gcge_ops->Printf("-   B & A precondition [ON]\n");
#endif
        // (-1) beta 保存下来
        int num = batch_size * batch_size;
        if (conv_nev == 0)
        {
            dcopy(&num, beta, &i_one, solver->pre_double, &i_one);
        }
        else
        {
            num = batch_size - conv_nev;
            for (i = 0; i < batch_size - conv_nev; i++)
            {
                dcopy(&num, beta + (i + conv_nev) * batch_size + conv_nev, &i_one,
                      solver->pre_double + i * num, &i_one);
            }
        }

        // (0) 求解 inv(BH)bh
        LUfactor_Solve(solver, aux_B);
        void **invB_bh = aux_B->AH_inv_aux_Hh;

        // (1) 处理矩阵 B 的稠密部分: beta -= bh^T * inv(BH) * bh
        aux_B->is_diag = 1;
        double *bhT_invB_bh = aux_B->aux_hh_inv;
        mv_s[0] = 0;
        mv_e[0] = batch_size;
        mv_s[1] = 0;
        mv_e[1] = batch_size;
        gcge_ops->MultiVecInnerProd('S', bh, invB_bh, 0, mv_s, mv_e, bhT_invB_bh, batch_size, gcge_ops);
        num = batch_size * batch_size;
        daxpy(&num, &m_one, bhT_invB_bh, &i_one, beta, &i_one);

        // (2) 开始处理 A
        // 会用 A 的 AH_inv_aux_Hh 来记录 AH * inv(BH) * bh
        void **A_invB_bh = aux_A->AH_inv_aux_Hh;
        gcge_ops->MatDotMultiVec(AH, invB_bh, A_invB_bh, mv_s, mv_e, gcge_ops);

        // (3) 处理 A 的稠密部分: 计算 [ inv(BH) * bh ]^T * [ AH * inv(BH) * bh ]
        // 会用 A 的 AH_inv_aux_Hh 来记录这个值
        double *bhT_invB_A_invB_bh = aux_A->aux_hh_inv;
        gcge_ops->MultiVecInnerProd('S', invB_bh, A_invB_bh, 0, mv_s, mv_e,
                                    bhT_invB_A_invB_bh, batch_size, gcge_ops);

        // (4) 处理 A 的稠密部分: 计算 ah^T * inv(BH) * bh
        double *ahT_invB_bh = solver->double_tmp;
        gcge_ops->MultiVecInnerProd('N', ah, invB_bh, 0, mv_s, mv_e,
                                    ahT_invB_bh, batch_size, gcge_ops);

        // (5) 改变 A 的多向量部分 ah -= AH * inv(BH) * bh
        gcge_ops->MultiVecAxpby(-1.0, A_invB_bh, 1.0, ah, mv_s, mv_e, gcge_ops);

        // (6) 改变 A 的稠密部分
        // alpha += [ inv(BH) * bh ]^T * [ AH * inv(BH) * bh ] - ah^T * inv(BH) * bh - bh^T * inv(BH) * ah
        daxpy(&num, &m_one, ahT_invB_bh, &i_one, bhT_invB_A_invB_bh, &i_one);
        for (i = 0; i < batch_size; i++)
        {
            for (j = 0; j < batch_size; j++)
            {
                bhT_invB_A_invB_bh[i * batch_size + j] -= ahT_invB_bh[j * batch_size + i];
            }
        }
        daxpy(&num, &p_one, bhT_invB_A_invB_bh, &i_one, alpha, &i_one);

        // (4) 处理解向量的部分
        gcge_ops->MultiVecAxpby(1.0, invB_bh, 0.0, solver->aux_sol->b_H, mv_s, mv_e, gcge_ops);
        // 至此 B成为一个块对角矩阵

#if 1
        // shift_invert
        //(1) update A_H : aux_A->AH = shift_AH
        aux_A->A_H = solver->multigrid->A_array[solver->aux_coarse_level + 1];

        //(2) update \alpha : \alpha = \alpha - \theta \beta
        double shift_invert = solver->current_shift;
        shift_invert *= -1.0;
        daxpy(&num, &shift_invert, beta, &i_one, alpha, &i_one);

        //(3)update fac for shift_A_H for gcge
        aux_A->factorization = solver->shift_factorization;
#endif

        // (5.1) 处理后面 GCGE 内需要调用的数据
        LUfactor_Solve(solver, aux_A);
#if 0
        KSP A_ksp;
        PC A_factor;
        KSPCreate(PETSC_COMM_WORLD, &A_ksp);
        KSPSetOperators(A_ksp, aux_A->A_H, aux_A->A_H);
        KSPSetType(A_ksp, KSPPREONLY);
        KSPGetPC(A_ksp, &A_factor);
        PCSetType(A_factor, PCLU);
        // PCFactorSetMatSolverType(A_factor, MATSOLVERMUMPS);
        PCFactorSetMatSolverType(A_factor, MATSOLVERSUPERLU_DIST);
        PCFactorSetUpMatSolverType(A_factor);
        KSPSetFromOptions(A_ksp);
        KSPSetUp(A_ksp);
        aux_A->factorization = A_ksp;
#endif
        double *new_alpha = aux_A->aux_hh_inv;
        void **AH_inv_ah = aux_A->AH_inv_aux_Hh;
        mv_s[0] = 0;
        mv_e[0] = batch_size;
        mv_s[1] = 0;
        mv_e[1] = batch_size;
        gcge_ops->MultiVecInnerProd('S', ah, AH_inv_ah, 0, mv_s, mv_e, new_alpha, batch_size, gcge_ops);
        dscal(&num, &m_one, new_alpha, &i_one);
        daxpy(&num, &p_one, alpha, &i_one, new_alpha, &i_one);
        A_pre = 1;

#if DIRECT
        // compute alpha^{-1}
        int info;
        int *ipiv = solver->int_tmp;
        double *WORK = solver->double_tmp;
        dgetrf(&batch_size, &batch_size, new_alpha, &batch_size, ipiv, &info);
        assert(info == 0);
        dgetri(&batch_size, new_alpha, &batch_size, ipiv, WORK, &num, &info);
        assert(info == 0);
#endif

        solver->precondition_solve_time += gcge_ops->GetWtime();
#if PRINT_INFO
        timer_end = gcge_ops->GetWtime();
        gcge_ops->Printf("-   B & A precondition time : %f sec\n", timer_end - timer_start);
#endif
    }
#if 0
    // 如果需要做谱分解
    if (solver->pc_eigendcp)
    {
        if (solver->pc_type != PRECOND_B && solver->pc_type != PRECOND_B_A)
        {
            PASE_Error("PASE_Mg_set_pase_aux_matrix", "eigendecomposition only when pc_type = B / B_A");
        }
        aux_B->is_dense_diag = 1;
        // (1) 进行 eigendecomposition
        char char_V = 'V', char_A = 'A', char_U = 'U';
        double abstol = 1e-16;
        double *eigenvalues = solver->double_tmp;
        double *eigenvectors = solver->pre_double;
        double *work_double = eigenvectors + aux_size * aux_size;
        int *work_int = solver->int_tmp;
        int info, lwork = 8 * aux_size;
        dsyevx(&char_V, &char_A, &char_U, &aux_size, beta, &aux_size,
               NULL, NULL, NULL, NULL, &abstol, &aux_size, eigenvalues,
               eigenvectors, &aux_size,
               eigenvectors + aux_size * aux_size, &lwork,
               work_int, work_int + 5 * aux_size, &info);
        assert(info == 0);
        int rank = -1;
        MPI_Comm_rank(PASE_COMM_WORLD, &rank);
        if (rank == 0)
        {
            for (i = 0; i < aux_size; i++)
            {
                printf("eig[%d]=%g\n", i, eigenvalues[i]);
            }
        }

        // (2) 改变 B 的稠密部分
        int one_int = 1;
        dcopy(&aux_size, eigenvalues, &one_int, beta, &one_int);

        // (3) 改变 u 的稠密部分 (这里只需要改变 conv_nev - batch_size 即可)
        char char_T = 'T', char_N = 'N';
        double one_double = 1.0, zero_double = 0.0;
        double *gamma = solver->aux_sol->aux_h + conv_nev * aux_size;
        dgemm(&char_T, &char_N, &aux_size, &aux_size, &aux_size,
              &one_double, eigenvalues, &aux_size,
              gamma, &aux_size,
              &zero_double, gamma, &aux_size);

        // (4) 改变 A 的稠密部分
        gcge_ops->lapack_ops->DenseMatQtAP('U', 'N', aux_size, aux_size, aux_size, aux_size, 1.0,
                                           eigenvalues, aux_size, alpha, aux_size, eigenvalues, aux_size, 0.0,
                                           alpha, aux_size, solver->double_tmp);

        // (5) 改变 A 的多向量部分
        mv_s[0] = 0;
        mv_e[0] = aux_size;
        mv_s[1] = 0;
        mv_e[1] = aux_size;
        gcge_ops->MultiVecLinearComb(ah, ah, 0, mv_s, mv_e, eigenvectors, aux_size, NULL, 0, gcge_ops);
    }
#endif

    solver->nlock_auxmat_A = solver->conv_nev;
    solver->nlock_auxmat_B = solver->conv_nev;
}

int PASE_Mg_set_pase_aux_matrix_simple(PASE_MG_SOLVER solver)
{
    OPS *gcge_ops = solver->gcge_ops;
    int mv_s[2], mv_e[2];

#if PRINT_INFO
    double timer_start, timer_end;
    timer_start = gcge_ops->GetWtime();
#endif

    int i, j;
    int batch_size = solver->batch_size;
    int conv_nev = solver->conv_nev;
    int aux_nev = batch_size + solver->more_aux_nev;
    int f_level = solver->aux_fine_level, c_level = solver->aux_coarse_level;
    int nev_start = solver->current_nev_start, nev_end = solver->current_nev_end;
    void **uh = solver->solution[f_level];

    /*--    A    --*/
    // (1) 参数
    PASE_Matrix aux_A = solver->aux_A;
    aux_A->num_aux_vec = batch_size;
    void *Ah = solver->multigrid->A_array[f_level];
    void *AH = aux_A->A_H;
    void **ah = aux_A->aux_Hh;
    double *alpha = aux_A->aux_hh;
    void **Ah_uh = solver->cg_rhs[f_level];
    int nlock = 0; // 上一次计算中已经计算了的部分
    // (2) 多向量部分, 只处理 nlcok - batch_size 部分
    gcge_ops->Printf("计算 %d - %d 部分的特征对\n", nev_start + nlock, nev_end);
    mv_s[0] = nev_start + nlock;
    mv_e[0] = nev_end;
    mv_s[1] = nlock;
    mv_e[1] = batch_size;
    gcge_ops->MatDotMultiVec(Ah, uh, Ah_uh, mv_s, mv_e, gcge_ops);
    mv_s[0] = nlock;
    mv_e[0] = batch_size;
    mv_s[1] = nlock;
    mv_e[1] = batch_size;
    PASE_MULTIGRID_fromItoJ(solver->multigrid, f_level, c_level, mv_s, mv_e, Ah_uh, ah);
    // (3) 稠密部分, 算后面的 nlock - batch_size 列然后做对称化(这里应该还可以优化)
    mv_s[0] = nev_start;
    mv_e[0] = nev_end;
    mv_s[1] = nlock;
    mv_e[1] = batch_size;
    gcge_ops->MultiVecInnerProd('N', uh, Ah_uh, 0, mv_s, mv_e, alpha + nlock * batch_size, batch_size, gcge_ops);
    for (i = nlock; i < batch_size; i++)
    {
        for (j = 0; j < nlock; j++)
        {
            alpha[j * batch_size + i] = alpha[i * batch_size + j];
        }
    }
    // (4) 需要的话处理边界自由度
    if (solver->multigrid_type == PASE_GMG && solver->boundary_delete == false)
    {
        int boundary_num = solver->boundary_num;
        int local_size = solver->multigrid->local_size[c_level];
        int *boundary_index = solver->boundary_index;
        double *array = NULL;
        BV bv = (BV)ah;
        BVSetActiveColumns(bv, 0, batch_size);
        BVGetArray(bv, &array);
        for (i = 0; i < batch_size; i++)
        {
            for (j = 0; j < boundary_num; j++)
            {
                array[i * local_size + boundary_index[j]] = 0.0;
            }
        }
        BVRestoreArray(bv, &array);
    }
#if PRINT_INFO
    timer_end = gcge_ops->GetWtime();
    gcge_ops->Printf("-   setup A time : %f sec\n", timer_end - timer_start);
#endif

    /*--    B    --*/
    // (1) 参数
    PASE_Matrix aux_B = solver->aux_B;
    aux_B->num_aux_vec = batch_size;
    void *Bh = solver->multigrid->B_array[f_level];
    void *BH = aux_B->A_H;
    void **bh = aux_B->aux_Hh;
    double *beta = aux_B->aux_hh;
    void **Bh_uh = solver->cg_p[f_level];
    nlock = 0; // 上一次计算中已经计算了的部分

    // (2) 多向量部分, 只处理 nlcok - batch_size 部分
    mv_s[0] = nev_start + nlock;
    mv_s[1] = nlock;
    mv_e[0] = nev_end;
    mv_e[1] = batch_size;
    gcge_ops->MatDotMultiVec(Bh, uh, Bh_uh, mv_s, mv_e, gcge_ops);
    mv_s[0] = nlock;
    mv_e[0] = batch_size;
    mv_s[1] = nlock;
    mv_e[1] = batch_size;
    PASE_MULTIGRID_fromItoJ(solver->multigrid, f_level, c_level, mv_s, mv_e, Bh_uh, bh);
    // (3) 稠密部分, 算后面的 nlock - batch_size 列然后做对称化(这里应该还可以优化)
    mv_s[0] = nev_start;
    mv_e[0] = nev_end;
    mv_s[1] = nlock;
    mv_e[1] = batch_size;
    gcge_ops->MultiVecInnerProd('N', uh, Bh_uh, 0, mv_s, mv_e, beta + nlock * batch_size, batch_size, gcge_ops);
    for (i = nlock; i < batch_size; i++)
    {
        for (j = 0; j < nlock; j++)
        {
            beta[j * batch_size + i] = beta[i * batch_size + j];
        }
    }

    // (4) 需要的话处理边界自由度
    if (solver->multigrid_type == PASE_GMG && solver->boundary_delete == false)
    {
        int boundary_num = solver->boundary_num;
        int local_size = solver->multigrid->local_size[c_level];
        int *boundary_index = solver->boundary_index;
        double *array = NULL;
        BV bv = (BV)bh;
        BVSetActiveColumns(bv, 0, batch_size);
        BVGetArray(bv, &array);
        for (i = 0; i < batch_size; i++)
        {
            for (j = 0; j < boundary_num; j++)
            {
                array[i * local_size + boundary_index[j]] = 0.0;
            }
        }
        BVRestoreArray(bv, &array);
    }
#if PRINT_INFO
    timer_end = gcge_ops->GetWtime();
    gcge_ops->Printf("-   setup B time : %f sec\n", timer_end - timer_start);
#endif

    double p_one = 1.0, m_one = -1.0;
    int i_one = 1;
    /* A precondition */
    if (solver->pc_type == PRECOND_A)
    {
        solver->precondition_solve_time -= gcge_ops->GetWtime();
#if PRINT_INFO
        timer_start = gcge_ops->GetWtime();
        gcge_ops->Printf("-   A precondition [ON]\n");
#endif
        // (0) 求解 inv(AH)ah
        // PASE_Mat_Output(solver->aux_A, "auxA");
        LUfactor_Solve(solver, aux_A);
        void **invA_ah = aux_A->AH_inv_aux_Hh;
        // MultiVec_Output(invA_ah, 100, "invA_ah");

        // (1) 处理矩阵 A 的稠密部分: alpha -= ah^T * inv(AH) * ah
        aux_A->is_diag = 1;
        double *ahT_invA_ah = aux_A->aux_hh_inv;
        mv_s[0] = 0;
        mv_e[0] = batch_size;
        mv_s[1] = 0;
        mv_e[1] = batch_size;
        gcge_ops->MultiVecInnerProd('S', ah, invA_ah, 0, mv_s, mv_e, ahT_invA_ah, batch_size, gcge_ops);
        int num = batch_size * batch_size;
        daxpy(&num, &m_one, ahT_invA_ah, &i_one, alpha, &i_one);

        // (2) 开始处理 B
        // 会用 B 的 AH_inv_aux_Hh 来记录 BH * inv(AH) * ah
        void **B_invA_ah = aux_B->AH_inv_aux_Hh;
        gcge_ops->MatDotMultiVec(BH, invA_ah, B_invA_ah, mv_s, mv_e, gcge_ops);

        // (3) 处理 B 的稠密部分: 计算 [ inv(AH) * ah ]^T * [ BH * inv(AH) * ah ]
        // 会用 B 的 AH_inv_aux_Hh 来记录这个值
        double *ahT_invA_B_invA_ah = aux_B->aux_hh_inv;
        gcge_ops->MultiVecInnerProd('S', invA_ah, B_invA_ah, 0, mv_s, mv_e,
                                    ahT_invA_B_invA_ah, batch_size, gcge_ops);

        // (4) 处理 B 的稠密部分: 计算 bh^T * inv(AH) * ah
        double *bhT_invA_ah = solver->double_tmp;
        gcge_ops->MultiVecInnerProd('N', bh, invA_ah, 0, mv_s, mv_e,
                                    bhT_invA_ah, batch_size, gcge_ops);

        // (5) 改变 B 的多向量部分 bh -= BH * inv(AH) * ah
        gcge_ops->MultiVecAxpby(-1.0, B_invA_ah, 1.0, bh, mv_s, mv_e, gcge_ops);

        // (6) 改变 B 的稠密部分
        // beta += [ inv(AH) * ah ]^T * [ BH * inv(AH) * ah ] - bh^T * inv(AH) * ah - ah^T * inv(AH) * bh
        daxpy(&num, &m_one, bhT_invA_ah, &i_one, ahT_invA_B_invA_ah, &i_one);
        for (i = 0; i < batch_size; i++)
        {
            for (j = 0; j < batch_size; j++)
            {
                ahT_invA_B_invA_ah[i * batch_size + j] -= bhT_invA_ah[j * batch_size + i];
            }
        }
        daxpy(&num, &p_one, ahT_invA_B_invA_ah, &i_one, beta, &i_one);

        // (4) 处理解向量的部分
        gcge_ops->MultiVecAxpby(1.0, invA_ah, 0.0, solver->aux_sol->b_H, mv_s, mv_e, gcge_ops);

        solver->precondition_solve_time += gcge_ops->GetWtime();
#if PRINT_INFO
        timer_end = gcge_ops->GetWtime();
        gcge_ops->Printf("-   A precondition time : %f sec\n", timer_end - timer_start);
#endif
    }
    else if (solver->pc_type == PRECOND_B)
    {
        solver->precondition_solve_time -= gcge_ops->GetWtime();
#if PRINT_INFO
        timer_start = gcge_ops->GetWtime();
        gcge_ops->Printf("-   B precondition [ON]\n");
#endif

        // (0) 求解 inv(BH)bh
        LUfactor_Solve(solver, aux_B);
        void **invB_bh = aux_B->AH_inv_aux_Hh;

        // (1) 处理矩阵 B 的稠密部分: beta -= bh^T * inv(BH) * bh
        aux_B->is_diag = 1;
        double *bhT_invB_bh = aux_B->aux_hh_inv;
        mv_s[0] = 0;
        mv_e[0] = batch_size;
        mv_s[1] = 0;
        mv_e[1] = batch_size;
        gcge_ops->MultiVecInnerProd('S', bh, invB_bh, 0, mv_s, mv_e, bhT_invB_bh, batch_size, gcge_ops);
        int num = batch_size * batch_size;
        daxpy(&num, &m_one, bhT_invB_bh, &i_one, beta, &i_one);

        // (2) 开始处理 A
        // 会用 A 的 AH_inv_aux_Hh 来记录 AH * inv(BH) * bh
        void **A_invB_bh = aux_A->AH_inv_aux_Hh;
        gcge_ops->MatDotMultiVec(AH, invB_bh, A_invB_bh, mv_s, mv_e, gcge_ops);

        // (3) 处理 A 的稠密部分: 计算 [ inv(BH) * bh ]^T * [ AH * inv(BH) * bh ]
        // 会用 A 的 AH_inv_aux_Hh 来记录这个值
        double *bhT_invB_A_invB_bh = aux_A->aux_hh_inv;
        gcge_ops->MultiVecInnerProd('S', invB_bh, A_invB_bh, 0, mv_s, mv_e,
                                    bhT_invB_A_invB_bh, batch_size, gcge_ops);

        // (4) 处理 A 的稠密部分: 计算 ah^T * inv(BH) * bh
        double *ahT_invB_bh = solver->double_tmp;
        gcge_ops->MultiVecInnerProd('N', ah, invB_bh, 0, mv_s, mv_e,
                                    ahT_invB_bh, batch_size, gcge_ops);

        // (5) 改变 A 的多向量部分 ah -= AH * inv(BH) * bh
        gcge_ops->MultiVecAxpby(-1.0, A_invB_bh, 1.0, ah, mv_s, mv_e, gcge_ops);

        // (6) 改变 A 的稠密部分
        // alpha += [ inv(BH) * bh ]^T * [ AH * inv(BH) * bh ] - ah^T * inv(BH) * bh - bh^T * inv(BH) * ah
        daxpy(&num, &m_one, ahT_invB_bh, &i_one, bhT_invB_A_invB_bh, &i_one);
        for (i = 0; i < batch_size; i++)
        {
            for (j = 0; j < batch_size; j++)
            {
                bhT_invB_A_invB_bh[i * batch_size + j] -= ahT_invB_bh[j * batch_size + i];
            }
        }
        daxpy(&num, &p_one, bhT_invB_A_invB_bh, &i_one, alpha, &i_one);

        // (4) 处理解向量的部分
        gcge_ops->MultiVecAxpby(1.0, invB_bh, 0.0, solver->aux_sol->b_H, mv_s, mv_e, gcge_ops);

        solver->precondition_solve_time += gcge_ops->GetWtime();
#if PRINT_INFO
        timer_end = gcge_ops->GetWtime();
        gcge_ops->Printf("-   B precondition time : %f sec\n", timer_end - timer_start);
#endif
    }
    else if (solver->pc_type == PRECOND_B_A)
    {
        solver->precondition_solve_time -= gcge_ops->GetWtime();
#if PRINT_INFO
        timer_start = gcge_ops->GetWtime();
        gcge_ops->Printf("-   B & A precondition [ON]\n");
#endif

        // (0) 求解 inv(BH)bh
        LUfactor_Solve(solver, aux_B);
        void **invB_bh = aux_B->AH_inv_aux_Hh;

        // (1) 处理矩阵 B 的稠密部分: beta -= bh^T * inv(BH) * bh
        aux_B->is_diag = 1;
        double *bhT_invB_bh = aux_B->aux_hh_inv;
        mv_s[0] = 0;
        mv_e[0] = batch_size;
        mv_s[1] = 0;
        mv_e[1] = batch_size;
        gcge_ops->MultiVecInnerProd('S', bh, invB_bh, 0, mv_s, mv_e, bhT_invB_bh, batch_size, gcge_ops);
        int num = batch_size * batch_size;
        daxpy(&num, &m_one, bhT_invB_bh, &i_one, beta, &i_one);

        // (2) 开始处理 A
        // 会用 A 的 AH_inv_aux_Hh 来记录 AH * inv(BH) * bh
        void **A_invB_bh = aux_A->AH_inv_aux_Hh;
        gcge_ops->MatDotMultiVec(AH, invB_bh, A_invB_bh, mv_s, mv_e, gcge_ops);

        // (3) 处理 A 的稠密部分: 计算 [ inv(BH) * bh ]^T * [ AH * inv(BH) * bh ]
        // 会用 A 的 AH_inv_aux_Hh 来记录这个值
        double *bhT_invB_A_invB_bh = aux_A->aux_hh_inv;
        gcge_ops->MultiVecInnerProd('S', invB_bh, A_invB_bh, 0, mv_s, mv_e,
                                    bhT_invB_A_invB_bh, batch_size, gcge_ops);

        // (4) 处理 A 的稠密部分: 计算 ah^T * inv(BH) * bh
        double *ahT_invB_bh = solver->double_tmp;
        gcge_ops->MultiVecInnerProd('N', ah, invB_bh, 0, mv_s, mv_e,
                                    ahT_invB_bh, batch_size, gcge_ops);

        // (5) 改变 A 的多向量部分 ah -= AH * inv(BH) * bh
        gcge_ops->MultiVecAxpby(-1.0, A_invB_bh, 1.0, ah, mv_s, mv_e, gcge_ops);

        // (6) 改变 A 的稠密部分
        // alpha += [ inv(BH) * bh ]^T * [ AH * inv(BH) * bh ] - ah^T * inv(BH) * bh - bh^T * inv(BH) * ah
        daxpy(&num, &m_one, ahT_invB_bh, &i_one, bhT_invB_A_invB_bh, &i_one);
        for (i = 0; i < batch_size; i++)
        {
            for (j = 0; j < batch_size; j++)
            {
                bhT_invB_A_invB_bh[i * batch_size + j] -= ahT_invB_bh[j * batch_size + i];
            }
        }
        daxpy(&num, &p_one, bhT_invB_A_invB_bh, &i_one, alpha, &i_one);

        // (4) 处理解向量的部分
        gcge_ops->MultiVecAxpby(1.0, invB_bh, 0.0, solver->aux_sol->b_H, mv_s, mv_e, gcge_ops);

        // (5) 处理后面 GCGE 内需要调用的数据
        LUfactor_Solve(solver, aux_A);
        double *new_alpha = aux_A->aux_hh_inv;
        void **AH_inv_ah = aux_A->AH_inv_aux_Hh;
        gcge_ops->MultiVecInnerProd('S', ah, AH_inv_ah, 0, mv_s, mv_e, new_alpha, batch_size, gcge_ops);
        dscal(&num, &m_one, new_alpha, &i_one);
        daxpy(&num, &p_one, alpha, &i_one, new_alpha, &i_one);
        A_pre = 1;

        solver->precondition_solve_time += gcge_ops->GetWtime();
#if PRINT_INFO
        timer_end = gcge_ops->GetWtime();
        gcge_ops->Printf("-   B & A precondition time : %f sec\n", timer_end - timer_start);
#endif
    }
    // PASE_Mat_Output(solver->aux_A, "auxA");
    // PASE_Mat_Output(solver->aux_B, "auxB");
    // PASE_MultiVec_Output(solver->aux_sol, solver->batch_size + solver->more_aux_nev, "sol");
    // MPI_Barrier(MPI_COMM_WORLD);
    // MPI_Abort(MPI_COMM_WORLD, 0);


    solver->nlock_auxmat_A = solver->conv_nev;
    solver->nlock_auxmat_B = solver->conv_nev;
}

int PASE_Mg_set_pase_aux_vector(PASE_MG_SOLVER solver)
{
    OPS *gcge_ops = solver->gcge_ops;
#if PRINT_INFO
    double timer_start, timer_end;
    timer_start = gcge_ops->GetWtime();
#endif

    int batch_size = solver->batch_size;
    int conv_nev = solver->conv_nev;
    int aux_nev = batch_size + solver->more_aux_nev;
    int f_level = solver->aux_fine_level, c_level = solver->aux_coarse_level;
    int nev_start = solver->current_nev_start, nev_end = solver->current_nev_end;

    PASE_MultiVector aux_u = solver->aux_sol;
    aux_u->num_aux_vec = batch_size;
    aux_u->num_vec = aux_nev;
    void **u = aux_u->b_H;
    double *gamma = aux_u->aux_h;

    // (1) 0 到 batch_size 个应该是( 0  I )^T
    int mv_s[2], mv_e[2];
    mv_s[0] = 0;
    mv_e[0] = batch_size;
    mv_s[1] = 0;
    mv_e[1] = batch_size;
    gcge_ops->MultiVecAxpby(0.0, u, 0.0, u, mv_s, mv_e, gcge_ops);
    int num = batch_size * batch_size;
    memset(gamma, 0.0, num * sizeof(double));
    int i;
    for (i = 0; i < batch_size; i++)
    {
        gamma[i * batch_size + i] = 1.0;
    }

    // (3) 更新 gcge 工作空间多向量的信息
    solver->aux_gcg_mv[0]->num_aux_vec = batch_size;
    solver->aux_gcg_mv[1]->num_aux_vec = batch_size;
    solver->aux_gcg_mv[2]->num_aux_vec = batch_size;
    solver->aux_gcg_mv[3]->num_aux_vec = batch_size;
    solver->aux_gcg_mv[4]->num_aux_vec = batch_size + solver->more_aux_nev;

#if PRINT_INFO
    timer_end = gcge_ops->GetWtime();
    gcge_ops->Printf("-   setup solution time : %f sec\n", timer_end - timer_start);
#endif
}

static void smoothing_bcg(PASE_MG_SOLVER solver, int max_bcg_iter)
{
    OPS *gcge_ops = solver->gcge_ops;
    int current_level = solver->aux_fine_level;
    void *A = solver->multigrid->A_array[current_level];
    void *B = solver->multigrid->B_array[current_level];
    void **solution = solver->solution[current_level];
    void **rhs = solver->cg_rhs[current_level];
    int nlock_smooth = solver->nlock_smooth;
    double *eigenvalues = solver->eigenvalues;

    double rate = 1e-2, tol = 1e-12;
    double *dbl_ws = solver->double_tmp;
    int *int_ws = solver->int_tmp;
    void **cg_space_mv[3];
    cg_space_mv[0] = solver->cg_res[current_level];
    cg_space_mv[1] = solver->cg_p[current_level];
    cg_space_mv[2] = solver->cg_w[current_level];
    MultiLinearSolverSetup_BlockPCG(max_bcg_iter, rate, tol, "abs",
                                    cg_space_mv, dbl_ws, int_ws, NULL, NULL, gcge_ops);

    int mv_s[2] = {solver->current_nev_start + nlock_smooth, nlock_smooth};
    int mv_e[2] = {solver->current_nev_end, solver->batch_size};
    assert(mv_e[0] - mv_s[0] == mv_e[1] - mv_s[1]);
    gcge_ops->MatDotMultiVec(B, solution, rhs, mv_s, mv_e, gcge_ops);
    gcge_ops->MultiVecLinearComb(NULL, rhs, 0, mv_s, mv_e, NULL, 0, eigenvalues + mv_s[0], 1, gcge_ops);
    mv_s[0] = nlock_smooth;
    mv_s[1] = solver->current_nev_start + nlock_smooth;
    mv_e[0] = solver->batch_size;
    mv_e[1] = solver->current_nev_end;
    gcge_ops->MultiLinearSolver(A, rhs, solution, mv_s, mv_e, gcge_ops);
}

int PASE_Mg_smoothing(PASE_MG_SOLVER solver, int times)
{
    OPS *gcge_ops = solver->gcge_ops;
#if PRINT_INFO
    gcge_ops->Printf("-   itr_times : %d\n", times);
    gcge_ops->Printf("-   nlock_smooth : %d\n", solver->nlock_smooth);
#endif

    if (solver->smoothing_type == PASE_BCG)
    {
#if PRINT_INFO
        gcge_ops->Printf("-   smooth_type : BCG\n");
#endif
        smoothing_bcg(solver, times);
    }

    return 0;
}

int PASE_Mg_set_up(PASE_MG_SOLVER solver, PASE_PARAMETER param)
{
    OPS *gcge_ops = solver->gcge_ops;
    solver->solver_setup_time -= MPI_Wtime();

    // (1) aux space ops
    OPS *pase_ops_to_gcge;
    OPS_Create(&pase_ops_to_gcge);
    GCGE_PASE_SetOps(pase_ops_to_gcge, solver->pase_ops);
    OPS_Setup(pase_ops_to_gcge);
    solver->pase_ops_to_gcge = pase_ops_to_gcge;

    // (2) space for eigenvalues
    int init_gcge_nev = solver->gcge_nev; // 此时这个参数应该是 (user_nev + more_batch_size) * 2
    int init_gcge_real_nev = solver->more_nev;
    solver->eigenvalues = (double *)calloc(init_gcge_nev, sizeof(double));
    // (3) multigrid set up
    PASE_MULTIGRID_Create(&(solver->multigrid), param, gcge_ops);
    solver->num_levels = solver->multigrid->num_levels;
    solver->aux_coarse_level = solver->multigrid->pase_coarse_level;
    solver->aux_fine_level = solver->multigrid->pase_fine_level;
    solver->initial_level = (solver->num_given_eigs == 0) ? solver->aux_coarse_level : solver->initial_level;

    // (4) space for initial GCGE
    // 给intial GCGE 预留求 init_gcge_nev 个特征对的位置
    int aux_coarse_level = solver->aux_coarse_level;
    int sizeV = init_gcge_nev + init_gcge_real_nev / 2;
    int size = 2 * sizeV * sizeV + 10 * sizeV +
               (init_gcge_nev + init_gcge_real_nev) + (init_gcge_real_nev * init_gcge_real_nev / 2);
    solver->double_tmp = (double *)calloc(size, sizeof(double));
    size = 6 * sizeV + 2 * (init_gcge_real_nev / 2 + 3);
    solver->int_tmp = (int *)calloc(size, sizeof(int));

    solver->aux_gcg_mv[0] = (PASE_MultiVector)malloc(sizeof(pase_MultiVector));
    MPI_Comm_dup(solver->group_comm, &((solver->aux_gcg_mv[0])->comm));
    solver->aux_gcg_mv[0]->num_aux_vec = 0;
    solver->aux_gcg_mv[0]->num_vec = 0;
    gcge_ops->MultiVecCreateByMat(&(solver->aux_gcg_mv[0]->b_H), init_gcge_nev + init_gcge_real_nev, solver->multigrid->A_array[aux_coarse_level], gcge_ops);
    solver->aux_gcg_mv[0]->aux_h = NULL;
    solver->aux_gcg_mv[0]->aux_h_tmp = NULL;

    solver->aux_gcg_mv[1] = (PASE_MultiVector)malloc(sizeof(pase_MultiVector));
    MPI_Comm_dup(solver->group_comm, &((solver->aux_gcg_mv[1])->comm));
    solver->aux_gcg_mv[1]->num_aux_vec = 0;
    solver->aux_gcg_mv[1]->num_vec = 0;
    // gcge_ops->MultiVecCreateByMat(&(solver->aux_gcg_mv[1]->b_H), init_gcge_real_nev / 2, solver->multigrid->A_array[aux_coarse_level], gcge_ops);
    gcge_ops->MultiVecCreateByMat(&(solver->aux_gcg_mv[1]->b_H), init_gcge_nev + init_gcge_real_nev, solver->multigrid->A_array[aux_coarse_level], gcge_ops);

    solver->aux_gcg_mv[1]->aux_h = NULL;
    solver->aux_gcg_mv[1]->aux_h_tmp = NULL;

    solver->aux_gcg_mv[2] = (PASE_MultiVector)malloc(sizeof(pase_MultiVector));
    MPI_Comm_dup(solver->group_comm, &((solver->aux_gcg_mv[2])->comm));
    solver->aux_gcg_mv[2]->num_aux_vec = 0;
    solver->aux_gcg_mv[2]->num_vec = 0;
    // gcge_ops->MultiVecCreateByMat(&(solver->aux_gcg_mv[2]->b_H), init_gcge_real_nev / 2, solver->multigrid->A_array[aux_coarse_level], gcge_ops);
    gcge_ops->MultiVecCreateByMat(&(solver->aux_gcg_mv[2]->b_H), init_gcge_nev + init_gcge_real_nev, solver->multigrid->A_array[aux_coarse_level], gcge_ops);

    solver->aux_gcg_mv[2]->aux_h = NULL;
    solver->aux_gcg_mv[2]->aux_h_tmp = NULL;

    solver->aux_gcg_mv[3] = (PASE_MultiVector)malloc(sizeof(pase_MultiVector));
    MPI_Comm_dup(solver->group_comm, &((solver->aux_gcg_mv[3])->comm));
    solver->aux_gcg_mv[3]->num_aux_vec = 0;
    solver->aux_gcg_mv[3]->num_vec = 0;
    gcge_ops->MultiVecCreateByMat(&(solver->aux_gcg_mv[3]->b_H), init_gcge_real_nev / 2, solver->multigrid->A_array[aux_coarse_level], gcge_ops);
    solver->aux_gcg_mv[3]->aux_h = NULL;
    solver->aux_gcg_mv[3]->aux_h_tmp = NULL;

    solver->aux_gcg_mv[4] = (PASE_MultiVector)malloc(sizeof(pase_MultiVector));
    MPI_Comm_dup(solver->group_comm, &((solver->aux_gcg_mv[4])->comm));
    solver->aux_gcg_mv[4]->num_aux_vec = 0;
    solver->aux_gcg_mv[4]->num_vec = 0;
    gcge_ops->MultiVecCreateByMat(&(solver->aux_gcg_mv[4]->b_H), init_gcge_nev + init_gcge_real_nev, solver->multigrid->A_array[aux_coarse_level], gcge_ops);
    solver->aux_gcg_mv[4]->aux_h = NULL;
    solver->aux_gcg_mv[4]->aux_h_tmp = NULL;

    BV *solution = (BV *)malloc(solver->num_levels * sizeof(BV));
    BV *cg_rhs = (BV *)malloc(solver->num_levels * sizeof(BV));
    BV *cg_res = (BV *)malloc(solver->num_levels * sizeof(BV));
    BV *cg_p = (BV *)malloc((solver->num_levels + 1) * sizeof(BV));
    BV *cg_w = (BV *)malloc(solver->num_levels * sizeof(BV));
    int idx_level;
    for (idx_level = 0; idx_level < solver->num_levels; idx_level++)
    {
        solution[idx_level] = NULL;
        cg_rhs[idx_level] = NULL;
        cg_res[idx_level] = NULL;
        cg_p[idx_level] = NULL;
        cg_w[idx_level] = NULL;
    }
    cg_p[solver->num_levels] = NULL;
    // 给 initial GCGE 的解向量预留空间先存在本层
    gcge_ops->MultiVecCreateByMat((void ***)(&(solution[solver->initial_level])), init_gcge_nev,
                                  (void **)solver->multigrid->A_array[solver->initial_level], gcge_ops);
    solver->solution = (void ***)solution, solver->multigrid->solution = (void ***)solution;
    solver->cg_rhs = (void ***)cg_rhs, solver->multigrid->cg_rhs = (void ***)cg_rhs;
    solver->cg_res = (void ***)cg_res, solver->multigrid->cg_res = (void ***)cg_res;
    solver->cg_p = (void ***)cg_p, solver->multigrid->cg_p = (void ***)cg_p;
    solver->cg_w = (void ***)cg_w, solver->multigrid->cg_w = (void ***)cg_w;

    solver->solver_setup_time += MPI_Wtime();
#if PRINT_INFO
    if (solver->print_level > 0)
    {
        PASE_Mg_print_param(solver);
    }
#endif

    if (!solver->num_given_eigs)
    {
#if PRINT_INFO
        gcge_ops->Printf("\n-------- initial vecs ----------\n");
#endif
        solver->get_initvec_time -= gcge_ops->GetWtime();
        PASE_Direct_solve(solver);
        solver->get_initvec_time += gcge_ops->GetWtime();
#if PRINT_INFO
        gcge_ops->Printf("\n[direct solve] get initial vecs time : %f sec.\n", solver->get_initvec_time);
        gcge_ops->Printf("-------------------------------\n\n");
#endif
    }

    solver->solver_setup_time -= MPI_Wtime();
    // (5) space for PASE cycle
    // (5.1) cg_res (会作为插值的中间站所以每层都要有, 大小为 batch_size)
    // 粗层上会作为 aux_A 的 AH_inv_aux_Hh 部分
    for (idx_level = solver->aux_fine_level; idx_level < solver->aux_coarse_level + 1; idx_level++)
    {
        gcge_ops->MultiVecCreateByMat((void ***)(&(cg_res[idx_level])), solver->batch_size,
                                      (void **)solver->multigrid->A_array[idx_level], gcge_ops);
    }
    // (5.2) solution
    int aux_nev = solver->batch_size + solver->more_aux_nev; // 每次 aux_gcge 实际会需要计算的特征对个数
    int gcge_nev = solver->gcge_nev;                         // 每次 aux_gcge 实际会需要计算的特征对个数 * 2
    solver->aux_eigenvalues = (double *)calloc(gcge_nev, sizeof(double));
    int level_ind;
    level_ind = solver->aux_fine_level;
    // 在细层上需要存储 real_nev + more_batch_size 个
    if (level_ind != solver->initial_level) // 如果初值不是直接在细层求得的话
        gcge_ops->MultiVecCreateByMat((void ***)(&(solution[level_ind])), solver->real_nev + solver->more_batch_size,
                                      (void **)solver->multigrid->A_array[level_ind], gcge_ops);
    solver->prolong_time -= gcge_ops->GetWtime();
    PASE_Mg_prolong_from_Solution(solver, solver->aux_fine_level);
    solver->prolong_time += gcge_ops->GetWtime();

    level_ind = solver->aux_coarse_level;
    // 在粗层上需要作为aux_sol->b_H使用, 则需要 gcge_nev 大小
    if (level_ind != solver->initial_level) // 如果不是在粗层上计算的初值
    {
        gcge_ops->MultiVecCreateByMat((void ***)(&(solution[level_ind])), gcge_nev,
                                      (void **)solver->multigrid->A_array[level_ind], gcge_ops);
        // 然后把初始层的释放掉
        gcge_ops->MultiVecDestroy((void ***)(&(solution[solver->initial_level])), solver->real_nev, gcge_ops);
        solution[solver->initial_level] = NULL;
    }
    else // 如果是在粗层上计算的初值
    {
        gcge_ops->MultiVecDestroy((void ***)(&(solution[level_ind])), 0, gcge_ops);
        solution[level_ind] = NULL;
        gcge_ops->MultiVecCreateByMat((void ***)(&(solution[level_ind])), gcge_nev,
                                      (void **)solver->multigrid->A_array[level_ind], gcge_ops);
    }

    // (5.3) double_tmp & int_tmp (这个具体多大还需要找师姐确认)
    sizeV = gcge_nev + aux_nev;
    size = 3 * sizeV * sizeV + 10 * sizeV +
           (gcge_nev + aux_nev) + (aux_nev * aux_nev / 2);
    // size = 2 * size; // 师妹 size
    solver->double_tmp = (double *)realloc(solver->double_tmp, size * sizeof(double));
    size = 6 * sizeV + 2 * (aux_nev + 3);
    solver->int_tmp = (int *)realloc(solver->int_tmp, size * sizeof(int));
    int len_tem_hrr = sizeV * sizeV + 2 * sizeV;
    solver->double_tmp_hrr = (double *)calloc(len_tem_hrr, sizeof(double));

    // (5.4) aux GCGE
    // 如果 init_gcge 求的个数和 aux_gcge 求得个数相同
    if (init_gcge_real_nev != aux_nev)
    {
        // aux_nev = batch_size + more_aux_nev
        // gcge_nev = 2 * aux_nev
        size = gcge_nev + aux_nev; // 3 *(batch_size + more_aux_nev)
        gcge_ops->MultiVecDestroy((void ***)(&(solver->aux_gcg_mv[0]->b_H)),
                                  0, gcge_ops);
        solver->aux_gcg_mv[0]->b_H = NULL;
        gcge_ops->MultiVecCreateByMat(&(solver->aux_gcg_mv[0]->b_H), size, solver->multigrid->A_array[aux_coarse_level], gcge_ops);

        gcge_ops->MultiVecDestroy((void ***)(&(solver->aux_gcg_mv[1]->b_H)),
                                  0, gcge_ops);
        gcge_ops->MultiVecDestroy((void ***)(&(solver->aux_gcg_mv[2]->b_H)),
                                  0, gcge_ops);
        gcge_ops->MultiVecDestroy((void ***)(&(solver->aux_gcg_mv[3]->b_H)),
                                  0, gcge_ops);
        gcge_ops->MultiVecDestroy((void ***)(&(solver->aux_gcg_mv[4]->b_H)),
                                  0, gcge_ops);
        solver->aux_gcg_mv[1]->b_H = NULL;
        solver->aux_gcg_mv[2]->b_H = NULL;
        solver->aux_gcg_mv[3]->b_H = NULL;
        solver->aux_gcg_mv[4]->b_H = NULL;
        gcge_ops->MultiVecCreateByMat(&(solver->aux_gcg_mv[1]->b_H), size, solver->multigrid->A_array[aux_coarse_level], gcge_ops);
        gcge_ops->MultiVecCreateByMat(&(solver->aux_gcg_mv[2]->b_H), size, solver->multigrid->A_array[aux_coarse_level], gcge_ops);
        size = aux_nev / 2;
        gcge_ops->MultiVecCreateByMat(&(solver->aux_gcg_mv[3]->b_H), size, solver->multigrid->A_array[aux_coarse_level], gcge_ops);
        gcge_ops->MultiVecCreateByMat(&(solver->aux_gcg_mv[4]->b_H), gcge_nev + aux_nev, solver->multigrid->A_array[aux_coarse_level], gcge_ops);
    }
    size = aux_nev * (gcge_nev + aux_nev);
    // gcge_ops->Printf("aux_nev = %d\n", aux_nev);
    solver->aux_gcg_mv[0]->num_aux_vec = aux_nev;
    solver->aux_gcg_mv[0]->num_vec = gcge_nev + aux_nev;
    solver->aux_gcg_mv[0]->aux_h = (double *)malloc(size * sizeof(double));
    solver->aux_gcg_mv[0]->aux_h_tmp = (double *)malloc(size * sizeof(double));

    solver->aux_gcg_mv[4]->num_aux_vec = aux_nev;
    solver->aux_gcg_mv[4]->num_vec = gcge_nev + aux_nev;
    solver->aux_gcg_mv[4]->aux_h = (double *)malloc(size * sizeof(double));
    solver->aux_gcg_mv[4]->aux_h_tmp = (double *)malloc(size * sizeof(double));

    solver->aux_gcg_mv[1]->num_aux_vec = aux_nev;
    solver->aux_gcg_mv[1]->num_vec = gcge_nev + aux_nev;
    solver->aux_gcg_mv[1]->aux_h = (double *)malloc(size * sizeof(double));
    solver->aux_gcg_mv[1]->aux_h_tmp = (double *)malloc(size * sizeof(double));

    solver->aux_gcg_mv[2]->num_aux_vec = aux_nev;
    solver->aux_gcg_mv[2]->num_vec = gcge_nev + aux_nev;
    solver->aux_gcg_mv[2]->aux_h = (double *)malloc(size * sizeof(double));
    solver->aux_gcg_mv[2]->aux_h_tmp = (double *)malloc(size * sizeof(double));

    size = aux_nev * aux_nev / 2;
    solver->aux_gcg_mv[3]->num_aux_vec = aux_nev;
    solver->aux_gcg_mv[3]->num_vec = aux_nev / 2;
    solver->aux_gcg_mv[3]->aux_h = (double *)malloc(size * sizeof(double));
    solver->aux_gcg_mv[3]->aux_h_tmp = (double *)malloc(size * sizeof(double));

    int batch_size = solver->batch_size;
    // 如果是 BCG 光滑的话, 则下面几项都可以只管 aux_fine_level 和 aux_coarse_level 两层
    if (solver->smoothing_type == PASE_BCG)
    {
        // (5.5) cg_rhs
        level_ind = solver->aux_fine_level;
        gcge_ops->MultiVecCreateByMat((void ***)(&(cg_rhs[level_ind])), batch_size,
                                      (void **)solver->multigrid->A_array[level_ind], gcge_ops);
        level_ind = solver->aux_coarse_level; // 作为 aux_A 的多向量部分
        gcge_ops->MultiVecCreateByMat((void ***)(&(cg_rhs[level_ind])), batch_size,
                                      (void **)solver->multigrid->A_array[level_ind], gcge_ops);
        // (5.4) cg_p
        level_ind = solver->aux_fine_level;
        gcge_ops->MultiVecCreateByMat((void ***)(&(cg_p[level_ind])), batch_size,
                                      (void **)solver->multigrid->A_array[level_ind], gcge_ops);
        level_ind = solver->aux_coarse_level; // 作为 aux_B 的多向量部分
        gcge_ops->MultiVecCreateByMat((void ***)(&(cg_p[level_ind])), batch_size,
                                      (void **)solver->multigrid->A_array[level_ind], gcge_ops);
        level_ind = solver->aux_coarse_level + 1;
        gcge_ops->MultiVecCreateByMat((void ***)(&(cg_p[level_ind])), batch_size,
                                      (void **)solver->multigrid->A_array[level_ind], gcge_ops);
        // (5.5) cg_w
        level_ind = solver->aux_fine_level;
        gcge_ops->MultiVecCreateByMat((void ***)(&(cg_w[level_ind])), batch_size,
                                      (void **)solver->multigrid->A_array[level_ind], gcge_ops);
        level_ind = solver->aux_coarse_level; // 作为 aux_B 的 AH_inv_aux_Hh 部分
        gcge_ops->MultiVecCreateByMat((void ***)(&(cg_w[level_ind])), batch_size,
                                      (void **)solver->multigrid->A_array[level_ind], gcge_ops);
    }
    else
        PASE_Error("PASE_Mg_set_up", "unknown smoothing type");
    // (5.6) for PC
    if (solver->pc_eigendcp)
    {
        solver->pre_double = (double *)malloc(batch_size * (2 * batch_size + 8) * sizeof(double));
    }
    else
    {
        solver->pre_double = (double *)malloc(batch_size * batch_size * sizeof(double));
    }

    // (6) aux space build
    PASE_Mg_pase_aux_matrix_create(solver);
    PASE_Mg_pase_aux_vector_create(solver);
    if (solver->pc_type == PRECOND_A || solver->pc_type == PRECOND_B ||
        solver->pc_type == PRECOND_B_A)
    {
        solver->precondition_setup_time -= MPI_Wtime();
        PASE_Mg_pase_aux_matrix_factorization(solver);
        solver->precondition_setup_time += MPI_Wtime();
    }
    solver->solver_setup_time += MPI_Wtime();
}

int PASE_Direct_solve(PASE_MG_SOLVER solver)
{
    OPS *gcge_ops = solver->gcge_ops;
    int initial_level = solver->initial_level;
    void *A = solver->multigrid->A_array[initial_level];
    void *B = solver->multigrid->B_array[initial_level];
    double *eigenvalues = solver->eigenvalues;
    void **eigenvectors = solver->solution[initial_level];

// 目前还没办法分组计算
#if 1
    /* gcg 设置 */
    int nevConv = solver->user_nev + solver->more_batch_size;
    int multiMax = 1;
    double gapMin = 1e-5;
    // int nevGiven = solver->batch_size;
    int nevGiven = 0; //
    int block_size = nevConv / 2, nevMax = 2 * nevConv;
    int nevInit = 2 * nevConv;

    if (nevConv > 50)
    {
        block_size = nevConv / 5;
        nevInit = 3 * block_size;
        nevMax = nevInit + nevConv;
    }
    if (nevConv > 300)
    {
        block_size = nevConv / 10;
        nevInit = 3 * block_size;
        nevMax = nevInit + nevConv;
    }

    nevInit = nevInit < nevMax ? nevInit : nevMax;
    int max_iter_gcg = solver->max_initial_direct_count;
    double tol_gcg[2] = {solver->initial_atol, solver->initial_rtol};
#if PRINT_INFO
    gcge_ops->Printf("[direct solve] level :\n", initial_level);
    gcge_ops->Printf("[direct solve] gcg设置:\n");
    gcge_ops->Printf("·              nevConv    = %d \n", nevConv);
    gcge_ops->Printf("·              block_size = %d \n", block_size);
    gcge_ops->Printf("·              nevMax     = %d \n", nevMax);
    gcge_ops->Printf("·              nevGiven   = %d \n", nevGiven);
    gcge_ops->Printf("·              gapMin     = %e \n", gapMin);
#endif
    if (eigenvalues == NULL)
    {
        eigenvalues = (double *)calloc(nevMax, sizeof(double));
    }
    if (eigenvectors == NULL)
    {
        gcge_ops->MultiVecCreateByMat(&eigenvectors, nevMax, A, gcge_ops);
        gcge_ops->MultiVecSetRandomValue(eigenvectors, 0, nevMax, gcge_ops);
    }
    void **gcg_mv_ws[4];
    gcg_mv_ws[0] = (void **)(solver->aux_gcg_mv[0]->b_H);
    gcg_mv_ws[1] = (void **)(solver->aux_gcg_mv[1]->b_H);
    gcg_mv_ws[2] = (void **)(solver->aux_gcg_mv[2]->b_H);
    gcg_mv_ws[3] = (void **)(solver->aux_gcg_mv[3]->b_H);

    double *dbl_ws = solver->double_tmp;
    int *int_ws = solver->int_tmp;
    // double *dbl_ws_hrr = solver->double_tmp_hrr;
    srand(0);
    EigenSolverSetup_GCG(multiMax, gapMin, nevInit, nevMax, block_size,
                         tol_gcg, max_iter_gcg, 0, 0.0, gcg_mv_ws, dbl_ws, int_ws, gcge_ops);
    int check_conv_max_num = 50;

    char initX_orth_method[8] = "mgs";
    int initX_orth_block_size = -1;
    int initX_orth_max_reorth = 2;
    double initX_orth_zero_tol = 2 * DBL_EPSILON; // 1e-12

    char compP_orth_method[8] = "mgs";
    int compP_orth_block_size = -1;
    int compP_orth_max_reorth = 2;
    double compP_orth_zero_tol = 2 * DBL_EPSILON; // 1e-12

    char compW_orth_method[8] = "mgs";
    int compW_orth_block_size = -1;
    int compW_orth_max_reorth = 2;
    double compW_orth_zero_tol = 2 * DBL_EPSILON; // 1e-12
    int compW_bpcg_max_iter = 30;
    double compW_bpcg_rate = 1e-2;
    double compW_bpcg_tol = 1e-14;
    char compW_bpcg_tol_type[8] = "abs";

    int compRR_min_num = -1;
    double compRR_min_gap = gapMin;
    double compRR_tol = 2 * DBL_EPSILON;
    EigenSolverSetParameters_GCG(check_conv_max_num,
                                 initX_orth_method, initX_orth_block_size, initX_orth_max_reorth, initX_orth_zero_tol,
                                 compP_orth_method, compP_orth_block_size, compP_orth_max_reorth, compP_orth_zero_tol,
                                 compW_orth_method, compW_orth_block_size, compW_orth_max_reorth, compW_orth_zero_tol,
                                 compW_bpcg_max_iter, compW_bpcg_rate, compW_bpcg_tol, compW_bpcg_tol_type, 0, // without shift
                                 compRR_min_num, compRR_min_gap, compRR_tol,
                                 gcge_ops);
    gcge_ops->comm = solver->group_comm;
    gcge_ops->EigenSolver(A, B, NULL, eigenvalues, eigenvectors, nevGiven, &nevConv, 0.0, gcge_ops);

    int idx;
    for (idx = 0; idx < nevConv; idx++)
    {
        // solver->init_eigenvalues[idx] = eigenvalues[idx];
        gcge_ops->Printf("init nev[%d] = %.16f\n", idx, eigenvalues[idx]);
    }
#if PRINT_INFO
    gcge_ops->Printf("[direct solve] gcg结果:\n");
    gcge_ops->Printf("·              nevConv    = %d \n", nevConv);
#endif
#endif
    // 只保留本组的部分
    if (solver->group_num != 1)
    {
        // 按照第0个进程统一一下 eigenvalues 的值
        MPI_Bcast(eigenvalues, solver->gcge_nev, MPI_DOUBLE, 0, PASE_COMM_WORLD);
        int real_nev = solver->real_nev, i;
        int nev_start = solver->nev_start, nev_end = solver->nev_end;
        // (1) 检查从 start - 1 开始重特征值情况
        // 重特征值会默认是分配给上一组算 (除了第一组之外)
        if (nev_start != 0)
        {
            for (i = nev_start - 1; i < nev_end; i++)
            {
                if (fabs(eigenvalues[i] - eigenvalues[i + 1]) / eigenvalues[i] > gapMin)
                    break;
            }
            solver->nev_start = i + 1;
        }
        // (2) 检查从 end - 1 开始重特征值情况
        // 重特征值会默认是分配给上一组算 (除了第一组之外)
        // 然后再多算 more_batch_size 个
        for (i = nev_end - 1; i < nevConv; i++)
        {
            if (fabs(eigenvalues[i] - eigenvalues[i + 1]) / eigenvalues[i] > gapMin)
                break;
        }
        solver->nev_end = i + 1;
        solver->real_nev = solver->nev_end - solver->nev_start;
        void **myeigvecs;
        gcge_ops->MultiVecCreateByMat(&myeigvecs, solver->real_nev + solver->more_batch_size, A, gcge_ops);
        int start[2] = {solver->nev_start, 0};
        int end[2] = {solver->nev_end + solver->more_batch_size, solver->real_nev + solver->more_batch_size};
        gcge_ops->MultiVecAxpby(1.0, eigenvectors, 0.0, myeigvecs, start, end, gcge_ops);
        gcge_ops->MultiVecDestroy(&eigenvectors, solver->gcge_nev, gcge_ops);
        solver->solution[initial_level] = myeigvecs;
    }
    else
    {
        solver->nev_start = 0;
        solver->nev_end = solver->user_nev;
        solver->real_nev = solver->user_nev;
    }
    // 为本组进行分批
    solver->real_batch_size = solver->batch_size < solver->real_nev ? solver->batch_size : solver->real_nev;
    solver->current_nev_start = 0;
    solver->current_nev_end = solver->real_batch_size + solver->more_batch_size;
    solver->batch_size = solver->current_nev_end;
    solver->batch_standard_size = solver->batch_size;
    solver->gcge_nev = 2 * (solver->batch_size + solver->more_aux_nev);
    solver->batch_num = (solver->real_nev + solver->real_batch_size - 1) / solver->real_batch_size;
}

int PASE_Mg_pase_aux_matrix_factorization(PASE_MG_SOLVER solver)
{
#if 0
    if (solver->pc_type == PRECOND_A || solver->pc_type == PRECOND_B_A)
#else
    if (solver->pc_type == PRECOND_A || solver->pc_type == PRECOND_B_A)
#endif
    {
        PASE_Matrix aux_A = solver->aux_A;
        KSP A_ksp;
        PC A_factor;
        MPI_Comm comm;
        PetscObjectGetComm(aux_A->A_H, &comm);
        KSPCreate(comm, &A_ksp);
        KSPSetOperators(A_ksp, aux_A->A_H, aux_A->A_H);
        KSPSetType(A_ksp, KSPPREONLY);
        KSPGetPC(A_ksp, &A_factor);
        PCSetType(A_factor, PCLU);
        // PCFactorSetMatSolverType(A_factor, MATSOLVERMUMPS);
        PCFactorSetMatSolverType(A_factor, MATSOLVERSUPERLU_DIST);
        PCFactorSetUpMatSolverType(A_factor);
        KSPSetFromOptions(A_ksp);
        KSPSetUp(A_ksp);
        aux_A->factorization = (void *)A_ksp;
    }
    if (solver->pc_type == PRECOND_B || solver->pc_type == PRECOND_B_A)
    {
        PASE_Matrix aux_B = solver->aux_B;
        KSP B_ksp;
        PC B_factor;
        MPI_Comm comm;
        PetscObjectGetComm(aux_B->A_H, &comm);
        KSPCreate(comm, &B_ksp);
        KSPSetOperators(B_ksp, aux_B->A_H, aux_B->A_H);
        KSPSetType(B_ksp, KSPPREONLY);
        KSPGetPC(B_ksp, &B_factor);
        PCSetType(B_factor, PCLU);
        PCFactorSetMatSolverType(B_factor, MATSOLVERSUPERLU_DIST);
        // PCFactorSetMatSolverType(B_factor, MATSOLVERMUMPS);
        PCFactorSetUpMatSolverType(B_factor);
        KSPSetFromOptions(B_ksp);
        KSPSetUp(B_ksp);
        aux_B->factorization = (void *)B_ksp;
    }
}

int PASE_Mg_pase_aux_matrix_create(PASE_MG_SOLVER solver)
{
    int aux_level = solver->aux_coarse_level;
    int batch_size = solver->batch_size;

    solver->aux_A = (PASE_Matrix)malloc(sizeof(pase_Matrix));
    PASE_Matrix aux_A = solver->aux_A;
    MPI_Comm_dup(solver->group_comm, &(aux_A->comm));
    aux_A->num_aux_vec = batch_size;
    aux_A->A_H = solver->multigrid->A_array[aux_level];
    aux_A->aux_Hh = solver->cg_rhs[aux_level];
    aux_A->aux_hh = (double *)malloc(batch_size * batch_size * sizeof(double));
    if (solver->pc_type == PRECOND_NONE)
    {
        aux_A->factorization = NULL;
        aux_A->AH_inv_aux_Hh = NULL;
        aux_A->aux_hh_inv = NULL;
    }
    else if (solver->pc_type == PRECOND_A)
    {
        aux_A->factorization = NULL;
        aux_A->AH_inv_aux_Hh = solver->cg_res[aux_level];
        aux_A->aux_hh_inv = (double *)malloc(batch_size * batch_size * sizeof(double));
    }
    else if (solver->pc_type == PRECOND_B || solver->pc_type == PRECOND_B_A)
    {
        aux_A->factorization = NULL;
        aux_A->AH_inv_aux_Hh = solver->cg_res[aux_level];
        aux_A->aux_hh_inv = (double *)malloc(batch_size * batch_size * sizeof(double));
    }
    aux_A->if_sym = 1;
    aux_A->is_diag = 0;
    aux_A->is_dense_diag = 0;

    solver->aux_B = (PASE_Matrix)malloc(sizeof(pase_Matrix));
    PASE_Matrix aux_B = solver->aux_B;
    MPI_Comm_dup(solver->group_comm, &(aux_B->comm));
    aux_B->num_aux_vec = batch_size;
    aux_B->A_H = solver->multigrid->B_array[aux_level];
    aux_B->aux_Hh = solver->cg_p[aux_level];
    aux_B->aux_hh = (double *)malloc(batch_size * batch_size * sizeof(double));
    solver->pre_bh = solver->cg_p[solver->num_levels];

    if (solver->pc_type == PRECOND_NONE)
    {
        aux_B->factorization = NULL;
        aux_B->AH_inv_aux_Hh = NULL;
        aux_B->aux_hh_inv = NULL;
    }
    else if (solver->pc_type == PRECOND_A)
    {
        aux_B->factorization = NULL;
        aux_B->AH_inv_aux_Hh = solver->cg_w[aux_level];
        aux_B->aux_hh_inv = (double *)malloc(batch_size * batch_size * sizeof(double));
    }
    else if (solver->pc_type == PRECOND_B || solver->pc_type == PRECOND_B_A)
    {
        aux_B->factorization = NULL;
        aux_B->AH_inv_aux_Hh = solver->cg_w[aux_level];
        aux_B->aux_hh_inv = (double *)malloc(batch_size * batch_size * sizeof(double));
    }
    aux_B->if_sym = 1;
    aux_B->is_diag = 0;
    aux_B->is_dense_diag = 0;

    // 跟师妹商量下要不要做
    //  solver->org_aux_A = (PASE_Matrix)malloc(sizeof(pase_Matrix));
    //  PASE_Matrix org_aux_A = solver->org_aux_A;
}

int PASE_Mg_pase_aux_vector_create(PASE_MG_SOLVER solver)
{
    solver->aux_sol = (PASE_MultiVector)malloc(sizeof(pase_MultiVector));
    PASE_MultiVector aux_sol = solver->aux_sol;
    MPI_Comm_dup(solver->group_comm, &(aux_sol->comm));
    aux_sol->num_aux_vec = solver->batch_size;
    aux_sol->num_vec = solver->gcge_nev;
    aux_sol->b_H = solver->solution[solver->aux_coarse_level];
    aux_sol->aux_h = (double *)malloc(solver->batch_size * solver->gcge_nev * sizeof(double));
    aux_sol->aux_h_tmp = (double *)malloc(solver->batch_size * solver->gcge_nev * sizeof(double));
}

int PASE_Mg_prolong_from_Solution(PASE_MG_SOLVER solver, int object_level)
{
    int current_level = solver->initial_level;
    if (current_level == object_level)
        return 0;

    int batch_size = solver->real_batch_size, batch_num = solver->batch_num;
    // 因为插值空间的限制, 每次最多只能插值 batch_size 个向量
    int mv_s[2] = {0, 0}, mv_e[2] = {batch_size, batch_size};
    while (mv_e[0] < solver->real_nev)
    {
        PASE_MULTIGRID_fromItoJ(solver->multigrid, current_level, object_level, mv_s, mv_e, solver->solution[current_level], solver->solution[object_level]);
        mv_s[0] += batch_size;
        mv_s[1] += batch_size;
        mv_e[0] += batch_size;
        mv_e[1] += batch_size;
    }
    mv_e[0] = solver->real_nev;
    mv_e[1] = solver->real_nev;
    PASE_MULTIGRID_fromItoJ(solver->multigrid, current_level, object_level, mv_s, mv_e, solver->solution[current_level], solver->solution[object_level]);
}

PASE_MG_SOLVER PASE_Mg_solver_create(PASE_PARAMETER param, PASE_OPS *pase_ops)
{
    PASE_MG_SOLVER solver = (PASE_MG_SOLVER)malloc(sizeof(PASE_MG_SOLVER_PRIVATE));
    //======================= multigrid =====================
    solver->multigrid_type = param->multigrid_type;
    solver->multigrid = NULL;
    solver->num_levels = param->num_levels;
    solver->boundary_type = param->boundary_type;
    solver->boundary_delete = param->boundary_delete;
    solver->boundary_num = param->boundary_num;
    solver->boundary_index = param->boundary_index;
    //======================= problem setting =====================
    solver->more_batch_size = 0; // 每个 batch 里需要多算多少特征对
    solver->user_nev = param->nev;
    solver->more_nev = param->nev + solver->more_batch_size;
    solver->num_given_eigs = param->num_given_eigs;
    solver->gcge_nev = 2 * solver->more_nev;
    //======================= PASE =====================
    solver->solver_type = param->solver_type;
    solver->pc_type = param->pc_type;
    solver->pc_eigendcp = param->pc_eigendcp;
    solver->aux_coarse_level = param->aux_coarse_level;
    solver->aux_fine_level = param->aux_fine_level;
    solver->max_cycle_count = param->max_cycle_count;
    solver->group_num = param->group_num;
    solver->group_id = param->group_id;
    if (solver->group_num == 1)
    {
        solver->group_id = 0;
        solver->group = MPI_GROUP_EMPTY;
        solver->group_comm = MPI_COMM_WORLD;
    }
    else
    {
        if (solver->group_id == -1)
            PASE_Error("PASE_Mg_solver_create", "group id needed.");
        solver->group = param->group;
        solver->group_comm = param->group_comm;
        if (solver->group == MPI_GROUP_NULL || solver->group_comm == MPI_COMM_NULL)
            PASE_Error("PASE_Mg_solver_create", "group and group_comm needed.");
    }
    if (solver->user_nev % (solver->group_num))
        PASE_Error("PASE_Mg_solver_create", "user_nev needs to be an integer multiple of the number of groups.");
    // -----------------------------------------
    // 这部分参数都会等到初值求解的时候再进一步确认
    solver->real_nev = solver->user_nev / solver->group_num;
    solver->nev_start = solver->real_nev * solver->group_id;
    solver->nev_end = solver->nev_start + solver->real_nev;
    if (param->batch_size > 0)
    {
        solver->batch_size = param->batch_size;
        solver->batch_num = solver->real_nev / (solver->group_num * solver->batch_size) + 2;
    }
    else
    {
        solver->batch_size = solver->real_nev;
        solver->batch_num = 1;
    }
    // -----------------------------------------
    solver->rtol = param->rtol;
    solver->check_converge = false;
    solver->current_batch = 0;
    solver->current_cycle = 0;
    solver->current_nev_start = 0;
    solver->current_nev_end = 0;
    solver->conv_nev = 0;
    solver->nlock_auxmat_A = 0;
    solver->nlock_auxmat_B = 0;
    //======================= smoothing =====================
    solver->smoothing_type = param->smoothing_type;
    solver->max_pre_count = param->max_pre_count;
    solver->max_post_count = param->max_post_count;
    solver->nlock_smooth = 0;
    //======================= initial GCGE =====================
    solver->initial_level = param->initial_level;
    solver->max_initial_direct_count = param->max_initial_direct_count;
    solver->initial_atol = param->initial_atol > 0.0 ? param->initial_atol : sqrt(param->initial_rtol);
    solver->initial_rtol = param->initial_rtol;
    //======================= aux GCGE =====================
    solver->more_aux_nev = 100;
    if (solver->group_num == 1 && solver->batch_num == 1)
        solver->more_aux_nev = 0;
    solver->shift_factorization = NULL;
    solver->max_direct_count = param->max_direct_count;
    if (solver->solver_type == PASE_DEFAULT)
        solver->aux_more_nev = 0;
    else
        solver->aux_more_nev = 20;
    solver->current_shift = 0.0;
    solver->aux_atol = param->aux_atol > 0.0 ? param->aux_atol : sqrt(param->aux_rtol);
    solver->aux_rtol = param->aux_rtol;
    solver->max_aux_rtol = param->max_aux_rtol;
    solver->aux_rtol_coef = param->aux_rtol_coef;
    //======================= eigen pairs =======================
    solver->eigenvalues = NULL;
    solver->solution = NULL;
    //======================= tmp space =====================
    solver->cg_rhs = NULL;
    solver->cg_res = NULL;
    solver->cg_p = NULL;
    solver->cg_w = NULL;
    solver->double_tmp = NULL;
    solver->int_tmp = NULL;
    solver->double_tmp_hrr = NULL;
    solver->aux_gcg_mv[0] = NULL;
    solver->aux_gcg_mv[1] = NULL;
    solver->aux_gcg_mv[2] = NULL;
    solver->aux_gcg_mv[3] = NULL;
    solver->pre_double = NULL;
    //======================= ops ===========================
    solver->gcge_ops = pase_ops->gcge_ops;
    solver->pase_ops = pase_ops;
    solver->pase_ops_to_gcge = NULL;
    //======================= aux problem =====================
    solver->aux_sol = NULL;
    solver->aux_A = NULL;
    solver->aux_B = NULL;
    //======================= timer =====================
    solver->solver_setup_time = 0.0;
    solver->get_initvec_time = 0.0;
    solver->smooth_time = 0.0;
    solver->build_aux_time = 0.0;
    solver->prolong_time = 0.0;
    solver->error_estimate_time = 0.0;
    solver->aux_direct_solve_time = 0.0;
    solver->precondition_setup_time = 0.0;
    solver->precondition_solve_time = 0.0;
    solver->total_solve_time = 0.0;
    solver->total_time = 0.0;
    //======================================================
    solver->print_level = param->print_level;
#if PRINT_INFO
    if (solver->print_level)
    {
        if (solver->multigrid_type == PASE_AMG)
            pase_ops->gcge_ops->Printf("\n[CREATING] Multigrid Type is == AMG ==\n");
        else if (solver->multigrid_type == PASE_GMG)
            pase_ops->gcge_ops->Printf("\n[CREATING] Multigrid Type is == GMG ==\n");

        if (solver->pc_type == PRECOND_NONE)
            pase_ops->gcge_ops->Printf("[CREATING] PC: PRECOND_NONE\n");
        else if (solver->pc_type == PRECOND_A)
            pase_ops->gcge_ops->Printf("[CREATING] PC: PRECOND_A\n");
        else if (solver->pc_type == PRECOND_B)
            pase_ops->gcge_ops->Printf("[CREATING] PC: PRECOND_B\n");
        else if (solver->pc_type == PRECOND_B_A)
            pase_ops->gcge_ops->Printf("[CREATING] PC: PRECOND_B_A\n");

        pase_ops->gcge_ops->Printf("[CREATING] user_nev = %d | gcge_nev = %d | real_nev = %d\n",
                                   solver->user_nev, solver->gcge_nev, solver->real_nev);
        pase_ops->gcge_ops->Printf("[CREATING] init given eigs num: %d\n", solver->num_given_eigs);
    }
#endif
    return solver;
}

int PASE_Mg_solver_destroy(PASE_MG_SOLVER solver)
{
    if (solver->boundary_index != NULL)
    {
        free(solver->boundary_index);
        solver->boundary_index = NULL;
    }
    if (solver->eigenvalues != NULL)
    {
        free(solver->eigenvalues);
        solver->eigenvalues = NULL;
    }
    if (solver->aux_eigenvalues != NULL)
    {
        free(solver->aux_eigenvalues);
        solver->aux_eigenvalues = NULL;
    }
    if (solver->multigrid != NULL)
    {
        PASE_MULTIGRID_Destroy(&(solver->multigrid));
    }

    if (solver->pre_double != NULL)
    {
        free(solver->pre_double);
        solver->pre_double = NULL;
    }

    PASE_Matrix_destroy_sub(&(solver->aux_A));
    PASE_Matrix_destroy_sub(&(solver->aux_B));
    PASE_MultiVector_destroy_sub(&(solver->aux_sol));

    // solver->gcge_ops->MultiVecDestroy(&(solver->aux_gcg_mv[0]->b_H),
    //                                   solver->aux_nev + solver->batch_size, solver->gcge_ops);
    // PASE_MultiVector_destroy_sub(&(solver->aux_gcg_mv[0]));
    // solver->gcge_ops->MultiVecDestroy(&(solver->aux_gcg_mv[1]->b_H),
    //                                   solver->batch_size / 2, solver->gcge_ops);
    // PASE_MultiVector_destroy_sub(&(solver->aux_gcg_mv[1]));
    // solver->gcge_ops->MultiVecDestroy(&(solver->aux_gcg_mv[2]->b_H),
    //                                   solver->batch_size / 2, solver->gcge_ops);
    // PASE_MultiVector_destroy_sub(&(solver->aux_gcg_mv[2]));
    // solver->gcge_ops->MultiVecDestroy(&(solver->aux_gcg_mv[3]->b_H),
    //                                   solver->batch_size / 2, solver->gcge_ops);
    // PASE_MultiVector_destroy_sub(&(solver->aux_gcg_mv[3]));

    if (solver->pase_ops != NULL)
    {
        PASE_OPS_Destroy(&(solver->pase_ops));
        solver->pase_ops = NULL;
    }
    if (solver->gcge_ops != NULL)
    {
        OPS_Destroy(&(solver->gcge_ops));
        solver->gcge_ops = NULL;
    }
    if (solver->pase_ops_to_gcge != NULL)
    {
        OPS_Destroy(&(solver->pase_ops_to_gcge));
        solver->pase_ops_to_gcge = NULL;
    }

    free(solver);
    solver = NULL;
    return 0;
}

int PASE_Matrix_destroy_sub(PASE_Matrix *aux_A)
{
    free((*aux_A)->aux_hh);
    (*aux_A)->aux_hh = NULL;
    if ((*aux_A)->aux_hh_inv != NULL)
    {
        free((*aux_A)->aux_hh_inv);
        (*aux_A)->aux_hh_inv = NULL;
    }
    if ((*aux_A)->factorization != NULL)
    {
        KSPDestroy((KSP *)(&((*aux_A)->factorization)));
        (*aux_A)->factorization = NULL;
    }
    free(*aux_A);
    *aux_A = NULL;
    return 0;
}

int PASE_MultiVector_destroy_sub(PASE_MultiVector *aux_sol)
{
    free((*aux_sol)->aux_h);
    (*aux_sol)->aux_h = NULL;
    free((*aux_sol)->aux_h_tmp);
    (*aux_sol)->aux_h_tmp = NULL;
    free(*aux_sol);
    *aux_sol = NULL;
    return 0;
}

int PASE_Mg_print_timer(PASE_MG_SOLVER solver)
{
    OPS *gcge_ops = solver->gcge_ops;
    if (solver->print_level > 0)
    {
        gcge_ops->Printf("=============================================================\n");
        gcge_ops->Printf("· solver setup time         = %f seconds\n", solver->solver_setup_time);
        gcge_ops->Printf("· get initvec time          = %f seconds\n", solver->get_initvec_time);
        gcge_ops->Printf("· smooth time               = %f seconds\n", solver->smooth_time);
        gcge_ops->Printf("· build aux time            = %f seconds\n", solver->build_aux_time);
        gcge_ops->Printf("· prolong time              = %f seconds\n", solver->prolong_time);
        gcge_ops->Printf("· error estimate time       = %f seconds\n", solver->error_estimate_time);
        gcge_ops->Printf("· aux direct solve time     = %f seconds\n", solver->aux_direct_solve_time);
        gcge_ops->Printf("· precondition setup time   = %f seconds\n", solver->precondition_setup_time);
        gcge_ops->Printf("· precondition solve time   = %f seconds\n", solver->precondition_solve_time);
        gcge_ops->Printf("· total solve time          = %f seconds\n", solver->total_solve_time);
        gcge_ops->Printf("· total time                = %f seconds\n", solver->total_time);
        gcge_ops->Printf("=============================================================\n");
    }
}

int PASE_Mg_print_param(PASE_MG_SOLVER solver)
{
    int i = 0;
    int num_levels = solver->num_levels;
    PetscPrintf(PETSC_COMM_WORLD, "\n");
    PetscPrintf(PETSC_COMM_WORLD, "|=================== PARAMETERS ====================|\n");
    PetscPrintf(PETSC_COMM_WORLD, "|------------------ [eigenvalue] -------------------|\n");
    PetscPrintf(PETSC_COMM_WORLD, "| user_nev                 = %12d           |\n", solver->user_nev);
    PetscPrintf(PETSC_COMM_WORLD, "| num_given_eigs           = %12d           |\n", solver->num_given_eigs);
    PetscPrintf(PETSC_COMM_WORLD, "| group_num                = %12d           |\n", solver->group_num);
    PetscPrintf(PETSC_COMM_WORLD, "| batch_size               = %12d           |\n", solver->batch_size);
    PetscPrintf(PETSC_COMM_WORLD, "|------------------ [converging] -------------------|\n");
    PetscPrintf(PETSC_COMM_WORLD, "| rtol                     = %12e           |\n", solver->rtol);
    PetscPrintf(PETSC_COMM_WORLD, "| aux_rtol                 = %12e           |\n", solver->aux_rtol);
    PetscPrintf(PETSC_COMM_WORLD, "| initial_rtol             = %12e           |\n", solver->initial_rtol);
    PetscPrintf(PETSC_COMM_WORLD, "| max_aux_rtol             = %12e           |\n", solver->max_aux_rtol);
    PetscPrintf(PETSC_COMM_WORLD, "| aux_rtol_coef            = %12e           |\n", solver->aux_rtol_coef);
    PetscPrintf(PETSC_COMM_WORLD, "|------------------ [multigrid] --------------------|\n");
    PetscPrintf(PETSC_COMM_WORLD, "| num_levels               = %12d           |\n", solver->num_levels);
    PetscPrintf(PETSC_COMM_WORLD, "| initial_level            = %12d           |\n", solver->initial_level);
    PetscPrintf(PETSC_COMM_WORLD, "| aux_coarse_level         = %12d           |\n", solver->aux_coarse_level);
    PetscPrintf(PETSC_COMM_WORLD, "| aux_fine_level           = %12d           |\n", solver->aux_fine_level);
    PetscPrintf(PETSC_COMM_WORLD, "|------------------ [iterations] -------------------|\n");
    PetscPrintf(PETSC_COMM_WORLD, "| max_cycle_count          = %12d           |\n", solver->max_cycle_count);
    PetscPrintf(PETSC_COMM_WORLD, "| max_pre_count            = %12d           |\n", solver->max_pre_count);
    PetscPrintf(PETSC_COMM_WORLD, "| max_post_count           = %12d           |\n", solver->max_post_count);
    PetscPrintf(PETSC_COMM_WORLD, "| max_direct_count         = %12d           |\n", solver->max_direct_count);
    PetscPrintf(PETSC_COMM_WORLD, "| max_initial_direct_count = %12d           |\n", solver->max_initial_direct_count);
    PetscPrintf(PETSC_COMM_WORLD, "|===================================================|\n\n");
    return 0;
}

/**
 * @brief 直接调用GCGE求解特征值问题，GCG的相关参数均在此函数中进行设置
 *
 * @param A    刚度矩阵
 * @param B    质量矩阵
 * @param flag 0:用BCG作为线性解法器，1:用其他的线性解法器
 * @param nev  要计算的特征对个数
 * @param atol 特征值问题绝对误差
 * @param rtol 特征值问题相对
 * @param argc 命令行读入的参数个数
 * @param argv 命令行读入的参数
 */
int PASE_DIRECT_GCGE(void *A, void *B, int flag, int nev, double atol, double rtol, int argc, char *argv[])
{
    Mat slepc_matA, slepc_matB;
    slepc_matA = (Mat)A;
    slepc_matB = (Mat)B;
    double shift = 0;
    MatAXPY(slepc_matA, -shift, slepc_matB, DIFFERENT_NONZERO_PATTERN);
    MatAssemblyBegin(slepc_matA, MAT_FINAL_ASSEMBLY);
    MatAssemblyEnd(slepc_matA, MAT_FINAL_ASSEMBLY);
    OPS *ops;
    OPS_Create(&ops);
    OPS_SLEPC_Set(ops);
    OPS_Setup(ops);

    int nevConv = nev, multiMax = 1;
    double gapMin = 1e-5;
    int nevGiven = 0, block_size = nevConv / 2, nevMax = 2 * nevConv;
    int nevInit = 2 * nevConv;

    if (nevConv > 300)
    {
        block_size = nevConv / 10;
        nevInit = 3 * block_size;
        nevMax = nevInit + nevConv;
    }

    ops->GetOptionFromCommandLine("-nevConv", 'i', &nevConv, argc, argv, ops);
    ops->GetOptionFromCommandLine("-nevMax", 'i', &nevMax, argc, argv, ops);
    ops->GetOptionFromCommandLine("-blockSize", 'i', &block_size, argc, argv, ops);
    ops->GetOptionFromCommandLine("-nevInit", 'i', &nevInit, argc, argv, ops);

    nevInit = nevInit < nevMax ? nevInit : nevMax;
    int max_iter_gcg = 500;
    double tol_gcg[2] = {1e-5, 1e-10};
    tol_gcg[0] = atol;
    tol_gcg[1] = rtol;

    double *eval;
    void **evec;
    eval = malloc(nevMax * sizeof(double));
    memset(eval, 0, nevMax * sizeof(double));
    ops->MultiVecCreateByMat(&evec, nevMax, A, ops);
    ops->MultiVecSetRandomValue(evec, 0, nevMax, ops);
    void **gcg_mv_ws[4];
    double *dbl_ws;
    int *int_ws;
    ops->MultiVecCreateByMat(&gcg_mv_ws[0], nevMax + 2 * block_size, A, ops);
    ops->MultiVecSetRandomValue(gcg_mv_ws[0], 0, nevMax + 2 * block_size, ops);
    ops->MultiVecCreateByMat(&gcg_mv_ws[1], nevMax + 2 * block_size, A, ops);
    ops->MultiVecSetRandomValue(gcg_mv_ws[1], 0, nevMax + 2 * block_size, ops);
    ops->MultiVecCreateByMat(&gcg_mv_ws[2], nevMax + 2 * block_size, A, ops);
    ops->MultiVecSetRandomValue(gcg_mv_ws[2], 0, nevMax + 2 * block_size, ops);
    ops->MultiVecCreateByMat(&gcg_mv_ws[3], block_size, A, ops);
    ops->MultiVecSetRandomValue(gcg_mv_ws[3], 0, block_size, ops);
    int sizeV = nevInit + 2 * block_size;
    int length_dbl_ws = 2 * sizeV * sizeV + 10 * sizeV + (nevMax + 2 * block_size) + (nevMax)*block_size;
    // ops->Printf ( "length_dbl_ws = %d\n", length_dbl_ws );
    int length_int_ws = 6 * sizeV + 2 * (block_size + 3);
    // ops->Printf ( "length_int_ws = %d\n", length_int_ws );
    dbl_ws = malloc(length_dbl_ws * sizeof(double));
    memset(dbl_ws, 0, length_dbl_ws * sizeof(double));
    int_ws = malloc(length_int_ws * sizeof(int));
    memset(int_ws, 0, length_int_ws * sizeof(int));

    double *dbl_ws_hrr;
    int len_dbl_ws_hrr = sizeV * sizeV + /*ss_matB*/
                         2 * sizeV;      /*ALPHAR BETA*/
    dbl_ws_hrr = malloc(len_dbl_ws_hrr * sizeof(double));
    memset(dbl_ws_hrr, 0, len_dbl_ws_hrr * sizeof(double));

    srand(0);
    double time_start, time_interval;
    time_start = ops->GetWtime();

    ops->Printf("===============================================\n");
    ops->Printf("GCG Eigen Solver\n");
    EigenSolverSetup_GCG(multiMax, gapMin, nevInit, nevMax, block_size,
                         tol_gcg, max_iter_gcg, flag, 0.0, gcg_mv_ws, dbl_ws, int_ws, ops);
    // EigenSolverSetup_GCG_h(multiMax, gapMin, nevInit, nevMax, block_size,
    //                      tol_gcg, max_iter_gcg, flag, 0.0, gcg_mv_ws, dbl_ws, int_ws, dbl_ws_hrr, ops);

    int check_conv_max_num = 50;

    char initX_orth_method[8] = "mgs";
    int initX_orth_block_size = -1;
    int initX_orth_max_reorth = 2;
    double initX_orth_zero_tol = 2 * DBL_EPSILON; // 1e-12

    char compP_orth_method[8] = "mgs";
    int compP_orth_block_size = -1;
    int compP_orth_max_reorth = 2;
    double compP_orth_zero_tol = 2 * DBL_EPSILON; // 1e-12

    char compW_orth_method[8] = "mgs";
    int compW_orth_block_size = -1;
    int compW_orth_max_reorth = 2;
    double compW_orth_zero_tol = 2 * DBL_EPSILON; // 1e-12
    int compW_bpcg_max_iter = 30;
    double compW_bpcg_rate = 1e-2;
    double compW_bpcg_tol = 1e-14;
    char compW_bpcg_tol_type[8] = "abs";

    int compRR_min_num = -1;
    double compRR_min_gap = gapMin;
    double compRR_tol = 2 * DBL_EPSILON;

    EigenSolverSetParameters_GCG(
        check_conv_max_num,
        initX_orth_method, initX_orth_block_size,
        initX_orth_max_reorth, initX_orth_zero_tol,
        compP_orth_method, compP_orth_block_size,
        compP_orth_max_reorth, compP_orth_zero_tol,
        compW_orth_method, compW_orth_block_size,
        compW_orth_max_reorth, compW_orth_zero_tol,
        compW_bpcg_max_iter, compW_bpcg_rate,
        compW_bpcg_tol, compW_bpcg_tol_type, 1, // without shift
        compRR_min_num, compRR_min_gap,
        compRR_tol,
        ops);

    ops->Printf("nevGiven = %d, nevConv = %d, nevMax = %d, block_size = %d, nevInit = %d\n",
                nevGiven, nevConv, nevMax, block_size, nevInit);
    ops->EigenSolver((void *)slepc_matA, B, NULL, eval, evec, nevGiven, &nevConv, shift, ops);
    ops->Printf("numIter = %d, nevConv = %d\n",
                ((GCGSolver *)ops->eigen_solver_workspace)->numIter, nevConv);
    ops->Printf("++++++++++++++++++++++++++++++++++++++++++++++\n");

    time_interval = ops->GetWtime() - time_start;
    ops->Printf("Time is %.3f\n", time_interval);

    ops->MultiVecDestroy(&gcg_mv_ws[0], nevMax + 2 * block_size, ops);
    ops->MultiVecDestroy(&gcg_mv_ws[1], block_size, ops);
    ops->MultiVecDestroy(&gcg_mv_ws[2], block_size, ops);
    ops->MultiVecDestroy(&gcg_mv_ws[3], block_size, ops);
    free(dbl_ws);
    free(int_ws);
    free(dbl_ws_hrr);
#if 1
    ops->Printf("eigenvalues\n");
    int idx;
    for (idx = 0; idx < nevConv; ++idx)
    {
        ops->Printf("%d: %6.14e\n", idx + 1, eval[idx]);
    }
    // ops->Printf("eigenvectors\n");
    // ops->MultiVecView(evec,0,nevConv,ops);
#endif
    ops->MultiVecDestroy(&(evec), nevMax, ops);
    OPS_Destroy(&ops);

    free(eval);
    return 0;
}

int PASE_DIRECT_EPS(void *A, void *B, int flag, int nev, double tol, int argc, char *argv[])
{
    OPS *ops;
    OPS_Create(&ops);
    OPS_SLEPC_Set(ops);
    OPS_Setup(ops);
    EPS eps;
    EPSType type;
    PetscInt ncv, mpd, max_it, nconv, its, blocksize;
    PetscReal restart;
    if (flag == 2) // ks
    {
        ncv = 2 * nev;
        mpd = ncv;
    }
    else if (flag == 6) // lobpcg
    {
        ncv = 2 * nev;
        blocksize = nev / 5;
        restart = 0.1;
        mpd = 3 * blocksize;
    }
    max_it = 2000;
    EPSCreate(PETSC_COMM_WORLD, &eps);
    EPSSetOperators(eps, (Mat)A, (Mat)B);
    if (B == NULL)
        EPSSetProblemType(eps, EPS_HEP);
    else
        EPSSetProblemType(eps, EPS_GHEP);
    switch (flag)
    {
    case 1:
        EPSSetType(eps, EPSLANCZOS);
        break;
    case 2:
        EPSSetType(eps, EPSKRYLOVSCHUR);
        break;
    case 3:
        EPSSetType(eps, EPSGD);
        break;
    case 4:
        EPSSetType(eps, EPSJD);
        break;
    case 5:
        EPSSetType(eps, EPSRQCG);
        break;
    case 6:
        EPSSetType(eps, EPSLOBPCG);
        break;
    default:
        EPSSetType(eps, EPSKRYLOVSCHUR);
        // EPSSetType(eps,EPSLOBPCG);
        break;
    }
    EPSSetDimensions(eps, nev, ncv, mpd);
    EPSSetWhichEigenpairs(eps, EPS_SMALLEST_REAL);
    EPSSetTolerances(eps, tol, max_it);
    EPSSetConvergenceTest(eps, EPS_CONV_REL);
    // EPSSetConvergenceTest(eps,EPS_CONV_ABS);
    if (flag == 6)
    {
        EPSLOBPCGSetBlockSize(eps, blocksize);
        EPSLOBPCGSetRestart(eps, restart);
    }

    EPSSetFromOptions(eps);
    EPSSetUp(eps);
    EPSView(eps, PETSC_VIEWER_STDOUT_WORLD);
    ST st;
    KSP ksp;
    EPSGetST(eps, &st);
    STGetKSP(st, &ksp);
    KSPView(ksp, PETSC_VIEWER_STDOUT_WORLD);

    double time_start, time_interval;
    time_start = ops->GetWtime();

    EPSSolve(eps);

    time_interval = ops->GetWtime() - time_start;
    ops->Printf("Time is %.3f\n", time_interval);

    EPSGetType(eps, &type);
    EPSGetConverged(eps, &nconv);
    EPSGetIterationNumber(eps, &its);
    PetscPrintf(PETSC_COMM_WORLD, " Solution method: %s\n\n", type);
    PetscPrintf(PETSC_COMM_WORLD, " Number of requested eigenvalues: %D\n", nev);
    PetscPrintf(PETSC_COMM_WORLD, " Number of converged eigenpairs: %D\n\n", nconv);
    PetscPrintf(PETSC_COMM_WORLD, " Number of iterations of the method: %D\n", its);
#if 0
    int i; PetscScalar eigr;
    for (i = 0; i < nconv; ++i) {
    EPSGetEigenvalue(eps,i,&eigr,NULL);
    PetscPrintf(PETSC_COMM_WORLD,"%d: %6.14e\n",1+i,eigr);
    }
#else
    PetscViewerPushFormat(PETSC_VIEWER_STDOUT_WORLD, PETSC_VIEWER_ASCII_INFO_DETAIL);
    // EPSConvergedReasonView(eps,PETSC_VIEWER_STDOUT_WORLD);
    EPSErrorView(eps, EPS_ERROR_ABSOLUTE, PETSC_VIEWER_STDOUT_WORLD);
    EPSErrorView(eps, EPS_ERROR_RELATIVE, PETSC_VIEWER_STDOUT_WORLD);
    PetscViewerPopFormat(PETSC_VIEWER_STDOUT_WORLD);
#endif
    EPSDestroy(&eps);
    OPS_Destroy(&ops);

    return 0;
}
