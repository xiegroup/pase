#ifndef _pase_mg_h_
#define _pase_mg_h_

#include "pase_ops.h"
#include "pase_param.h"

typedef struct pase_MultiGrid_struct
{
    /* level */
    int num_levels;
    int pase_coarse_level;
    int pase_fine_level;
    /* matrix */
    void **A_array;
    void **B_array;
    void **P_array;
    int *local_size;
    /* vectors */
    void ***solution;
    void ***cg_rhs;
    void ***cg_res;
    void ***cg_p;
    void ***cg_w;
    /* tmp */
    double *double_tmp;
    int *int_tmp;
    /* ops */
    OPS *gcge_ops;
    /* gmg */
    int boundary_num;
    int *boundary_index;
} pase_MultiGrid;
typedef struct pase_MultiGrid_struct *PASE_MULTIGRID;

int PASE_MULTIGRID_fromItoJ(PASE_MULTIGRID multi_grid, int level_i, int level_j,
                            int *mv_s, int *mv_e, void **pvx_i, void **pvx_j);

int PASE_MULTIGRID_Create(PASE_MULTIGRID *multi_grid, PASE_PARAMETER param, OPS *gcge_ops);
int PASE_MULTIGRID_Destroy(PASE_MULTIGRID *multi_grid);

#endif