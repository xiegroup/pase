#include "pase_mg.h"
#include "pase_param.h"

#define PRINT_INFO 1
#define PRINT_TIME_INFO 1
#define PETSC_GAMG_USE 1

static int Prolong_from_I_to_J(PASE_MULTIGRID multigrid, int level_i, int level_j,
                               int *mv_s, int *mv_e, void **pvx_i, void **pvx_j)
{
    int idx_level;
    OPS *gcge_ops = multigrid->gcge_ops;
    void **from_mv, **to_mv;
    int start[2], end[2];
    void ***tmp = multigrid->cg_res;
    void **P_array = multigrid->P_array;
    for (idx_level = level_i; idx_level > level_j; idx_level--)
    {
        if (idx_level == level_i)
        {
            from_mv = pvx_i;
            start[0] = mv_s[0];
            end[0] = mv_e[0];
        }
        else
        {
            from_mv = tmp[idx_level];
            start[0] = 0;
            end[0] = mv_e[0] - mv_s[0];
        }
        if (idx_level == level_j + 1)
        {
            to_mv = pvx_j;
            start[1] = mv_s[1];
            end[1] = mv_e[1];
        }
        else
        {
            to_mv = tmp[idx_level - 1];
            start[1] = 0;
            end[1] = mv_e[0] - mv_s[0];
        }
        gcge_ops->MatDotMultiVec(P_array[idx_level - 1], from_mv, to_mv, start, end, gcge_ops);
    }
    return 0;
}

static int Restrict_from_I_to_J(PASE_MULTIGRID multigrid, int level_i, int level_j,
                                int *mv_s, int *mv_e, void **pvx_i, void **pvx_j)
{
    int idx_level;
    OPS *gcge_ops = multigrid->gcge_ops;
    void **from_mv, **to_mv;
    int start[2], end[2];
    void ***tmp = multigrid->cg_res;
    void **P_array = multigrid->P_array;
    for (idx_level = level_i; idx_level < level_j; idx_level++)
    {
        if (idx_level == level_i)
        {
            from_mv = pvx_i;
            start[0] = mv_s[0];
            end[0] = mv_e[0];
        }
        else
        {
            from_mv = tmp[idx_level];
            start[0] = 0;
            end[0] = mv_e[0] - mv_s[0];
        }

        if (idx_level == level_j - 1)
        {
            to_mv = pvx_j;
            start[1] = mv_s[1];
            end[1] = mv_e[1];
        }
        else
        {
            to_mv = tmp[idx_level + 1];
            start[1] = 0;
            end[1] = mv_e[0] - mv_s[0];
        }
        gcge_ops->MatTransDotMultiVec(P_array[idx_level], from_mv, to_mv, start, end, gcge_ops);
    }
    return 0;
}

int PASE_MULTIGRID_fromItoJ(PASE_MULTIGRID multi_grid, int level_i, int level_j,
                            int *mv_s, int *mv_e, void **pvx_i, void **pvx_j)
{
    if (level_i < level_j)
    {
        Restrict_from_I_to_J(multi_grid, level_i, level_j, mv_s, mv_e, pvx_i, pvx_j);
    }
    else if (level_i > level_j)
    {
        Prolong_from_I_to_J(multi_grid, level_i, level_j, mv_s, mv_e, pvx_i, pvx_j);
    }
    else if (level_i == level_j)
    {
        multi_grid->gcge_ops->MultiVecAxpby(1.0, pvx_i, 0.0, pvx_j, mv_s, mv_e, multi_grid->gcge_ops);
    }
    return 0;
}

static void get_array_by_petsc_gamg(PASE_MULTIGRID multi_grid, PASE_PARAMETER param, OPS *gcge_ops)
{
#if PRINT_INFO
    gcge_ops->Printf("\n[petsc_gamg] multigrid 初始层数: %d levels\n", multi_grid->num_levels);
#endif
#if PRINT_TIME_INFO
    double get_amg_array_time = 0.0;
    double get_A_P_array_time = 0.0;
    double get_B_array_time = 0.0;
    get_amg_array_time -= gcge_ops->GetWtime();
    get_A_P_array_time -= gcge_ops->GetWtime();
#endif

    PetscErrorCode ierr;
    Mat petsc_A = (Mat)(param->A), petsc_B = (Mat)(param->B);
    Mat *A_array = NULL, *B_array = NULL, *P_array = NULL;
    Mat *Aarr = NULL, *Parr = NULL;

    PC pc;
    PCCreate(PETSC_COMM_WORLD, &pc);
    PCSetOperators(pc, petsc_A, petsc_A);
    PCSetType(pc, PCGAMG);
    PCGAMGSetNlevels(pc, multi_grid->num_levels);
    PCGAMGSetType(pc, PCGAMGAGG);
    PCGAMGSetRepartition(pc, PETSC_TRUE);
    PCSetUp(pc);
    PCGetCoarseOperators(pc, &(multi_grid->num_levels), &Aarr);
    PCGetInterpolations(pc, &(multi_grid->num_levels), &Parr);
#if PRINT_INFO
    gcge_ops->Printf("[petsc_gamg] multigrid 层数更新: %d levels\n", multi_grid->num_levels);
#endif
    A_array = (Mat *)malloc((multi_grid->num_levels) * sizeof(Mat));
    P_array = (Mat *)malloc((multi_grid->num_levels - 1) * sizeof(Mat));
    int *local_size = (int *)malloc(multi_grid->num_levels * sizeof(int));
    A_array[0] = petsc_A;
    int idx_level, m, n;
    MatGetSize(A_array[0], &m, &n);
#if PRINT_INFO
    gcge_ops->Printf("·            size of level %d: %d x %d\n", 0, m, n);
#endif
    MatGetLocalSize(A_array[0], &m, &n);
    local_size[0] = m;
    for (idx_level = 1; idx_level < (multi_grid->num_levels); idx_level++)
    {
        P_array[idx_level - 1] = Parr[multi_grid->num_levels - idx_level - 1];
#if PRINT_INFO
        MatGetSize(P_array[idx_level - 1], &m, &n);
        gcge_ops->Printf("·             |  interpolation %d: %d x %d\n", idx_level - 1, m, n);
#endif
        A_array[idx_level] = Aarr[multi_grid->num_levels - idx_level - 1];
        MatGetSize(A_array[idx_level], &m, &n);
#if PRINT_INFO
        gcge_ops->Printf("·            size of level %d: %d x %d\n", idx_level, m, n);
#endif
        MatGetLocalSize(A_array[idx_level], &m, &n);
        local_size[idx_level] = m;
    }
    PetscFree(Aarr);
    PetscFree(Parr);
    PCDestroy(&pc);
#if PRINT_TIME_INFO
    get_A_P_array_time += gcge_ops->GetWtime();
    get_B_array_time -= gcge_ops->GetWtime();
#endif
    B_array = (Mat *)malloc((multi_grid->num_levels) * sizeof(Mat));
    B_array[0] = petsc_B;
    for (idx_level = 1; idx_level < multi_grid->num_levels; idx_level++)
    {
        MatPtAP(B_array[idx_level - 1], P_array[idx_level - 1],
                MAT_INITIAL_MATRIX, PETSC_DEFAULT, &(B_array[idx_level]));
    }

    multi_grid->A_array = (void **)A_array;
    multi_grid->B_array = (void **)B_array;
    multi_grid->P_array = (void **)P_array;
    multi_grid->local_size = local_size;

#if PRINT_TIME_INFO
    get_B_array_time += gcge_ops->GetWtime();
    get_amg_array_time += gcge_ops->GetWtime();
    gcge_ops->Printf("\n------ timer -------\n");
    gcge_ops->Printf("[get_amg_array] 总时间: %f\n", get_amg_array_time);
    gcge_ops->Printf("(1): 调用GAMG获取A_array和P_array时间:%f\n", get_A_P_array_time);
    gcge_ops->Printf("(2): 计算B_array时间:                 %f\n", get_B_array_time);
    gcge_ops->Printf("------ timer -------\n\n");
#endif

    multi_grid->pase_coarse_level = multi_grid->num_levels - 1;
    multi_grid->pase_fine_level = 0;
#if PRINT_INFO
    multi_grid->gcge_ops->Printf("[GMG] 选择粗层为: Level %d\n", multi_grid->pase_coarse_level);
    multi_grid->gcge_ops->Printf("[GMG] 选择细层为: Level %d\n\n", multi_grid->pase_fine_level);
#endif
}

static void get_array_by_gmg(PASE_MULTIGRID multi_grid, PASE_PARAMETER param, OPS *gcge_ops)
{
#if PRINT_INFO
    gcge_ops->Printf("[GMG] multigrid 层数读入: %d levels\n", multi_grid->num_levels);
#endif

    Mat *A_array = (Mat *)malloc((multi_grid->num_levels + 1) * sizeof(Mat)); 
    Mat *B_array = (Mat *)malloc(multi_grid->num_levels * sizeof(Mat));
    Mat *P_array = (Mat *)malloc((multi_grid->num_levels - 1) * sizeof(Mat));
    int *local_size = (int *)malloc(multi_grid->num_levels * sizeof(int));
    Mat *A = (Mat *)param->A_array;
    Mat *B = (Mat *)param->B_array;
    Mat *P = (Mat *)param->P_array;

    int idx_level, m, n;
    A_array[0] = A[multi_grid->num_levels - 1];
    B_array[0] = B[multi_grid->num_levels - 1];
    MatGetSize(A_array[0], &m, &n);
#if PRINT_INFO
    gcge_ops->Printf("·            size of level %d: %d x %d\n", 0, m, n);
#endif
    MatGetLocalSize(A_array[0], &m, &n);
    local_size[0] = m;
    for (idx_level = 1; idx_level < multi_grid->num_levels; idx_level++)
    {
#if PRINT_INFO
        P_array[idx_level - 1] = P[multi_grid->num_levels - 1 - idx_level];
        MatGetSize(P_array[idx_level - 1], &m, &n);
        gcge_ops->Printf("·             |  interpolation %d: %d x %d\n", idx_level - 1, m, n);
#endif
        A_array[idx_level] = A[multi_grid->num_levels - 1 - idx_level];
        B_array[idx_level] = B[multi_grid->num_levels - 1 - idx_level];
        MatGetSize(A_array[idx_level], &m, &n);
#if PRINT_INFO
        gcge_ops->Printf("·            size of level %d: %d x %d\n", idx_level, m, n);
#endif
        MatGetLocalSize(A_array[idx_level], &m, &n);
        local_size[idx_level] = m;
    }
    MatConvert(A_array[multi_grid->num_levels-1],  MATSAME, MAT_INITIAL_MATRIX, &A_array[multi_grid->num_levels]);
    MatGetSize(A_array[multi_grid->num_levels], &m, &n);
    gcge_ops->Printf("·            size of shift level %d: %d x %d\n", multi_grid->num_levels, m, n);

    multi_grid->A_array = (void **)A_array;
    multi_grid->B_array = (void **)B_array;
    multi_grid->P_array = (void **)P_array;
    multi_grid->local_size = local_size;

    multi_grid->pase_coarse_level = multi_grid->num_levels - 1;
    multi_grid->pase_fine_level = 0;
#if PRINT_INFO
    multi_grid->gcge_ops->Printf("[GMG] 选择粗层为: Level %d\n", multi_grid->pase_coarse_level);
    multi_grid->gcge_ops->Printf("[GMG] 选择细层为: Level %d\n\n", multi_grid->pase_fine_level);
#endif
}


static void get_boundary_index(PASE_MULTIGRID multi_grid)
{
    int coarse_level = multi_grid->pase_coarse_level;
    Mat A = multi_grid->A_array[coarse_level];
    Mat B = multi_grid->B_array[coarse_level];
    int start, end, i;
    MatGetOwnershipRange(A, &start, &end);
    bool *if_dirichlet = (bool *)calloc(end - start, sizeof(bool));
    int boundary_num = 0;
    for (i = start; i < end; i++)
    {
        int ncols;
        const int *cols;
        const double *vals;
        MatGetRow(A, i, &ncols, &cols, &vals);
        if (ncols == 1 && cols[0] == i && vals[0] == 1.0)
        {
            if_dirichlet[i - start] = true;
            boundary_num++;
        }
        MatRestoreRow(A, i, &ncols, &cols, &vals);
    }
    multi_grid->boundary_num = boundary_num;
    int *boundary_index = (int *)malloc(boundary_num * sizeof(int));
    boundary_num = 0;
    for (i = 0; i < end - start; i++)
    {
        if (if_dirichlet[i])
        {
            boundary_index[boundary_num] = i;
            boundary_num++;
        }
    }
    multi_grid->boundary_index = boundary_index;
}

int PASE_MULTIGRID_Create(PASE_MULTIGRID *multi_grid, PASE_PARAMETER param, OPS *gcge_ops)
{
    (*multi_grid) = (PASE_MULTIGRID)malloc(sizeof(pase_MultiGrid));
    /* level */
    (*multi_grid)->num_levels = param->num_levels;
    (*multi_grid)->pase_coarse_level = param->num_levels - 1;
    (*multi_grid)->pase_fine_level = 0;
    /* matrix */
    (*multi_grid)->A_array = NULL;
    (*multi_grid)->B_array = NULL;
    (*multi_grid)->P_array = NULL;
    (*multi_grid)->local_size = NULL;
    /* vectors */
    (*multi_grid)->solution = NULL;
    (*multi_grid)->cg_rhs = NULL;
    (*multi_grid)->cg_res = NULL;
    (*multi_grid)->cg_p = NULL;
    (*multi_grid)->cg_w = NULL;
    /* tmp */
    (*multi_grid)->double_tmp = NULL;
    (*multi_grid)->int_tmp = NULL;
    /* ops */
    (*multi_grid)->gcge_ops = gcge_ops;
    /* gmg */
    (*multi_grid)->boundary_num = 0;
    (*multi_grid)->boundary_index = NULL;

    // generate matrices and choose the pase_coarse_level
    if (param->multigrid_type == PASE_AMG)
#if PETSC_GAMG_USE
        get_array_by_petsc_gamg((*multi_grid), param, gcge_ops);
#endif
    else if (param->multigrid_type == PASE_GMG)
        get_array_by_gmg((*multi_grid), param, gcge_ops);

    // check gmg boundary
    if (param->multigrid_type == PASE_GMG && (!(param->boundary_delete)))
        get_boundary_index(*multi_grid);

    return 0;
}

int PASE_MULTIGRID_Destroy(PASE_MULTIGRID *multi_grid)
{
    int level;
    for (level = 0; level < (*multi_grid)->num_levels; ++level)
    {
        MatDestroy((Mat *)(&((*multi_grid)->A_array[level])));
        MatDestroy((Mat *)(&((*multi_grid)->B_array[level])));
    }
    for (level = 0; level < (*multi_grid)->num_levels - 1; ++level)
    {
        MatDestroy((Mat *)(&((*multi_grid)->P_array[level])));
    }
    for (level = (*multi_grid)->pase_fine_level; level < (*multi_grid)->pase_coarse_level; ++level)
    {
        if ((*multi_grid)->solution[level] != NULL)
            (*multi_grid)->gcge_ops->MultiVecDestroy(&((*multi_grid)->solution[level]), 0, (*multi_grid)->gcge_ops);
        if ((*multi_grid)->cg_rhs[level] != NULL)
            (*multi_grid)->gcge_ops->MultiVecDestroy(&((*multi_grid)->cg_rhs[level]), 0, (*multi_grid)->gcge_ops);
        if ((*multi_grid)->cg_res[level] != NULL)
            (*multi_grid)->gcge_ops->MultiVecDestroy(&((*multi_grid)->cg_res[level]), 0, (*multi_grid)->gcge_ops);
        if ((*multi_grid)->cg_p[level] != NULL)
            (*multi_grid)->gcge_ops->MultiVecDestroy(&((*multi_grid)->cg_p[level]), 0, (*multi_grid)->gcge_ops);
        if ((*multi_grid)->cg_w[level] != NULL)
            (*multi_grid)->gcge_ops->MultiVecDestroy(&((*multi_grid)->cg_w[level]), 0, (*multi_grid)->gcge_ops);
    }
    free((*multi_grid)->A_array);
    free((*multi_grid)->B_array);
    free((*multi_grid)->P_array);
    free((*multi_grid)->solution);
    free((*multi_grid)->cg_rhs);
    free((*multi_grid)->cg_res);
    free((*multi_grid)->cg_p);
    free((*multi_grid)->cg_w);
    free((*multi_grid)->double_tmp);
    free((*multi_grid)->int_tmp);
    free((*multi_grid)->local_size);

    free(*multi_grid);
    *multi_grid = NULL;

    return 0;
}
